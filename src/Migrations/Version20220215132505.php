<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220215132505 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE present_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE present (id INT NOT NULL, product_id INT DEFAULT NULL, present_product_id INT DEFAULT NULL, description TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FDBCAE174584665A ON present (product_id)');
        $this->addSql('CREATE INDEX IDX_FDBCAE17DCAD4F1A ON present (present_product_id)');
        $this->addSql('ALTER TABLE present ADD CONSTRAINT FK_FDBCAE174584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE present ADD CONSTRAINT FK_FDBCAE17DCAD4F1A FOREIGN KEY (present_product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE present_id_seq CASCADE');
        $this->addSql('DROP TABLE present');
    }
}
