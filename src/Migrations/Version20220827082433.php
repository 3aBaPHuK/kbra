<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220827082433 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'make all offers active';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE offer SET active = true');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('UPDATE offer SET active = false');
    }
}
