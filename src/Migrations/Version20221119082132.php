<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221119082132 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE configuration_part_variant_id_seq CASCADE');
        $this->addSql('DROP TABLE configuration_part_variant');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE configuration_part_variant_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE configuration_part_variant (id INT NOT NULL, configuration_part_id INT DEFAULT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_2faa26cac2ab8332 ON configuration_part_variant (configuration_part_id)');
        $this->addSql('ALTER TABLE configuration_part_variant ADD CONSTRAINT fk_2faa26cac2ab8332 FOREIGN KEY (configuration_part_id) REFERENCES configuration_part (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
