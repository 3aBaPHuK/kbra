<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221113133428 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE configuration_part ADD image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE configuration_part ADD updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE configuration_part DROP color');
        $this->addSql('COMMENT ON COLUMN configuration_part.updated_at IS \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE configuration_part ADD color VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE configuration_part DROP image');
        $this->addSql('ALTER TABLE configuration_part DROP updated_at');
    }
}
