<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221106075428 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added constructor-bundle tables';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE constructor_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE constructor_item_value_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE constructor_item (id INT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE constructor_item_value (id INT NOT NULL, constructor_item_id INT DEFAULT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B8C1A407B78516C ON constructor_item_value (constructor_item_id)');
        $this->addSql('ALTER TABLE constructor_item_value ADD CONSTRAINT FK_B8C1A407B78516C FOREIGN KEY (constructor_item_id) REFERENCES constructor_item (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE constructor_item_value DROP CONSTRAINT FK_B8C1A407B78516C');
        $this->addSql('DROP SEQUENCE constructor_item_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE constructor_item_value_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE messenger_messages_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('DROP TABLE constructor_item');
        $this->addSql('DROP TABLE constructor_item_value');
    }
}
