<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220827024805 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE offer_color_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE offer_color (id INT NOT NULL, name VARCHAR(255) NOT NULL, icon VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE offer ADD offer_color_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE offer DROP color');
        $this->addSql('ALTER TABLE offer DROP preview');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_29D6873E3A7AFD7 FOREIGN KEY (offer_color_id) REFERENCES offer_color (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_29D6873E3A7AFD7 ON offer (offer_color_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE offer DROP CONSTRAINT FK_29D6873E3A7AFD7');
        $this->addSql('DROP SEQUENCE offer_color_id_seq CASCADE');
        $this->addSql('DROP TABLE offer_color');
        $this->addSql('DROP INDEX IDX_29D6873E3A7AFD7');
        $this->addSql('ALTER TABLE offer ADD color VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE offer ADD preview VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE offer DROP offer_color_id');
    }
}
