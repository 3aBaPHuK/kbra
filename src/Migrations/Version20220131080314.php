<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220131080314 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE shop_counter ADD position INT DEFAULT 0');
        $this->addSql('ALTER TABLE shop_email ADD position INT DEFAULT 0');
        $this->addSql('ALTER TABLE shop_phone ADD position INT DEFAULT 0');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE shop_counter DROP position');
        $this->addSql('ALTER TABLE shop_phone DROP position');
        $this->addSql('ALTER TABLE shop_email DROP position');
    }
}
