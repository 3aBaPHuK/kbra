<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220320115108 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'make link nullable for shop socials';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE shop_social ALTER link DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE shop_social ALTER link SET NOT NULL');
    }
}
