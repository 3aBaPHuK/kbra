<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221113070705 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE configuration_part ADD parent_part_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE configuration_part ADD level INT NOT NULL');
        $this->addSql('ALTER TABLE configuration_part ADD level_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE configuration_part ADD CONSTRAINT FK_2C5C06C13EAB3FAA FOREIGN KEY (parent_part_id) REFERENCES configuration_part (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_2C5C06C13EAB3FAA ON configuration_part (parent_part_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE configuration_part DROP CONSTRAINT FK_2C5C06C13EAB3FAA');
        $this->addSql('DROP INDEX IDX_2C5C06C13EAB3FAA');
        $this->addSql('ALTER TABLE configuration_part DROP parent_part_id');
        $this->addSql('ALTER TABLE configuration_part DROP level');
        $this->addSql('ALTER TABLE configuration_part DROP level_name');
    }
}
