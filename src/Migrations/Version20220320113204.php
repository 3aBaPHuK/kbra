<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220320113204 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added shop socials table and relation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE shop_social_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE shop_social (id INT NOT NULL, shop_id INT DEFAULT NULL, icon VARCHAR(255) NOT NULL, link VARCHAR(255) NOT NULL, position INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F4C3C7054D16C4DD ON shop_social (shop_id)');
        $this->addSql('ALTER TABLE shop_social ADD CONSTRAINT FK_F4C3C7054D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE shop_social_id_seq CASCADE');
        $this->addSql('DROP TABLE shop_social');
    }
}
