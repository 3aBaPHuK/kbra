<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220306073132 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added sketch field for product';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE product ADD sketch TEXT DEFAULT NULL");
    }
}
