<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220518081558 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'point of sale link';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE point_of_sale ADD link_title VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE point_of_sale ADD link VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE point_of_sale DROP link_title');
        $this->addSql('ALTER TABLE point_of_sale DROP link');
    }
}
