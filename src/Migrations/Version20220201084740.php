<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220201084740 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_equipment_item ALTER "position" DROP DEFAULT');
        $this->addSql('ALTER TABLE product_equipment_item ALTER "position" SET NOT NULL');
        $this->addSql('ALTER TABLE product_property ALTER "position" DROP DEFAULT');
        $this->addSql('ALTER TABLE product_property ALTER "position" SET NOT NULL');
        $this->addSql('ALTER TABLE shop_address ALTER "position" DROP DEFAULT');
        $this->addSql('ALTER TABLE shop_counter ALTER "position" DROP DEFAULT');
        $this->addSql('ALTER TABLE shop_counter ALTER "position" SET NOT NULL');
        $this->addSql('ALTER TABLE shop_email ALTER "position" DROP DEFAULT');
        $this->addSql('ALTER TABLE shop_email ALTER "position" SET NOT NULL');
        $this->addSql('ALTER TABLE shop_phone ALTER "position" DROP DEFAULT');
        $this->addSql('ALTER TABLE shop_phone ALTER "position" SET NOT NULL');
        $this->addSql('ALTER TABLE slide RENAME COLUMN sort_order TO position');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE product_property ALTER position SET DEFAULT 0');
        $this->addSql('ALTER TABLE product_property ALTER position DROP NOT NULL');
        $this->addSql('ALTER TABLE shop_address ALTER position SET DEFAULT 0');
        $this->addSql('ALTER TABLE shop_counter ALTER position SET DEFAULT 0');
        $this->addSql('ALTER TABLE shop_counter ALTER position DROP NOT NULL');
        $this->addSql('ALTER TABLE shop_phone ALTER position SET DEFAULT 0');
        $this->addSql('ALTER TABLE shop_phone ALTER position DROP NOT NULL');
        $this->addSql('ALTER TABLE slide RENAME COLUMN position TO sort_order');
        $this->addSql('ALTER TABLE shop_email ALTER position SET DEFAULT 0');
        $this->addSql('ALTER TABLE shop_email ALTER position DROP NOT NULL');
        $this->addSql('ALTER TABLE product_equipment_item ALTER position SET DEFAULT 0');
        $this->addSql('ALTER TABLE product_equipment_item ALTER position DROP NOT NULL');
    }
}
