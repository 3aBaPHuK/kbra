<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Product;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

final class Version20220306064005 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function getDescription(): string
    {
        return 'Move cover images to right path';
    }

    public function up(Schema $schema): void
    {
        $baseDir = $this->container->get('kernel')->getProjectDir().DIRECTORY_SEPARATOR.'public';
        $imagesPath = $baseDir.$this->container->getParameter('app.path.product_images');
        $oldImagesPath = $this->container->getParameter('app.path.1c_files_path');

        $em = $this->container->get('doctrine.orm.entity_manager');
        $productRepository = $em->getRepository(Product::class);

        $products = $productRepository->findAll();

        foreach ($products as $product) {
            /** @var Product $product */
            $oldFileName = $product->getCover();

            if ($oldFileName) {
                if (file_exists($baseDir.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.$oldFileName)) {
                    continue;
                }

                $oldFile = $oldImagesPath.DIRECTORY_SEPARATOR.$oldFileName;
                if (file_exists($oldFile)) {
                    $newFile = $imagesPath.DIRECTORY_SEPARATOR.$oldFileName;

                    rename($oldFile, $newFile);
                }
            }
        }
    }
}
