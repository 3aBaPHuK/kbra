<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221230095840 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added operator emails shop setting';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE shop_email ADD type VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE shop_email DROP type');
    }
}
