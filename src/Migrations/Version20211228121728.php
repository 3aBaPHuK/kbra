<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Transliterator;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211228121728 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'create slug for existing articles';
    }

    public function up(Schema $schema): void
    {
        $articles = $this->connection->fetchAllAssociative('SELECT id, name, slug from article');

        foreach ($articles as $article) {
            if (!empty($article['slug'])) {

                continue;
            }

            $slug = self::slugify($article['name']);
            $this->connection->executeQuery("UPDATE article SET slug='{$slug}' WHERE id=" . $article['id']);
        }
    }

    public static function slugify($text, string $divider = '-'): string
    {
        $text = preg_replace('~[^\pL\d]+~u', $divider, $text);
        $text = transliterator_transliterate('Any-Latin', $text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        $text = trim($text, $divider);
        $text = preg_replace('~ -+~', $divider, $text);
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
