<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221022153739 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Moved coupon categories to relation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE coupon_category (coupon_id INT NOT NULL, category_id INT NOT NULL, PRIMARY KEY(coupon_id, category_id))');
        $this->addSql('CREATE INDEX IDX_1FCF47F366C5951B ON coupon_category (coupon_id)');
        $this->addSql('CREATE INDEX IDX_1FCF47F312469DE2 ON coupon_category (category_id)');
        $this->addSql('ALTER TABLE coupon_category ADD CONSTRAINT FK_1FCF47F366C5951B FOREIGN KEY (coupon_id) REFERENCES coupon (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE coupon_category ADD CONSTRAINT FK_1FCF47F312469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE coupon_category');
        $this->addSql('ALTER TABLE coupon ADD categories TEXT DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN coupon.categories IS \'(DC2Type:simple_array)\'');
    }
}
