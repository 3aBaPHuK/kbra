<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221113112244 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE configuration_part_level_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE configuration_part_level (id INT NOT NULL, name VARCHAR(255) NOT NULL, level INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE configuration_part ADD level_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE configuration_part DROP level');
        $this->addSql('ALTER TABLE configuration_part DROP level_name');
        $this->addSql('ALTER TABLE configuration_part ADD CONSTRAINT FK_2C5C06C15FB14BA7 FOREIGN KEY (level_id) REFERENCES configuration_part_level (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_2C5C06C15FB14BA7 ON configuration_part (level_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE configuration_part DROP CONSTRAINT FK_2C5C06C15FB14BA7');
        $this->addSql('DROP SEQUENCE configuration_part_level_id_seq CASCADE');
        $this->addSql('DROP TABLE configuration_part_level');
        $this->addSql('DROP INDEX IDX_2C5C06C15FB14BA7');
        $this->addSql('ALTER TABLE configuration_part ADD level INT NOT NULL');
        $this->addSql('ALTER TABLE configuration_part ADD level_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE configuration_part DROP level_id');
    }
}
