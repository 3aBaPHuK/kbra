<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221210080929 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add configuration to order items';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE order_item ADD oder_item_configuration TEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE order_item DROP oder_item_configuration');
    }
}
