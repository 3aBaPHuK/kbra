<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Product;
use App\Entity\ProductImage;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220307150127 extends AbstractMigration implements ContainerAwareInterface
{
    use \Symfony\Component\DependencyInjection\ContainerAwareTrait;

    public function getDescription(): string
    {
        return 'Move first product image to cover';
    }

    public function up(Schema $schema): void
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $productRepository = $em->getRepository(Product::class);

        $products = $productRepository->findAll();

        foreach ($products as $product) {
            /** @var Product $product */
            $images = $product->getImageCollection()->toArray();

            if (empty($images)) {
                continue;
            }

            usort($images, function (ProductImage $a, ProductImage $b) {
                return $a->getPosition() > $b->getPosition();
            });

            /** @var ProductImage $firstImage */
            $firstImage = $images[0];

            $product->setCover($firstImage->getImage());
            $product->getImageCollection()->removeElement($firstImage);

            $productRepository->save($product);
        }
    }
}
