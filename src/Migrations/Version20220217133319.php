<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220217133319 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE order_item ADD is_present BOOLEAN');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE order_item DROP is_present');
    }
}
