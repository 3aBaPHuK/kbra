<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220204125909 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE point_of_sale_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE schedule_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE point_of_sale (id INT NOT NULL, title VARCHAR(255) NOT NULL, address TEXT NOT NULL, phone VARCHAR(12) NOT NULL, longitude DOUBLE PRECISION NOT NULL, latitude DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE schedule_item (id INT NOT NULL, point_of_sale_id INT DEFAULT NULL, position INT NOT NULL, day_of_week VARCHAR(3) NOT NULL, "from" VARCHAR(10) NOT NULL, "to" VARCHAR(10) NOT NULL, lunch_from VARCHAR(10) NOT NULL, lunch_to VARCHAR(10) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FF5305456B7E9A73 ON schedule_item (point_of_sale_id)');
        $this->addSql('ALTER TABLE schedule_item ADD CONSTRAINT FK_FF5305456B7E9A73 FOREIGN KEY (point_of_sale_id) REFERENCES point_of_sale (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE schedule_item DROP CONSTRAINT FK_FF5305456B7E9A73');
        $this->addSql('DROP SEQUENCE point_of_sale_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE schedule_item_id_seq CASCADE');
        $this->addSql('DROP TABLE point_of_sale');
        $this->addSql('DROP TABLE schedule_item');
    }
}
