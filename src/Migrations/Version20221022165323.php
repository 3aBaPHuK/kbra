<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221022165323 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Moved to products applying for coupon';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE coupon_product (coupon_id INT NOT NULL, product_id INT NOT NULL, PRIMARY KEY(coupon_id, product_id))');
        $this->addSql('CREATE INDEX IDX_3C22473B66C5951B ON coupon_product (coupon_id)');
        $this->addSql('CREATE INDEX IDX_3C22473B4584665A ON coupon_product (product_id)');
        $this->addSql('ALTER TABLE coupon_product ADD CONSTRAINT FK_3C22473B66C5951B FOREIGN KEY (coupon_id) REFERENCES coupon (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE coupon_product ADD CONSTRAINT FK_3C22473B4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE messenger_messages');
        $this->addSql('DROP TABLE coupon_category');
        $this->addSql('ALTER TABLE coupon DROP categories');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE TABLE coupon_category (coupon_id INT NOT NULL, category_id INT NOT NULL, PRIMARY KEY(coupon_id, category_id))');
        $this->addSql('CREATE INDEX idx_1fcf47f366c5951b ON coupon_category (coupon_id)');
        $this->addSql('CREATE INDEX idx_1fcf47f312469de2 ON coupon_category (category_id)');
        $this->addSql('ALTER TABLE coupon_category ADD CONSTRAINT fk_1fcf47f366c5951b FOREIGN KEY (coupon_id) REFERENCES coupon (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE coupon_category ADD CONSTRAINT fk_1fcf47f312469de2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE coupon_product');
    }
}
