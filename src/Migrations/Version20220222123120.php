<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220222123120 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added is active for products';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE product ADD active BOOLEAN DEFAULT true');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE product DROP active');
    }
}
