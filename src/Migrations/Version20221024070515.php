<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221024070515 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added present for coupon';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE coupon ADD has_present BOOLEAN DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE coupon DROP has_present');
    }
}
