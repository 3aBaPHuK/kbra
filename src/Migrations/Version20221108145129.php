<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221108145129 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE constructor_item_value DROP CONSTRAINT fk_b8c1a407b78516c');
        $this->addSql('DROP SEQUENCE constructor_item_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE constructor_item_value_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE configuration_part_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE configuration_part_variant_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_configuration_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE configuration_part (id INT NOT NULL, configuration_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2C5C06C173F32DD8 ON configuration_part (configuration_id)');
        $this->addSql('CREATE TABLE configuration_part_variant (id INT NOT NULL, configuration_part_id INT DEFAULT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2FAA26CAC2AB8332 ON configuration_part_variant (configuration_part_id)');
        $this->addSql('CREATE TABLE product_configuration (id INT NOT NULL, product_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7F0FB9254584665A ON product_configuration (product_id)');
        $this->addSql('ALTER TABLE configuration_part ADD CONSTRAINT FK_2C5C06C173F32DD8 FOREIGN KEY (configuration_id) REFERENCES product_configuration (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE configuration_part_variant ADD CONSTRAINT FK_2FAA26CAC2AB8332 FOREIGN KEY (configuration_part_id) REFERENCES configuration_part (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_configuration ADD CONSTRAINT FK_7F0FB9254584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE constructor_item');
        $this->addSql('DROP TABLE constructor_item_value');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE configuration_part_variant DROP CONSTRAINT FK_2FAA26CAC2AB8332');
        $this->addSql('ALTER TABLE configuration_part DROP CONSTRAINT FK_2C5C06C173F32DD8');
        $this->addSql('DROP SEQUENCE configuration_part_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE configuration_part_variant_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_configuration_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE constructor_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE constructor_item_value_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE constructor_item (id INT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE constructor_item_value (id INT NOT NULL, constructor_item_id INT DEFAULT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_b8c1a407b78516c ON constructor_item_value (constructor_item_id)');
        $this->addSql('ALTER TABLE constructor_item_value ADD CONSTRAINT fk_b8c1a407b78516c FOREIGN KEY (constructor_item_id) REFERENCES constructor_item (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE configuration_part');
        $this->addSql('DROP TABLE configuration_part_variant');
        $this->addSql('DROP TABLE product_configuration');
    }
}
