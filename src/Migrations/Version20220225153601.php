<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220225153601 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Separated product images to cover, images, and gallery';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE product_gallery_image_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE product_gallery_image (id INT NOT NULL, product_id INT DEFAULT NULL, image VARCHAR(255) NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, position INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FB4DC88E4584665A ON product_gallery_image (product_id)');
        $this->addSql('ALTER TABLE product_gallery_image ADD CONSTRAINT FK_FB4DC88E4584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product ADD cover VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE product_image ADD position INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_image DROP main');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE product_gallery_image_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE messenger_messages_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('DROP TABLE product_gallery_image');
        $this->addSql('ALTER TABLE product_image ADD main BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE product_image DROP position');
        $this->addSql('ALTER TABLE product DROP cover');
    }
}
