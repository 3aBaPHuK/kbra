<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Product;
use App\Entity\ProductGalleryImage;
use App\Entity\ProductImage;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220307145630 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function getDescription(): string
    {
        return 'Add position for all images';
    }

    public function up(Schema $schema): void
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $productRepo = $em->getRepository(Product::class);
        $products = $productRepo->findAll();


        foreach ($products as $product) {
            /** @var Product $product */

            /** @var ProductImage[] $images */
            $images = $product->getImageCollection()->toArray();

            if (!empty($images) && $images[0]->getPosition() !== null) {
                continue;
            }

            foreach ($images as $key => $image) {
                $image->setPosition($key + 1);

                $em->persist($image);
            }


            /** @var ProductGalleryImage $gallery */
            $gallery = $product->getGalleryImagesCollection()->toArray();

            if (!empty($gallery) && $gallery[0]->getPosition() !== null) {
                continue;
            }

            foreach ($gallery as $key => $item) {
                $item->setPosition($key + 1);

                $em->persist($item);
            }

            /** @var ProductGalleryImage $gallery */
            $gallery = $product->getGalleryImagesCollection()->toArray();

            if (!empty($gallery) && $gallery[0]->getPosition() !== null) {
                continue;
            }

            foreach ($gallery as $key => $item) {
                $item->setPosition($key + 1);

                $em->persist($item);
            }

            /** @var ProductGalleryImage $properties */
            $properties = $product->getProductProperties()->toArray();

            if (!empty($properties) && $properties[0]->getPosition() !== null) {
                continue;
            }

            foreach ($properties as $key => $property) {
                $property->setPosition($key + 1);

                $em->persist($property);
            }

            /** @var ProductGalleryImage $properties */
            $equipments = $product->getProductEquipments()->toArray();

            if (!empty($equipments) && $equipments[0]->getPosition() !== null) {
                continue;
            }

            foreach ($equipments as $key => $equipment) {
                $equipment->setPosition($key + 1);

                $em->persist($equipment);
            }
        }

        $em->flush();
    }
}
