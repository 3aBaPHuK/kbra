<?php

namespace App\Entity;

use App\Modules\DiscountBundle\Entity\Discount;
use App\Modules\PresentBundle\Entity\Present;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Modules\ProductConfigurationBundle\Entity\Configuration;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @Vich\Uploadable
 */
class Product implements \JsonSerializable
{
    public function __toString()
    {
        return $this->getName() ?? '';
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $vendorCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $announce;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="products")
     * @ORM\JoinColumn(nullable=true)
     */
    private $category;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cover;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $sketch;

    /**
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="cover")
     * @var File
     */
    private $coverFile;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductImage", mappedBy="product", cascade={"remove", "persist"}, orphanRemoval=true)
     */
    private $imageCollection;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\ProductGalleryImage", mappedBy="product", cascade={"remove", "persist"}, orphanRemoval=true)
     */
    private $galleryImagesCollection;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=128, unique=true, nullable=true)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductOffer", mappedBy="product", cascade={"persist", "remove"})
     */
    private $productOffers;

    /**
     * @ORM\OneToMany(targetEntity=ProductProperty::class, mappedBy="product", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $productProperties;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=ProductEquipmentItem::class, mappedBy="product", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $productEquipments;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $externalId;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaTitle;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaKeywords;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var Present
     * @ORM\OneToMany(targetEntity=Present::class, mappedBy="product", cascade={"remove"})
     */
    private $present;

    /**
     * @var Present
     * @ORM\OneToMany(targetEntity=Present::class, mappedBy="presentProduct", cascade={"remove"})
     */
    private $presentFor;

    /**
     * @ORM\ManyToOne(targetEntity=Discount::class, inversedBy="products")
     * @var Discount|null
     */
    private $discount;

    /**
     * @ORM\ManyToOne(targetEntity=Configuration::class, inversedBy="products")
     */
    private $configuration;

    public function __construct()
    {
        $this->imageCollection = new ArrayCollection();
        $this->galleryImagesCollection = new ArrayCollection();
        $this->productProperties = new ArrayCollection();
        $this->productEquipments = new ArrayCollection();
        $this->productOffers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAnnounce(): ?string
    {
        return $this->announce;
    }

    public function setAnnounce(string $announce): self
    {
        $this->announce = $announce;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @param File|null $image
     *
     * @throws Exception
     */
    public function setCoverFile(?File $image = null)
    {
        $this->coverFile = $image;

        if ($image) {
            $this->updatedAt = new DateTime('now');
        }
    }

    public function getCoverFile(): ?File
    {
        return $this->coverFile;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(?string $cover) {
        $this->cover = $cover;
    }

    /**
     * @return Collection|ProductImage[]
     */
    public function getImageCollection(): Collection
    {
        return $this->imageCollection;
    }

    public function addImageCollection(?ProductImage $imageCollection): self
    {
        if (!$this->imageCollection->contains($imageCollection)) {
            if (!$imageCollection->getImage() && $imageCollection->getImageFile()) {
                $imageCollection->setImage($imageCollection->getImageFile()->getBasename());
            }

            $this->imageCollection[] = $imageCollection;
            $imageCollection->setProduct($this);
        }

        return $this;
    }

    public function removeImageCollection(ProductImage $imageCollection): self
    {
        if ($this->imageCollection->contains($imageCollection)) {
            $this->imageCollection->removeElement($imageCollection);
            // set the owning side to null (unless already changed)
            if ($imageCollection->getProduct() === $this) {
                $imageCollection->setProduct(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getMainImage() {

        foreach ($this->getImageCollection()->getValues() as $image) {
            if ($image->getMain()) {
                return $image;
            }
        }

        return null;
    }

    public function getProductOffers(?bool $activeOnly = true):? Collection
    {
        if ($activeOnly) {
            return $this->productOffers->filter(function (ProductOffer $productOffer) {
                return $productOffer->getOffer()->getActive();
            });
        }

        return $this->productOffers;
    }

    public function addProductOffer(ProductOffer $productOffer): self
    {
        if (!$this->productOffers->contains($productOffer)) {
            $this->productOffers[] = $productOffer;
            $productOffer->setProduct($this);
        }

        return $this;
    }

    public function removeProductOffer(ProductOffer $productOffer): self
    {
        if ($this->productOffers->contains($productOffer)) {
            $this->productOffers->removeElement($productOffer);
            if ($productOffer->getProduct() === $this) {
                $productOffer->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductProperty[]
     */
    public function getProductProperties(): Collection
    {
        return $this->productProperties;
    }

    public function addProductProperty(ProductProperty $productProperty): self
    {
        if (!$this->productProperties->contains($productProperty)) {
            $this->productProperties[] = $productProperty;
            $productProperty->setProduct($this);
        }

        return $this;
    }

    public function addProductEquipment(ProductEquipmentItem $productEquipment): self
    {
        if (!$this->productEquipments->contains($productEquipment)) {
            $this->productEquipments[] = $productEquipment;
            $productEquipment->setProduct($this);
        }

        return $this;
    }

    public function removeProductEquipment(ProductEquipmentItem $productEquipment): self
    {
        if ($this->productEquipments->removeElement($productEquipment)) {
            if ($productEquipment->getProduct() === $this) {
                $productEquipment->setProduct(null);
            }
        }

        return $this;
    }

    public function removeProductProperty(ProductProperty $productProperty): self
    {
        if ($this->productProperties->removeElement($productProperty)) {
            if ($productProperty->getProduct() === $this) {
                $productProperty->setProduct(null);
            }
        }

        return $this;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    public function setExternalId($externalId): void
    {
        $this->externalId = $externalId;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    public function setMetaKeywords(?string $metaKeywords): void
    {
        $this->metaKeywords = $metaKeywords;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    public function getProductEquipments()
    {
        return $this->productEquipments;
    }

    public function getVendorCode():? string
    {
        return $this->vendorCode;
    }

    public function setVendorCode(?string $vendorCode): void
    {
        $this->vendorCode = $vendorCode;
    }

    /**
     * @return bool
     */
    public function isActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return Collection
     */
    public function getGalleryImagesCollection(): Collection
    {
        return $this->galleryImagesCollection;
    }

    public function addGalleryImagesCollection(?ProductGalleryImage $image): self
    {
        if (!$this->galleryImagesCollection->contains($image)) {
            if (!$image->getImage() && $image->getImageFile()) {
                $image->setImage($image->getImageFile()->getBasename());
            }

            $this->galleryImagesCollection[] = $image;
            $image->setProduct($this);
        }

        return $this;
    }

    public function removeGalleryImagesCollection(ProductGalleryImage $image): self
    {
        if ($this->galleryImagesCollection->contains($image)) {
            $this->galleryImagesCollection->removeElement($image);
            if ($image->getProduct() === $this) {
                $image->setProduct(null);
            }
        }

        return $this;
    }

    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getSketch(): ?string
    {
        return $this->sketch;
    }

    public function setSketch(?string $sketch): void
    {
        $this->sketch = $sketch;
    }

    public function getDiscount(): ?Discount
    {
        return $this->discount;
    }

    public function setDiscount(?Discount $discount): void
    {
        $this->discount = $discount;
    }

    public function getConfiguration(): ?Configuration
    {
        return $this->configuration;
    }

    public function setConfiguration(?Configuration $configuration): void
    {
        $this->configuration = $configuration;
    }
}
