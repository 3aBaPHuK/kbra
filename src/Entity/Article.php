<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 * @Vich\Uploadable
 *
 */
class Article
{
    public const TAG_INFORMATION_TO_CLIENTS = 'INFORMATION_TO_CLIENTS';
    public const TAG_ABOUT_MANUFACTURER     = 'ABOUT_MANUFACTURER';
    public const TAG_LEGAL_INFORMATION      = 'LEGAL_INFORMATION';
    public const TAG_TECHNICAL_INFORMATION  = 'TECHNICAL_INFORMATION';
    public const TAG_PAYMENT_AND_DELIVERY   = 'PAYMENT_AND_DELIVERY';

    public static function getAvailableTags(): array
    {
        return [
            self::TAG_INFORMATION_TO_CLIENTS => self::TAG_INFORMATION_TO_CLIENTS,
            self::TAG_ABOUT_MANUFACTURER => self::TAG_ABOUT_MANUFACTURER,
            self::TAG_LEGAL_INFORMATION => self::TAG_LEGAL_INFORMATION,
            self::TAG_TECHNICAL_INFORMATION => self::TAG_TECHNICAL_INFORMATION,
            self::TAG_PAYMENT_AND_DELIVERY => self::TAG_PAYMENT_AND_DELIVERY,
        ];
    }

    public function __toString()
    {
        return $this->getName() ?? '';
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     */
    private $announce;

    /**
     * @Vich\UploadableField(mapping="article_images", fileNameProperty="announceImage")
     * @var File
     */
    private $announceImageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $announceImage;

    /**
     * @Vich\UploadableField(mapping="article_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ArticleCategory", inversedBy="articles")
     * @ORM\JoinColumn(nullable=true)
     */
    private $category;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaTitle;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaKeywords;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity=Gallery::class, mappedBy="article")
     */
    private $gallery;

    public function __construct()
    {
        $this->gallery = new ArrayCollection();
    }

    /**
     * @param File|null $image
     *
     *
     * @throws Exception
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        $this->updatedAt = new \DateTime('now');
    }

    /**
     * @param File|null $image
     *
     * @throws Exception
     */
    public function setAnnounceImageFile(File $image = null)
    {
        $this->announceImageFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getAnnounceImageFile():? File
    {
        return $this->announceImageFile;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAnnounce(): ?string
    {
        return $this->announce;
    }

    public function setAnnounce(string $announce): self
    {
        $this->announce = $announce;

        return $this;
    }

    public function getAnnounceImage(): ?string
    {
        return $this->announceImage;
    }

    public function setAnnounceImage(?string $announceImage): self
    {
        $this->announceImage = $announceImage;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getCategory(): ?ArticleCategory
    {
        return $this->category;
    }

    public function setCategory(?ArticleCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeInterface $updatedAt): self
    {
        if (!$updatedAt) {
            $updatedAt = new \DateTime();
        }

        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|Gallery[]
     */
    public function getGallery(): Collection
    {
        return $this->gallery;
    }

    public function addGallery(Gallery $gallery): self
    {
        if (!$this->gallery->contains($gallery)) {
            $this->gallery[] = $gallery;
            $gallery->setArticle($this);
        }

        return $this;
    }

    public function removeGallery(Gallery $gallery): self
    {
        if ($this->gallery->removeElement($gallery)) {
            if ($gallery->getArticle() === $this) {
                $gallery->setArticle(null);
            }
        }

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    public function setMetaKeywords(?string $metaKeywords): void
    {
        $this->metaKeywords = $metaKeywords;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }
}
