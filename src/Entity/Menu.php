<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MenuRepository")
 */
class Menu
{
    public const TYPE_CATALOG = 'catalog';
    public const TYPE_ARTICLE_LIST = 'article_list';
    public const TYPE_GALLERY = 'gallery';
    public const TYPE_CONTACTS = 'contacts';
    public const TYPE_MAIN = 'main';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $route;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $params = [];

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $sort;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getRoute(): ?string
    {
        return $this->route;
    }

    public function setRoute(string $route): self
    {
        $this->route = $route;

        return $this;
    }

    public function getParams(): ?array
    {
        return $this->params;
    }

    public function setParams(?array $params): self
    {
        $this->params = $params;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public static function getAvailableTypes(): array
    {
        return [self::TYPE_ARTICLE_LIST, self::TYPE_CATALOG, self::TYPE_GALLERY, self::TYPE_CONTACTS, self::TYPE_MAIN];
    }

    public function setType(?string $type): self
    {
        if ($type && !in_array($type, self::getAvailableTypes())) {
            throw new InvalidArgumentException('Invalid menu type: '.$type.'. Type might be only: '.implode(' or ' , self::getAvailableTypes()));
        }

        $this->type = $type;

        return $this;
    }

    public function getSort(): ?int
    {
        return $this->sort;
    }

    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }
}
