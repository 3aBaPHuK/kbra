<?php

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints\Date;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OfferRepository")
 * @Vich\Uploadable
 */
class Offer implements JsonSerializable
{
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $externalCode;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $discountPrice;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $updatedAt;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $sketch;

    /**
     * @ORM\OneToMany(targetEntity=OfferImage::class, mappedBy="offer", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $offerImages;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cover;

    /**
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="cover")
     * @var File
     */
    private $coverFile;

    /**
     * @ORM\ManyToOne(targetEntity=OfferColor::class, inversedBy="offer")
     */
    private $offerColor;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductOffer", mappedBy="offer", cascade={"persist", "remove"})
     */
    private $productOffers;

    public function __construct()
    {
        $this->offerImages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getExternalCode(): ?string
    {
        return $this->externalCode;
    }

    public function setExternalCode(?string $externalCode): self
    {
        $this->externalCode = $externalCode;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function getDiscountPrice(): ?float
    {
        return $this->discountPrice;
    }

    public function setDiscountPrice(?float $discountPrice): self
    {
        $this->discountPrice = $discountPrice;

        return $this;
    }

    public function getSketch(): ?string
    {
        return $this->sketch;
    }

    public function setSketch(?string $sketch): void
    {
        $this->sketch = $sketch;
    }

    /**
     * @return Collection|OfferImage[]
     */
    public function getOfferImages(): Collection
    {
        return $this->offerImages;
    }

    public function addOfferImage(OfferImage $offerImage): self
    {
        if (!$this->offerImages->contains($offerImage)) {
            $this->offerImages[] = $offerImage;
            $offerImage->setOffer($this);
        }

        return $this;
    }

    public function removeOfferImage(OfferImage $offerImage): self
    {
        if ($this->offerImages->removeElement($offerImage)) {
            // set the owning side to null (unless already changed)
            if ($offerImage->getOffer() === $this) {
                $offerImage->setOffer(null);
            }
        }

        return $this;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(?string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    public function setCoverFile(?File $image = null)
    {
        $this->coverFile = $image;

        if ($image) {
            $this->updatedAt = new DateTimeImmutable('now');
        }
    }

    public function getCoverFile(): ?File
    {
        return $this->coverFile;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(): void
    {
        $this->updatedAt = new DateTimeImmutable('now');
    }

    public function getOfferColor(): ?OfferColor
    {
        return $this->offerColor;
    }

    public function setOfferColor(?OfferColor $offerColor): self
    {
        $this->offerColor = $offerColor;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
