<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 25.04.2020
 * Time: 19:17
 */
namespace App\Twig\Shop;

use App\Controller\Shop\ShopController;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ShopExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('getMenu', [$this, 'getMenu']),
        ];
    }
}