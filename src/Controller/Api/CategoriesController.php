<?php

namespace App\Controller\Api;

use App\Dto\Categories\CategoriesCollection;
use App\Dto\Categories\CategoryDto;
use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Repository\OfferRepository;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

class CategoriesController extends AbstractController
{

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(CategoryRepository $categoryRepository, ProductRepository $productRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * List of categories
     *
     * @Route("/api/categories", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns all active categories collection",
     * )
     * @OA\Tag(name="categories")
     */
    public function getCategories(): JsonResponse
    {
        $categories = $this->categoryRepository->findBy(['status' => true], ['level' => 'ASC', 'position' => 'ASC']);
        $values     = [];
        foreach ($categories as $category) {
            $cheapestPrice = $this->productRepository->findCheapestPriceOfCategory($category->getId());

            $values[] = CategoryDto::fromCategoryEntity($category, $this->getParameter('app.path.product_images'), $cheapestPrice);
        }

        return new JsonResponse(new CategoriesCollection(...$values));
    }

    /**
     * Category details by it's slug
     *
     * @Route("/api/categories/{slug}", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns category dto by id",
     * )
     * @OA\Tag(name="categories")
     */
    public function getCategory($slug): JsonResponse
    {
        $category = $this->categoryRepository->findOneBy(['slug' => $slug, 'status' => true]);


        if (!$category) {

            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(CategoryDto::fromCategoryEntity($category, $this->getParameter('app.path.product_images')));
    }
}
