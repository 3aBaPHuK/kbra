<?php

namespace App\Controller\Api;

use App\Dto\Articles\ArticleCategoryDto;
use App\Dto\Articles\ArticleDto;
use App\Dto\Articles\ArticlesCollection;
use App\Repository\ArticleCategoryRepository;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

class ArticlesController extends AbstractController
{
    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    /**
     * @var ArticleCategoryRepository
     */
    private $categoryRepository;

    public function __construct(ArticleRepository $articleRepository, ArticleCategoryRepository $categoryRepository)
    {
        $this->articleRepository = $articleRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Get category by slug
     *
     * @Route("/api/category/{slug}", methods={"GET"})
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns category with articles collection by slug",
     * )
     * @OA\Tag(name="articles")
     */
    public function getArticlesByPosition(string $slug): JsonResponse
    {
        $category = $this->categoryRepository->findOneBy(['slug' => $slug]);

        if (!$category) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(ArticleCategoryDto::fromEntity($category, $this->getParameter('app.path.article_images')));
    }
}