<?php

namespace App\Controller\Api;

use App\Service\ErpService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ExchangeController extends AbstractController
{

    /**
     * @var ErpService
     */
    private $service;

    public function __construct(ErpService $service)
    {
        $this->service = $service;
    }

    /**
     * Starts exchange process
     *
     * @Route("/api/exchange", methods={"POST", "GET"})
     * @OA\Tag(name="catalog")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function start(Request $request): ?Response
    {
        ini_set('post_max_size', '1024M');
        ini_set('upload_max_filesize', '1024M');


        switch ($request->get('type')) {
            case 'catalog':
                return $this->importProducts($request);
            case 'sale':
                return $this->exchangeOrders($request);
        }

        return new Response(null, Response::HTTP_BAD_REQUEST);
    }

    private function exchangeOrders(Request $request): Response
    {
        if ($response = $this->checkAuth($request)) {

            return $response;
        }

        if ($request->get('mode') === 'query') {

            $response = new Response($this->service->getOrdersXml());

            $response->headers->set('Content-Type', 'text/xml');

            return $response;
        }

        return new Response('OK');
    }

    private function importProducts(Request $request): ?Response
    {
        if ($response = $this->checkAuth($request)) {

            return $response;
        }

        if ($response = $this->checkExchangeParameters($request)) {

            return $response;
        }

        if ($request->get('mode') === 'file') {
            $this->service->writeFileFromFileRequest($request);

            return new Response('success' . PHP_EOL . 'test' . PHP_EOL . base64_encode('test'));
        }

        if ($request->get('mode') === 'import') {
            $this->service->startImport($request->get('filename'));

            return new Response('success');
        }

        return new Response('success' . PHP_EOL . 'test' . PHP_EOL . base64_encode('test'));
    }

    private function checkExchangeParameters(Request $request): ?Response
    {
        $type = $request->get('type');
        $mode = $request->get('mode');
        if ($type === 'catalog' && $mode === 'init') {
            return new Response("zip=no\nfile_limit=20000000");
        }

        return null;
    }

    private function checkAuth(Request $request): ?Response
    {
        $mode = $request->get('mode');
        if ($mode === 'checkauth') {
            return new Response("success\ntestcookie\ntestcookievalue");
        }

        return null;
    }
}
