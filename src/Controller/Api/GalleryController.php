<?php

namespace App\Controller\Api;

use App\Dto\Gallery\GalleriesCollection;
use App\Dto\Gallery\GalleryDto;
use App\Dto\Gallery\GalleryItemDto;
use App\Dto\Gallery\GalleryItemsCollection;
use App\Entity\Gallery;
use App\Entity\GalleryItem;
use App\Repository\GalleryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Annotations as OA;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class GalleryController extends AbstractController
{
    /**
     * @var GalleryRepository
     */
    private $galleryRepository;

    public function __construct(GalleryRepository $galleryRepository)
    {
        $this->galleryRepository = $galleryRepository;
    }

    /**
     * Get gallery items by gallery ID
     *
     * @Route("/api/gallery/items/{id}", methods={"GET"})
     * @OA\Tag(name="gallery")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function getGalleryItemsByGalleryId(int $id): JsonResponse
    {
        $gallery = $this->galleryRepository->find($id);

        if (!$gallery) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        $galleryItems = $gallery->getGalleryItems()->toArray();

        usort($galleryItems, function (GalleryItem $a, GalleryItem $b) {
            return $a->getPosition() < $b->getPosition() ? -1 : 1;
        });

        return new JsonResponse(new GalleryItemsCollection(...array_map(function (GalleryItem $entity) {
            return GalleryItemDto::fromGalleryItemEntity($entity, $this->getParameter('app.path.gallery_images'));
        }, $galleryItems)));
    }

    /**
     * Get galleries list with no article
     *
     * @Route("/api/galleries", methods={"GET"})
     * @OA\Tag(name="gallery")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function getGalleriesList(): JsonResponse
    {
        $galleries = $this->galleryRepository->findBy(['article' => null]);

        if (!$galleries) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        $galleriesDtoArray = array_map(function (Gallery $galleryEntity) {
            return GalleryDto::fromGalleryEntity($galleryEntity, $this->getParameter('app.path.gallery_images'));
        }, $galleries);

        return new JsonResponse(new GalleriesCollection(...$galleriesDtoArray));
    }
}
