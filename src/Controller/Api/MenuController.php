<?php

namespace App\Controller\Api;

use App\Dto\Menu\MenuDto;
use App\Dto\Menu\MenuItemsCollection;
use App\Repository\MenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

class MenuController extends AbstractController
{
    /**
     * @var MenuRepository
     */
    private $menuRepository;

    public function __construct(MenuRepository $menuRepository)
    {
        $this->menuRepository = $menuRepository;
    }

    /**
     * Get all menu items
     *
     * @Route("/api/menu/list", methods={"GET"})
     * @OA\Tag(name="menu")
     */
    public function getMenuItems(): JsonResponse
    {
        $menuItems = $this->menuRepository->findAll();

        return new JsonResponse(new MenuItemsCollection(...array_map(function($menuItemEntity) {
            return MenuDto::fromMenuEntity($menuItemEntity);
        }, $menuItems)));
    }
}