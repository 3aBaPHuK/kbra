<?php

namespace App\Controller\Api;

use App\Dto\Products\ProductDto;
use App\Dto\Products\ProductsCollection;
use App\Entity\Category;
use App\Entity\Offer;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

class ProductController extends AbstractController
{

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(CategoryRepository $categoryRepository, ProductRepository $productRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * Get products list
     *
     * @Route("/api/products", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns product dto's collection",
     * )
     * @OA\Tag(name="products")
     */
    public function getProducts(): JsonResponse
    {
        $products = $this->productRepository->findAll();
        $values = [];
        foreach ($products as $product) {
            $values[] = ProductDto::fromProductEntity($product, $this->getParameter('app.path.product_images'));
        }

        return new JsonResponse(new ProductsCollection(...$values));
    }

    /**
     * Get products list
     *
     * @Route("/api/category/{slug}/products", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns product dto's collection",
     * )
     * @OA\Tag(name="products")
     */
    public function getProductsOfCategory($slug): JsonResponse
    {
        $categoryIds = $this->categoryRepository->findChildCategoryIds($slug);

        if (!$categoryIds) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        $products = $this->productRepository->findByCategoryIdsList($categoryIds);

        usort($products, function (Product $a, Product $b) {
            $offerA = $a->getProductOffers()->first();
            $offerB = $b->getProductOffers()->first();

            if ($offerA && $offerB) {
                return $offerA->getOffer()->getPrice() < $offerB->getOffer()->getPrice() ? -1 : 1;
            }

            return -1;
        });

        $values = [];
        foreach ($products as $product) {
            if (!$product->isActive()) {
                continue;
            }

            $values[] = ProductDto::fromProductEntity($product, $this->getParameter('app.path.product_images'));
        }

        return new JsonResponse(new ProductsCollection(...$values));
    }

    /**
     * Get product by id
     *
     * @Route("/api/products/{id}", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns product dto",
     * )
     * @OA\Tag(name="products")
     */
    public function getProductById(int $id): JsonResponse
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);

        return new JsonResponse(ProductDto::fromProductEntity($product, $this->getParameter('app.path.product_images')));
    }

    /**
     * Get products by ids
     *
     * @Route("/api/products/list/byIds", methods={"POST"})
     * @OA\Response(
     *     response=200,
     *     description="Returns product dto's collection",
     * )
     *
     * @OA\RequestBody(
     *   required=true,
     *   content={ @OA\MediaType(mediaType="application/json") }
     * )
     * @OA\Tag(name="products")
     */
    public function getProductsByIds(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (!$data || !is_array($data)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        $products = $this->productRepository->findByIdList($data);

        $values = [];

        foreach ($products as $product) {
            $values[] = ProductDto::fromProductEntity($product, $this->getParameter('app.path.product_images'));
        }

        return new JsonResponse(new ProductsCollection(...$values));
    }

    /**
     * Get product by slug
     *
     * @Route("/api/{categorySlug}/product/{slug}", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns product dto",
     * )
     * @OA\Tag(name="products")
     */
    public function getProductBySlugAndCategorySlug(string $categorySlug, string $slug): JsonResponse
    {
        $product = $this->productRepository->findBySlugAndCategory($slug, $categorySlug);

        if (!$product) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(ProductDto::fromProductEntity($product, $this->getParameter('app.path.product_images')));
    }
}
