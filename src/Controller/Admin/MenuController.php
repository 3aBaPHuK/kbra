<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 25.04.2020
 * Time: 17:49
 */

namespace App\Controller\Admin;

use App\Entity\Menu;
use App\Form\Admin\MenuParamsType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class MenuController extends EasyAdminController
{

    protected function createEntityFormBuilder($entity, $view)
    {
        $formBuilder = parent::createEntityFormBuilder($entity, $view);

        $choices = [];

        foreach (Menu::getAvailableTypes() as $type) {
            $choices['ADMIN_SHOP_FORM_MENU_TYPE_'.strtoupper($type)] = $type;
        }

        $formBuilder
            ->add('type', ChoiceType::class, [
                'choices' => $choices,
            ]);

        return $formBuilder;
    }
}