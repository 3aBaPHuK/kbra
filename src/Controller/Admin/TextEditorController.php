<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TextEditorController extends AbstractController
{
    /**
     * @Route("/admin/editor/upload", name="admin_editor_upload")
     */
    public function upload(Request $request, ValidatorInterface $validator): Response
    {
        /** @var UploadedFile $file */
        $uploadedFile = $request->files->get('file');

        $violations = $validator->validate($uploadedFile, [
            new NotBlank(),
            new File([
                'mimeTypes' => [
                    'image/*',
                    'application/pdf',
                ]
            ])]);

        if ($violations->count() > 0) {
            /** @var ConstraintViolation $violation */
            $violation = $violations[0];
            return new Response($violation->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $newFilename = uniqid().'.'.$uploadedFile->guessExtension();

        $uploadedFile->move($this->getParameter('app.path.articles_upload_path'), $newFilename);

        return new Response($this->getParameter('app.path.article_images') . '/' . $newFilename);
    }
}