<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 26.04.2020
 * Time: 19:16
 */

namespace App\Controller\Admin;

use App\Entity\Offer;
use App\Repository\ProductOfferRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class OfferController extends EasyAdminController
{

    private $productOfferRepository;

    public function __construct(ProductOfferRepository $productOfferRepository)
    {
        $this->productOfferRepository = $productOfferRepository;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function importAction(): RedirectResponse
    {
        set_time_limit(99999);

        $client = new CurlHttpClient();
        try {
            $resultsCount = 100;
            $offset       = 0;

            while ($resultsCount >= 99) {
                $command = "entity/assortment?limit=100&offset=$offset";

                $response = $this->getChunkResponse($client, $command);

                $resultArr    = json_decode($response, true);
                $resultsCount = 0;

                if (!empty($resultArr['rows'])) {
                    $offset       += 100;
                    $resultsCount = count($resultArr['rows']);

                    foreach ($resultArr['rows'] as $myStoreOffer) {
                        $er    = $this->em->getRepository(Offer::class);
                        $offer = $er->findOneBy(['externalCode' => $myStoreOffer['externalCode']]);

                        if (!$offer) {
                            $offer = new Offer();
                        }

                        $this->updateOrFillOffer($myStoreOffer, $offer);

                        $this->em->persist($offer);
                        $this->em->flush();
                    }
                }
            }

            $this->addFlash('success', 'ADMIN_IMPORT_ARE_SUCCESS');
        } catch (TransportExceptionInterface $e) {
        }

        return $this->redirectToReferrer();
    }

    /**
     * @param $myStoreOffer
     * @param Offer $offer
     */
    private function updateOrFillOffer($myStoreOffer, Offer $offer): void
    {
        foreach ($myStoreOffer as $property => $value) {
            $capitalizedProperty = ucfirst($property);

            $this->setField($offer, $capitalizedProperty, $value);

            $singleProp = substr_replace($capitalizedProperty, '', -1);

            $this->setCollection($offer, $singleProp, $value, $capitalizedProperty);
        }
    }

    /**
     * @param Offer $offer
     * @param string $capitalizedProperty
     * @param $value
     */
    private function setField(Offer $offer, string $capitalizedProperty, $value): void
    {
        if (method_exists($offer, "set$capitalizedProperty")) {
            $offer->{"set" . $capitalizedProperty}($value);
        }
    }

    /**
     * @param Offer $offer
     * @param $singleProp
     * @param $value
     * @param $capitalizedProperty
     */
    private function setCollection(Offer $offer, $singleProp, $value, $capitalizedProperty): void
    {
        if (method_exists($offer, "add{$singleProp}")) {
            $className = "App\Entity\\{$singleProp}";
            $entity    = new $className();

            foreach ($value as $el) {
                foreach ($el as $key => $prop) {
                    $capitalizedCollectionProperty = ucfirst($key);

                    if ($entity && method_exists($entity, "set{$capitalizedCollectionProperty}")) {

                        if ($capitalizedCollectionProperty === 'Value' && $capitalizedProperty === 'SalePrices') {
                            $prop = $prop / 100;
                        }

                        $entity->{"set{$capitalizedCollectionProperty}"}($prop);
                    }
                }

                $offer->{"add{$singleProp}"}($entity);
            }
        }
    }

    /**
     * @param CurlHttpClient $client
     * @param string $command
     * @return string
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function getChunkResponse(CurlHttpClient $client, string $command): string
    {
        return $client->request('GET', $this->getParameter('app.mystore.api.baseurl') . $command, [
            'auth_basic' => [
                $this->getParameter('app.mystore.api.login'),
                $this->getParameter('app.mystore.api.password')
            ]
        ])->getContent();
    }

    /**
     * @param Offer $entity
     * @throws ORMException
     * @throws OptimisticLockException
     */
    protected function removeEntity($entity)
    {
        $productOffers = $this->productOfferRepository->findByOffer($entity);

        foreach ($productOffers as $productOffer) {
            $productOffer->setOffer(null);
            $productOffer->setProduct(null);

            $this->em->remove($productOffer);
        }

        $this->em->remove($entity);
        $this->em->flush();
    }
}
