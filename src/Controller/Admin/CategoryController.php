<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 12.04.2020
 * Time: 0:38
 */

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Image;
use App\Entity\Product;
use App\Entity\ProductImage;
use App\Repository\CategoryRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use EasyCorp\Bundle\EasyAdminBundle\Exception\EntityRemoveException;
use Generator;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends EasyAdminController
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param $path
     *
     * @return Generator
     */
    private function readTheFile(string $path): Generator
    {
        $handle = fopen($path, "r");

        while (!feof($handle)) {
            yield fgetcsv($handle, 0, ';');
        }

        fclose($handle);
    }

    public function createCategoryEntityFormBuilder($entity, $view): FormBuilder
    {
        /**
         * @var Category $entity
         */

        $formBuilder = parent::createEntityFormBuilder($entity, $view);
        $categories = $this->categoryRepository->findAll();

        $categoryChoices = [];

        foreach ($categories as $category) {
            $categoryChoices[$category->getPath()] = $category->getId();
        };

        $categoryChoices[''] = null;

        $formBuilder->add('parentId', ChoiceType::class, [
            'choices' => $categoryChoices
        ]);

        return $formBuilder;
    }

    protected function deleteAction()
    {
        $forceDelete = $this->request->get('force', false);
        $this->dispatch(EasyAdminEvents::PRE_DELETE);

        if ('DELETE' !== $this->request->getMethod() && !$forceDelete) {
            return $this->redirect($this->generateUrl('easyadmin', [
                'action' => 'list',
                'entity' => $this->entity['name']
            ]));
        }

        $id   = $this->request->query->get('id');
        $form = $this->createDeleteForm($this->entity['name'], $id);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid() || $forceDelete) {
            $easyadmin = $this->request->attributes->get('easyadmin');
            $entity    = $easyadmin['item'];

            $this->dispatch(EasyAdminEvents::PRE_REMOVE, ['entity' => $entity]);

            try {
                $this->executeDynamicMethod('remove<EntityName>Entity', [$entity, $form]);
            } catch (ForeignKeyConstraintViolationException $e) {
                throw new EntityRemoveException([
                    'entity_name' => $this->entity['name'],
                    'message'     => $e->getMessage()
                ]);
            }

            $this->dispatch(EasyAdminEvents::POST_REMOVE, ['entity' => $entity]);
        }

        $this->dispatch(EasyAdminEvents::POST_DELETE);

        return $this->redirectToReferrer();
    }

    /**
     * @return Response|void
     */
    protected function listAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_LIST);

        $fields    = $this->entity['list']['fields'];
        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->entity['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter']);

        $this->dispatch(EasyAdminEvents::POST_LIST, ['paginator' => $paginator]);

        $er          = $this->em->getRepository(Category::class);
        $productRepo = $this->em->getRepository(Product::class);

        $categories    = $er->findAll();

        usort($categories, function (Category $a, Category $b) {
            return $a->getPosition() - $b->getPosition();
        });

        $nodesNoParent = array_filter($categories, function ($category) {
            return !$category->getParentId();
        });
        $tree          = [];

        foreach ($nodesNoParent as $node) {
            $tree[] = [
                'children' => $this->createTree($categories, $node),
                'category' => $node,
                'products' => $productRepo->findBy(['category' => $node])
            ];
        }

        $parameters = [
            'paginator'            => $paginator,
            'tree'                 => $tree,
            'product_images_path'  => $this->getParameter('app.path.product_images'),
            'fields'               => $fields,
            'batch_form'           => $this->createBatchForm($this->entity['name'])->createView(),
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ];

        return $this->executeDynamicMethod('render<EntityName>Template', [
            'list',
            $this->entity['templates']['list'],
            $parameters
        ]);
    }

    /**
     * @param Category $entity
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    protected function persistEntity($entity)
    {
        $entity->setLevel($this->createLevel($entity));
        $entity->setPath($this->createEntityCategoryPath($entity));

        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * @param Category $entity
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    protected function updateEntity($entity)
    {
        $entity->setLevel($this->createLevel($entity));
        $entity->setPath($this->createEntityCategoryPath($entity));

        $this->em->persist($entity);
        $this->em->flush();
    }


    private function createEntityCategoryPath(Category $entity): ?string
    {
        $path = $entity->getName();

        if ($entity->getParentId()) {
            $er = $this->em->getRepository(Category::class);
            $category = $entity;

            while ($category && ($parentId = $category->getParentId())) {
                $category = $er->findOneBy(['id' => $parentId]);

                $path = $category->getName() . '/' . $path;
            }
        }

        return $path;
    }

    private function createCategoryPath($row, $columnNums, $level): string
    {
        $path = '';

        if (($category1 = array_search('category1', $columnNums)) && $level >= 1) {
            $path .= $row[$category1];
        }

        if (($category2 = array_search('category2', $columnNums)) && $level >= 2) {
            $path .= '/' . $row[$category2];
        }

        if (($category3 = array_search('category3', $columnNums)) && $level >= 3) {
            $path .= '/' . $row[$category3];
        }

        return $path;
    }

    private function createCategory($categoriesData)
    {
        $er = $this->em->getRepository(Category::class);

        $categoriesData = array_filter($categoriesData, function ($item) {
            return $item;
        });

        $level = max(array_keys($categoriesData));
        $name  = $categoriesData[$level]['name'];
        $path  = $categoriesData[$level]['path'];

        $category = $er->findOneBy(['path' => $path]);

        if ($category) {
            return $category;
        }

        $newCategory = new Category();
        $newCategory->setUpdatedAt(new \DateTime('now'));
        $newCategory->setLevel($level);
        $newCategory->setStatus(true);
        $newCategory->setName($name);
        $newCategory->setPath($path);
        $newCategory->setParentId($this->getOrCreateParentCategory($categoriesData, $level - 1));

        $this->em->persist($newCategory);
        $this->em->flush();

        return $newCategory;
    }

    private function getOrCreateParentCategory($categoriesData, $level): ?int
    {
        $parentId = null;
        $er       = $this->em->getRepository(Category::class);

        if ($level > 0) {

            $name = $categoriesData[$level]['name'];
            $path = $categoriesData[$level]['path'];

            $parentCategory = $er->findOneBy(['path' => $path]);

            if (!$parentCategory) {

                $parentCategory = new Category();
                $parentCategory->setUpdatedAt(new \DateTime('now'));
                $parentCategory->setLevel($level);
                $parentCategory->setStatus(true);
                $parentCategory->setName($name);
                $parentCategory->setPath($path);
                $parentCategory->setParentId($this->getOrCreateParentCategory($categoriesData, $level - 1));

                $this->em->persist($parentCategory);
                $this->em->flush();
            }

            $parentId = $parentCategory->getId();
        }

        return $parentId;
    }

    private function createAdditionalImage($url): ?Image
    {
        $fileServer = $this->getParameter('app.path.imports.imageserver');
        $fileUrl    = $fileServer . $url;

        try {
            $fileContent       = file_get_contents($fileUrl);
            $productImage      = new Image();
            $productImagesPath = $this->getParameter('app.path.product_images');
            $rootPath          = $this->getParameter('kernel.project_dir');
            $newFileUrl        = str_replace('/', '_', $url);
            file_put_contents($rootPath . '/public' . $productImagesPath . '/' . $newFileUrl, $fileContent);

            $productImage->setImage($newFileUrl);
            $productImage->setName($newFileUrl);
            $productImage->setUpdatedAt(new \DateTime('now'));

            return $productImage;
        } catch (\Exception $exception) {
            return null;
        }
    }

    private function createImage($url, $main): ?ProductImage
    {
        $fileServer = $this->getParameter('app.path.imports.imageserver');
        $fileUrl    = $fileServer . $url;

        try {
            $fileContent       = file_get_contents($fileUrl);
            $productImage      = new ProductImage();
            $productImagesPath = $this->getParameter('app.path.product_images');
            $rootPath          = $this->getParameter('kernel.project_dir');
            $newFileUrl        = str_replace('/', '_', $url);
            file_put_contents($rootPath . '/public' . $productImagesPath . '/' . $newFileUrl, $fileContent);

            $productImage->setImage($newFileUrl);
            $productImage->setUpdatedAt(new \DateTime('now'));

            return $productImage;
        } catch (\Exception $exception) {
            return null;
        }
    }

    private function createTree(array $categories, $parent): array
    {
        $children = [];

        foreach ($categories as $category) {

            /**
             * @var Category $category
             * @var Category $parent
             */
            if ($category->getParentId() === $parent->getId()) {
                $child = [
                    'category' => $category,
                    'children' => $this->createTree($categories, $category)
                ];

                $productRepo       = $this->em->getRepository(Product::class);
                $child['products'] = $productRepo->findBy(['category' => $category]);

                $children[] = $child;

            }
        }

        return $children;
    }

    private function createLevel($entity): ?int
    {
        $level = 1;

        if ($entity->getParentId()) {
            $er             = $this->em->getRepository('App\Entity\Category');
            $parentCategory = $er->find($entity->getParentId());

            $level = $parentCategory->getLevel() + 1;
        }

        return $level;
    }
}
