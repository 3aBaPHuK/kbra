<?php

namespace App\Controller\Admin;

use App\Entity\Offer;
use App\Entity\OfferColor;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

class OfferColorController extends EasyAdminController
{
    /**
     * @param OfferColor $entity
     */
    protected function removeEntity($entity)
    {
        $offers = $entity->getOffer()->toArray();

        foreach ($offers as $offer) {
            /** @var Offer $offer */
            $offer->setOfferColor(null);
        }

        $this->em->remove($entity);
        $this->em->flush();
    }
}
