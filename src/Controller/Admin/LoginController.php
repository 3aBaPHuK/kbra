<?php

namespace App\Controller\Admin;

use App\Security\LoginFormAuthentificatorAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends AbstractController
{
    /**
     * @Route(name="admin_login", path="/admin/login")
     */
    public function index(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('@EasyAdmin/page/login.html.twig', [
            'error' => $error,
            'last_username' => $lastUsername,
            'page_title' => 'lodki.shop',
            'csrf_token_intention' => 'authenticate',
            'target_path' => '/backoffice',
            'username_label' => 'Логин',
            'password_label' => 'Пароль',
            'sign_in_label' => 'Войти',
        ]);
    }
}
