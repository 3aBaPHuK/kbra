<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 26.04.2020
 * Time: 14:27
 */

namespace App\Controller\Admin;

use App\Entity\Menu;
use App\Entity\Product;
use App\Repository\OfferRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use EasyCorp\Bundle\EasyAdminBundle\Exception\EntityRemoveException;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilder;

class ProductController extends EasyAdminController
{
    /**
     * @var OfferRepository
     */
    private $offerRepository;

    public function __construct(OfferRepository $offerRepository)
    {
        $this->offerRepository = $offerRepository;
    }

    /**
     * @param Product $entity
     */
    protected function persistEntity($entity)
    {
        foreach ($entity->getProductProperties() as $productProperty) {
            $productProperty->setProduct($entity);
        }

        foreach ($entity->getProductEquipments() as $productEquipment) {
            $productEquipment->setProduct($entity);
        }

        $this->em->persist($entity);
        $this->em->flush();
    }

    protected function updateEntity($entity)
    {
        foreach ($entity->getProductProperties() as $productProperty) {
            $productProperty->setProduct($entity);
        }

        foreach ($entity->getProductEquipments() as $productEquipment) {
            $productEquipment->setProduct($entity);
        }

        $this->em->flush();
    }

    protected function removeEntity($entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

    protected function deleteAction()
    {
        $forceDelete = $this->request->get('force', false);
        $this->dispatch(EasyAdminEvents::PRE_DELETE);

        if ('DELETE' !== $this->request->getMethod() && !$forceDelete) {
            return $this->redirect($this->generateUrl('easyadmin', [
                'action' => 'list',
                'entity' => $this->entity['name']
            ]));
        }

        $id   = $this->request->query->get('id');
        $form = $this->createDeleteForm($this->entity['name'], $id);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid() || $forceDelete) {
            $easyadmin = $this->request->attributes->get('easyadmin');
            $entity    = $easyadmin['item'];

            $this->dispatch(EasyAdminEvents::PRE_REMOVE, ['entity' => $entity]);

            try {
                $this->executeDynamicMethod('remove<EntityName>Entity', [$entity, $form]);
            } catch (ForeignKeyConstraintViolationException $e) {
                throw new EntityRemoveException([
                    'entity_name' => $this->entity['name'],
                    'message'     => $e->getMessage()
                ]);
            }

            $this->dispatch(EasyAdminEvents::POST_REMOVE, ['entity' => $entity]);
        }

        $this->dispatch(EasyAdminEvents::POST_DELETE);

        return $this->redirectToReferrer();
    }
}
