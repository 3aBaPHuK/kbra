<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

class ArticleController extends EasyAdminController
{
    protected function persistEntity($entity)
    {
        $this->updateRelations($entity);
    }

    private function updateRelations(Article $article)
    {
        foreach ($article->getGallery() as $gallery) {
            $gallery->setArticle($article);
        }

        $this->em->persist($article);
        $this->em->flush();
    }


    protected function updateEntity($entity)
    {
        $this->updateRelations($entity);
    }

    protected function removeEntity($entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }
}