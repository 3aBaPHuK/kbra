<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\ProductOffer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findByExternalId(string $externalId): ?Product
    {
        return $this->findOneBy(['externalId' => $externalId]);
    }

    public function save(Product $product): Product
    {
        $this->_em->persist($product);
        $this->_em->flush();

        return $product;
    }

    public function findByOfferId($offerId): array
    {
        $qb = $this->createQueryBuilder('p');

        return $qb->select()
            ->leftJoin('p.productOffers', 'po')
            ->leftJoin('po.offer', 'o')
            ->andWhere('o.id = '.$offerId)
            ->getQuery()
            ->getResult();
    }

    public function findBySlugAndCategory(string $slug, string $categorySlug): ?Product
    {
        return $this->createQueryBuilder('p')
            ->leftJoin('p.category','c')
            ->leftJoin('p.productOffers','po')
            ->leftJoin('po.offer','o')
            ->where('c.slug = :categorySlug')
            ->andWhere('p.slug = :slug')
            ->andWhere('p.active = true')
            ->andWhere('o.active = true')
            ->setParameters(['slug' => $slug, 'categorySlug' => $categorySlug])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findCheapestPriceOfCategory(int $categoryId): ?float
    {
        $categoryProducts = $this->findBy(['category' => $categoryId]);

        if (!$categoryProducts) {
            return null;
        }

        if (empty($categoryProducts[0]->getProductOffers()->first())) {
            return null;
        }

        $cheapestPrice = $categoryProducts[0]->getProductOffers()->first()->getOffer()->getDiscountPrice() ?? $categoryProducts[0]->getProductOffers()->first()->getOffer()->getPrice();

        foreach ($categoryProducts as $product)
            foreach ($product->getProductOffers() as $productOffer) {
                /** @var ProductOffer $productOffer */

                $price = $productOffer->getOffer()->getDiscountPrice() ?? $productOffer->getOffer()->getPrice();

                if ($price <= $cheapestPrice) {
                    $cheapestPrice = $price;
                }
            }

        return $cheapestPrice;
    }

    public function findByCategoryIdsList(array $categoryIds): ?array
    {
        return $this->createQueryBuilder('p')
            ->where('p.category IN(:ids)')
            ->andWhere('p.active = true')
            ->setParameters(['ids' => $categoryIds])
            ->getQuery()
            ->getResult();
    }

    public function findByIdList(array $ids): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.id IN(:ids)')
            ->andWhere('p.active = true')
            ->setParameters(['ids' => $ids])
            ->getQuery()
            ->getResult();
    }
}
