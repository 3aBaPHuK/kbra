<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function findByExternalId(string $externalId): ?Category
    {
        return $this->findOneBy(['externalId' => $externalId]);
    }

    public function findBySlug($slug): ?Category
    {
        return $this->findOneBy(['slug' => $slug]);
    }

    public function findChildCategoryIds(string $slug): ?array
    {
        $parent = $this->findBySlug($slug);

        if (!$parent) {
            return null;
        }

        $connection = $this->getEntityManager()->getConnection();

        $res = $connection->fetchFirstColumn('SELECT * FROM category
            WHERE parent_id = ' . $parent->getId() . '
            UNION SELECT * FROM category
            WHERE parent_id IN (SELECT id FROM category WHERE parent_id = ' . $parent->getId() . ')'
        );

        $res[] = $parent->getId();

        return $res;
    }

    public function save(Category $category): Category
    {
        $this->_em->persist($category);
        $this->_em->flush();

        return $category;
    }

    public function getMaxRightKey()
    {
        $categories = $this->findAll();

        return max(array_map(function (Category $category) {
            return $category->getRightKey();
        }, $categories) ?: [0]);
    }

    public function updateNextTreeKeys($rightKey)
    {
        $qb = $this->createQueryBuilder('c');

        return $qb->update()
            ->set('c.leftKey', 'c.leftKey + 2')
            ->set('c.rightKey', 'c.rightKey + 2')
            ->where('c.leftKey > ' . $rightKey)
            ->getQuery()
            ->getResult();
    }

    public function updateParentTreeKeys($rightKey, $leftKey)
    {
        $qb = $this->createQueryBuilder('c');

        return $qb->update()
            ->set('c.rightKey', 'c.rightKey + 2')
            ->where('c.rightKey >= ' . $rightKey . ' AND c.leftKey <= ' . $leftKey)
            ->getQuery()
            ->getResult();
    }

    public function findAllSort(array $sort)
    {
        return $this->createQueryBuilder('a')
            ->orderBy('a.' . array_key_first($sort), $sort[array_key_first($sort)])
            ->getQuery()
            ->getResult();
    }

    public function updatePositions(array $data)
    {
        $connection = $this->getEntityManager()->getConnection();

        foreach ($data as $category) {
            $q = 'UPDATE category';
            $q .= ' SET level=' . $category['level'];
            $q .= ', position=' . $category['position'];
            $q .= ", path='" . $this->createEntityCategoryPath($this->find($category['categoryId'])) . "'";
            $q .= ', parent_id=' . ($category['parentId'] ?? 'null');

            $q .= ' WHERE id=' . $category['categoryId'];

            $connection->executeQuery($q);
        }
    }

    private function createEntityCategoryPath(Category $entity): ?string
    {
        $path = $entity->getName();

        if ($entity->getParentId()) {
            $er = $this->_em->getRepository(Category::class);
            $category = $entity;

            while ($category && ($parentId = $category->getParentId())) {
                $category = $er->findOneBy(['id' => $parentId]);

                $path = $category->getName() . '/' . $path;
            }
        }

        return $path;
    }
}
