<?php

namespace App\Repository;

use App\Entity\OfferColor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OfferColor|null find($id, $lockMode = null, $lockVersion = null)
 * @method OfferColor|null findOneBy(array $criteria, array $orderBy = null)
 * @method OfferColor[]    findAll()
 * @method OfferColor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferColorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OfferColor::class);
    }

    // /**
    //  * @return OfferColor[] Returns an array of OfferColor objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OfferColor
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
