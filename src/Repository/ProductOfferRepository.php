<?php

namespace App\Repository;

use App\Entity\Offer;
use App\Entity\Product;
use App\Entity\ProductOffer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductOffer|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductOffer|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductOffer[]    findAll()
 * @method ProductOffer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductOfferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductOffer::class);
    }

    /**
     * @param Offer $offer
     * @return ProductOffer[]
     */
    public function findByOffer(Offer $offer): array
    {
        return $this->findBy(['offer' => $offer]);
    }

    public function findByProductAndOffer(Product $product, Offer $offer): ?ProductOffer
    {
        return $this->findOneBy(['product' => $product, 'offer' => $offer]);
    }

    public function save(ProductOffer $productOffer): ProductOffer
    {
        $this->_em->persist($productOffer);
        $this->_em->flush();

        return $productOffer;
    }
}
