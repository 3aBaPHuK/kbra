<?php

namespace App\Repository;

use App\Entity\ProductImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductImage[]    findAll()
 * @method ProductImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductImageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductImage::class);
    }

    public function save(ProductImage $productImage)
    {
        $this->_em->beginTransaction();
        $this->_em->persist($productImage);
        $this->_em->flush();
        $this->_em->commit();

        return $productImage;
    }

    public function removeImage(ProductImage $productImage)
    {
        $this->_em->remove($productImage);
        $this->_em->flush();
    }

    public function findByProductExternalIdAndImage(string $productExternalId, string $filePath): ?ProductImage
    {
        $connection = $this->getEntityManager()->getConnection();

        $res = $connection->fetchOne(
            'SELECT pi.id FROM product_image pi
            LEFT JOIN product AS p ON pi.product_id = p.id
            WHERE p.external_id=:extId AND pi.image=:img LIMIT 1',
            ['extId' => $productExternalId, 'img' => $filePath]
        );

        return $this->find($res);
    }
}
