<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\ProductProperty;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductProperty|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductProperty|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductProperty[]    findAll()
 * @method ProductProperty[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductPropertyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductProperty::class);
    }

    public function createFromArray(array $productPropertyArray, Product $product): ProductProperty
    {
        $productProperty = new ProductProperty();
        $productProperty->setLabel($productPropertyArray['label']);
        $productProperty->setValue($productPropertyArray['value']);
        $productProperty->setProduct($product);

        $this->_em->persist($productProperty);
        $this->_em->flush();

        return $productProperty;
    }

    public function findByProductAndLabel(Product $product, string $label): ?ProductProperty
    {
        return $this->findOneBy(['label' => $label, 'product' => $product]);
    }

    public function save(ProductProperty $productProperty)
    {
        $this->_em->persist($productProperty);
        $this->_em->flush();

        return $productProperty;
    }
}
