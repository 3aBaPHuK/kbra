<?php

namespace App\Repository;

use App\Entity\Offer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Offer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Offer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Offer[]    findAll()
 * @method Offer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Offer::class);
    }

    public function findByExternalCode(string $externalCode): ?Offer
    {
        return $this->findOneBy(['externalCode' => $externalCode]);
    }

    public function save(Offer $offer)
    {
        $this->_em->persist($offer);
        $this->_em->flush();
    }

    public function findAllIdNamesArray(): array
    {
        return $this->createQueryBuilder('o')->select(['o.id', 'o.name'])->getQuery()->getArrayResult();
    }
}
