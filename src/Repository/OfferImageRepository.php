<?php

namespace App\Repository;

use App\Entity\OfferImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OfferImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method OfferImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method OfferImage[]    findAll()
 * @method OfferImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferImageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OfferImage::class);
    }

    // /**
    //  * @return OfferImage[] Returns an array of OfferImage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OfferImage
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
