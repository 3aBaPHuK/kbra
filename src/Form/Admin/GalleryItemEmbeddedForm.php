<?php

namespace App\Form\Admin;

use App\Entity\GalleryItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class GalleryItemEmbeddedForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['required' => true, 'label' => 'Заголовок'])
            ->add('imageFile', VichImageType::class, ['required' => true, 'label' => 'Изображение'])
            ->add('href', TextType::class, ['required' => true, 'label' => 'Ссылка'])
            ->add('position', HiddenType::class, ['required' => false, 'label' => 'ADMIN_POSITION_FIELD']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => GalleryItem::class,
        ));
    }
}
