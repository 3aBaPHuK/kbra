<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 18.04.2020
 * Time: 16:23
 */

namespace App\Form\Admin;

use App\Entity\OfferImage;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OfferImageEmbeddedForm extends ImageEmbeddedForm
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => OfferImage::class,
        ));
    }
}
