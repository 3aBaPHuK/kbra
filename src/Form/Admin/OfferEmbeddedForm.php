<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 18.04.2020
 * Time: 16:23
 */

namespace App\Form\Admin;

use App\Entity\Offer;
use App\Entity\ProductOffer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OfferEmbeddedForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('displayName', TextType::class, [
                'label' => 'ADMIN_PRODUCT_OFFERS_FORM_DISPLAY_NAME'
            ])
            ->add('offer', EntityType::class, [
                    'label'        => 'ADMIN_PRODUCT_OFFERS_FORM_OFFER',
                    'choice_label' => 'name',
                    'class'        => Offer::class,
                    'attr'         => [
                        'data-widget'     => 'select2',
                        'tabindex'        => -1,
                    ]
                ]
            )
            ->add('position', HiddenType::class, ['required' => true, 'label' => 'ADMIN_LABEL_POSITION']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ProductOffer::class,
        ));
    }
}
