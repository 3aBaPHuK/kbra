<?php

namespace App\Form\Admin;

use App\Entity\ProductGalleryImage;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductGalleryImageEmbeddedForm extends ImageEmbeddedForm
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ProductGalleryImage::class,
        ));
    }
}
