<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 02.05.2020
 * Time: 23:50
 */

namespace App\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SalePricesEmbeddedForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('priceType', TextType::class, [
                'label' => 'SALE_PRICE_TYPE'
            ])
            ->add('value', TextType::class, [
                'label' => 'SALE_PRICE_VALUE'
            ]);
    }
}