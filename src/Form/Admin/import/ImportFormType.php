<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 19.04.2020
 * Time: 15:49
 */

namespace App\Form\Admin\import;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class ImportFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class)
            ->add('next', SubmitType::class);
    }
}