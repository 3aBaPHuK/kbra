<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 18.04.2020
 * Time: 16:23
 */

namespace App\Form\Admin;

use App\Entity\ProductImage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ImageEmbeddedForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('imageFile', VichImageType::class, [
                    'required'     => false,
                    'allow_delete' => false,
                ]
            )
            ->add('position', HiddenType::class, ['required' => true, 'label' => 'ADMIN_LABEL_POSITION']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ProductImage::class,
        ));
    }
}
