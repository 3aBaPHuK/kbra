<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 30.08.2020
 * Time: 14:46
 */

namespace App\Form\Admin;

use App\Entity\ProductProperties;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyPath;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PaymentInvoiceTemplateType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => $this->getTemplates(),
        ]);
    }

    private function getTemplates()
    {
        $finder = new Finder();

        $templates = [];

        $founded = $finder->files()->in(realpath($_SERVER['DOCUMENT_ROOT'] . '/../templates/shop/payment'));

        foreach ($founded as $file) {
            $templates[$file->getRelativePathname()] = $file->getRelativePathname();
        }

        return $templates;
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}