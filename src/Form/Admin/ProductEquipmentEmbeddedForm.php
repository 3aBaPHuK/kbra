<?php

namespace App\Form\Admin;

use App\Entity\ProductEquipmentItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductEquipmentEmbeddedForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label', TextType::class, ['required' => true, 'label' => 'PRODUCT_PROPERTIES_LABEL'])
            ->add('value', TextType::class, ['required' => true, 'label' => 'PRODUCT_PROPERTIES_VALUE'])
            ->add('position', HiddenType::class, ['required' => true, 'label' => 'ADMIN_LABEL_POSITION']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ProductEquipmentItem::class,
        ));
    }
}
