<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 02.08.2020
 * Time: 19:32
 */

namespace App\Form\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Form\Type\TextEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductPropertiesForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('article', TextType::class, ['required' => false, 'label' => 'PRODUCT_PROPERTIES_ARTICLE'])
            ->add('bonus', IntegerType::class, ['required' => false, 'label' => 'PRODUCT_PROPERTIES_BONUS'])
            ->add('instructions', TextEditorType::class, [
                'required' => false,
                'label'    => 'PRODUCT_PROPERTIES_INSTRUCTIONS'
            ])
            ->add('parameters', TextEditorType::class, [
                'required' => false,
                'label'    => 'PRODUCT_PROPERTIES_PARAMETERS'
            ])
            ->add('examples', TextEditorType::class, ['required' => false, 'label' => 'PRODUCT_PROPERTIES_EXAMPLES'])
            ->add('size', TextType::class, ['required' => false, 'label' => 'PRODUCT_PROPERTIES_SIZE'])
            ->add('rating', IntegerType::class, ['required' => false, 'label' => 'PRODUCT_PROPERTIES_RATING']);
    }
}