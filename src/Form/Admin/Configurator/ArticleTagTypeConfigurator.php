<?php

namespace App\Form\Admin\Configurator;

use App\Entity\Article;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\Configurator\TypeConfiguratorInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormConfigInterface;

class ArticleTagTypeConfigurator implements TypeConfiguratorInterface
{
    /**
     * {@inheritdoc}
     */
    public function configure($name, array $options, array $metadata, FormConfigInterface $parentConfig): array
    {
        if ($parentConfig->getData() instanceof Article) {
            $options['choices'] = Article::getAvailableTags();
        }

        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function supports($type, array $options, array $metadata): bool
    {
        return in_array($type, ['choice', ChoiceType::class], true);
    }
}