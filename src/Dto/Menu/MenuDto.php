<?php

namespace App\Dto\Menu;

use App\Entity\Menu;
use JsonSerializable;

class MenuDto implements JsonSerializable
{
    private $id;
    private $title;
    private $route;
    private $params = [];
    private $type;

    private function __construct()
    {
    }

    public static function fromMenuEntity(Menu $menuItem): MenuDto
    {
        $object = new self();
        $object->id = $menuItem->getId();
        $object->title = $menuItem->getTitle();
        $object->route = $menuItem->getRoute();
        $object->params = $menuItem->getParams();
        $object->type = $menuItem->getType();

        return $object;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}