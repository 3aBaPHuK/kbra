<?php


namespace App\Dto\Menu;


use App\Dto\Common\AbstractCollection;

class MenuItemsCollection extends AbstractCollection
{
    public function __construct(MenuDto ...$values)
    {
        parent::__construct(...$values);
    }
}