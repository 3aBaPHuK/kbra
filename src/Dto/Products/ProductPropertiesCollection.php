<?php

namespace App\Dto\Products;

use App\Dto\Common\AbstractCollection;

class ProductPropertiesCollection extends AbstractCollection
{
    public function __construct(ProductPropertyDto ...$values)
    {
        parent::__construct(...$values);
    }
}