<?php

namespace App\Dto\Products;

use App\Entity\Offer;
use App\Entity\OfferImage;
use JsonSerializable;

class OfferDto implements JsonSerializable
{
    private $id;
    private $name;
    private $cover;
    private $images;
    private $displayName;
    private $price;
    private $discountPrice;
    private $quantity;
    private $color;

    private function __construct(
        int $id,
        string $name,
        ?string $cover,
        array $images,
        string $displayName,
        float $price,
        float $discountPrice,
        int $quantity,
        ?string $color
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->images = $images;
        $this->displayName = $displayName;
        $this->price = $price;
        $this->discountPrice = $discountPrice;
        $this->quantity = $quantity;
        $this->color = $color;
        $this->cover = $cover;
    }

    public static function fromOfferEntity(Offer $offer, string $displayName, $imageBasePath = ''): OfferDto
    {
        return new self(
            $offer->getId(),
            $offer->getName(),
            $offer->getCover() ? $imageBasePath . DIRECTORY_SEPARATOR . $offer->getCover() : null,
            array_map(function (OfferImage $imageEntity) use ($imageBasePath) {
                return $imageBasePath . DIRECTORY_SEPARATOR . $imageEntity->getImage();
            }, $offer->getOfferImages()->toArray()),
            $displayName,
            $offer->getPrice() ?? 0,
            $offer->getDiscountPrice() ?? 0,
            $offer->getQuantity() ?? 0,
            $offer->getOfferColor() ? $imageBasePath . DIRECTORY_SEPARATOR . $offer->getOfferColor()->getIcon() : null
        );
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
