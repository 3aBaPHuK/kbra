<?php

namespace App\Dto\Products;

use App\Dto\Common\AbstractCollection;

class ProductsCollection extends AbstractCollection
{
    public function __construct(...$values)
    {
        parent::__construct(...$values);
    }
}