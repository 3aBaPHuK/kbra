<?php

namespace App\Dto\Products;

use App\Entity\ProductProperty;
use JsonSerializable;

class ProductPropertyDto implements JsonSerializable
{
    private $label;
    private $value;

    private function __construct(string $label, string $value)
    {
        $this->label = $label;
        $this->value = $value;
    }

    public static function fromProductPropertyEntity(ProductProperty $productProperty): ProductPropertyDto
    {
        return new self($productProperty->getLabel(), $productProperty->getValue());
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}