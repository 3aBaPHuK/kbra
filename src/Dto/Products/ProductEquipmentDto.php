<?php

namespace App\Dto\Products;

use App\Entity\ProductEquipmentItem;
use App\Entity\ProductProperty;
use JsonSerializable;

class ProductEquipmentDto implements JsonSerializable
{
    private $label;
    private $value;

    private function __construct(string $label, string $value)
    {
        $this->label = $label;
        $this->value = $value;
    }

    public static function fromEntity(ProductEquipmentItem $productEquipmentItem): self
    {
        return new self($productEquipmentItem->getLabel(), $productEquipmentItem->getValue());
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
