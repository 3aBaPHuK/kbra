<?php

namespace App\Dto\Products;

use App\Entity\Offer;
use App\Entity\Product;
use App\Entity\ProductEquipmentItem;
use App\Entity\ProductGalleryImage;
use App\Entity\ProductImage;
use App\Entity\ProductOffer;
use App\Entity\ProductProperty;
use JsonSerializable;

final class ProductDto implements JsonSerializable
{
    private $id;
    private $name;
    private $cover;
    private $images;
    private $gallery;
    private $sketch;
    private $announce;
    private $description;
    private $canonical;
    private $offers;
    private $properties;
    private $equipments;
    private $metaTitle;
    private $metaDescription;
    private $metaKeywords;
    private $categoryName;
    private $categorySlug;

    private function __construct(
        int                         $id,
        string                      $name,
        ?string                     $cover,
        array                       $images,
        array                       $gallery,
        ?string                     $sketch,
        ?string                     $announce,
        ?string                     $description,
        string                      $canonical,
        string                      $categoryName,
        string                      $categorySlug,
        ProductPropertiesCollection $productPropertiesCollection,
        ProductEquipmentsCollection $productEquipmentsCollection,
        OffersCollection            $offers,
        ?string                     $metaTitle,
        ?string                     $metaDescription,
        ?string                     $metaKeywords
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->cover = $cover;
        $this->images = $images;
        $this->gallery = $gallery;
        $this->sketch = $sketch;
        $this->announce = $announce;
        $this->description = $description;
        $this->canonical = $canonical;
        $this->properties = $productPropertiesCollection;
        $this->offers = $offers;
        $this->metaTitle = $metaTitle;
        $this->metaDescription = $metaDescription;
        $this->metaKeywords = $metaKeywords;
        $this->equipments = $productEquipmentsCollection;
        $this->categoryName = $categoryName;
        $this->categorySlug = $categorySlug;
    }

    /**
     * @param Product $product
     * @return ProductPropertyDto[]|array
     */
    private static function sortProperties(Product $product): array
    {
        $properties = $product->getProductProperties()->getValues();

        usort($properties, function (ProductProperty $propertyA, ProductProperty $propertyB) {
            return $propertyA->getPosition() > $propertyB->getPosition();
        });

        return array_map(function (ProductProperty $property) {
            return ProductPropertyDto::fromProductPropertyEntity($property);
        }, $properties);
    }

    /**
     * @param Product $product
     * @return ProductEquipmentDto[]|array
     */
    private static function sortEquipments(Product $product): array
    {
        $equipments = $product->getProductEquipments()->getValues();

        usort($equipments, function (ProductEquipmentItem $equipmentA, ProductEquipmentItem $equipmentB) {
            return $equipmentA->getPosition() > $equipmentB->getPosition();
        });

        return array_map(function (ProductEquipmentItem $equipment) {
            return ProductEquipmentDto::fromEntity($equipment);
        }, $equipments);
    }

    private static function sortByPosition(array $items): array
    {
        usort($items, function ($itemA, $itemB) {
            return $itemA->getPosition() > $itemB->getPosition();
        });

        return $items;
    }

    public static function fromProductEntity(Product $product, $imagePath = ''): ProductDto
    {
        $cover = $product->getCover();
        $productOffers = self::sortByPosition($product->getProductOffers()->toArray());
        $props = self::sortProperties($product);
        $equipments = self::sortEquipments($product);
        $imagesCollection = self::sortByPosition($product->getImageCollection()->toArray());
        $galleryImagesCollection = self::sortByPosition($product->getGalleryImagesCollection()->toArray());

        $equipmentsCollection = new ProductEquipmentsCollection(...$equipments);
        $offers = array_map(function (ProductOffer $productOffer) use ($imagePath) {
            return OfferDto::fromOfferEntity(
                $productOffer->getOffer(),
                $productOffer->getDisplayName() ?? '',
                $imagePath
            );
        }, $productOffers);

        $offersCollection = new OffersCollection(...$offers);
        $propsCollection = new ProductPropertiesCollection(...$props);


        return new self(
            $product->getId(),
            $product->getName() ?? '',
            $cover ? $imagePath . DIRECTORY_SEPARATOR . $product->getCover() : null,
            array_map(function (ProductImage $imageEntity) use ($imagePath) {
                return $imagePath . DIRECTORY_SEPARATOR . $imageEntity->getImage();
            }, $imagesCollection),
            array_map(function (ProductGalleryImage $imageEntity) use ($imagePath) {
                return $imagePath . DIRECTORY_SEPARATOR . $imageEntity->getImage();
            }, $galleryImagesCollection),
            $product->getSketch(),
            $product->getAnnounce(),
            $product->getDescription(),
            $product->getSlug(),
            $product->getCategory()->getName(),
            $product->getCategory()->getSlug(),
            $propsCollection,
            $equipmentsCollection,
            $offersCollection,
            $product->getMetaTitle() ?? '',
            $product->getMetaDescription() ?? '',
            $product->getMetaKeywords() ?? ''
        );
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
