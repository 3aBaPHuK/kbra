<?php

namespace App\Dto\Products;

use App\Dto\Common\AbstractCollection;

class ProductEquipmentsCollection extends AbstractCollection
{
    public function __construct(ProductEquipmentDto ...$values)
    {
        parent::__construct(...$values);
    }
}
