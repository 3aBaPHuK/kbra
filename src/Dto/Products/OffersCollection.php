<?php

namespace App\Dto\Products;

use App\Dto\Common\AbstractCollection;

class OffersCollection extends AbstractCollection
{
    public function __construct(OfferDto ...$values)
    {
        parent::__construct(...$values);
    }
}