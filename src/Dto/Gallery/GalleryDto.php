<?php

namespace App\Dto\Gallery;

use App\Entity\Gallery;
use App\Entity\GalleryItem;
use JsonSerializable;

class GalleryDto implements JsonSerializable
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $description;

    private function __construct()
    {
    }

    public static function fromGalleryEntity(Gallery $gallery): GalleryDto
    {
        $object = new self();

        $object->id = $gallery->getId();
        $object->title = $gallery->getTitle();
        $object->type = $gallery->getType();
        $object->description = $gallery->getDescription();

        return $object;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}