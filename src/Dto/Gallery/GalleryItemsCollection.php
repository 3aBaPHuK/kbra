<?php

namespace App\Dto\Gallery;

use App\Dto\Common\AbstractCollection;

class GalleryItemsCollection extends AbstractCollection
{
    public function __construct(GalleryItemDto ...$values)
    {
        parent::__construct(...$values);
    }
}