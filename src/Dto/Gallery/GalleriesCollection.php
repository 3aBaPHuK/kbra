<?php

namespace App\Dto\Gallery;

use App\Dto\Common\AbstractCollection;

class GalleriesCollection extends AbstractCollection
{
    public function __construct(GalleryDto ...$values)
    {
        parent::__construct(...$values);
    }
}