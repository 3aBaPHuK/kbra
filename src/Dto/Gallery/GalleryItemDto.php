<?php

namespace App\Dto\Gallery;

use App\Entity\GalleryItem;
use JsonSerializable;

class GalleryItemDto implements JsonSerializable
{
    private $id;
    private $title;
    private $image;
    private $href;

    private function __construct()
    {
    }

    public static function fromGalleryItemEntity(GalleryItem $entity, string $basePath): GalleryItemDto
    {
        $object = new self();

        $object->id = $entity->getId();
        $object->title = $entity->getTitle();
        $object->href = $entity->getHref();
        $object->image = $basePath.DIRECTORY_SEPARATOR.$entity->getImage();

        return $object;
    }

    public function jsonSerialize(): array
    {
        return array_filter(get_object_vars($this));
    }
}