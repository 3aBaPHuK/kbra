<?php

namespace App\Dto\Articles;

use App\Dto\Gallery\GalleryDto;
use App\Entity\Article;
use App\Entity\Gallery;
use JsonSerializable;

class ArticleDto implements JsonSerializable
{
    private $id;
    private $name;
    private $slug;
    private $announce;
    private $description;
    private $image;
    private $gallery;
    private $metaTitle;
    private $metaDescription;
    private $metaKeywords;

    private function __construct()
    {
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public static function fromArticleEntity(Article $article, string $baseImagePath = ''): ArticleDto
    {
        $articleDto = new self();

        $articleDto->setId($article->getId());
        $articleDto->setName($article->getName());
        $articleDto->setAnnounce($article->getAnnounce());
        $articleDto->setImage($article->getImage() ? $baseImagePath.DIRECTORY_SEPARATOR.$article->getImage() : null);
        $articleDto->setDescription($article->getDescription());
        $articleDto->setMetaTitle($article->getMetaTitle());
        $articleDto->setMetaDescription($article->getMetaDescription());
        $articleDto->setMetaKeywords($article->getMetaKeywords());
        $articleDto->setSlug($article->getSlug());

        if ($galleries = $article->getGallery()) {
            $articleDto->gallery = array_map(function (Gallery $gallery) {
                return GalleryDto::fromGalleryEntity($gallery);
            }, $galleries->toArray());
        }

        return $articleDto;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAnnounce()
    {
        return $this->announce;
    }

    /**
     * @param mixed $announce
     */
    public function setAnnounce($announce): void
    {
        $this->announce = $announce;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * @return mixed
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * @param mixed $metaTitle
     */
    public function setMetaTitle($metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @return mixed
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param mixed $metaDescription
     */
    public function setMetaDescription($metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return mixed
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * @param mixed $metaKeywords
     */
    public function setMetaKeywords($metaKeywords): void
    {
        $this->metaKeywords = $metaKeywords;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }
}
