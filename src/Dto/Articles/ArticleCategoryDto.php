<?php

namespace App\Dto\Articles;

use App\Entity\ArticleCategory;
use JsonSerializable;

class ArticleCategoryDto implements JsonSerializable
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var ArticlesCollection[]
     */
    private $articles = [];

    private function __construct(string $name, string $slug, ArticlesCollection $articles)
    {
        $this->name     = $name;
        $this->slug     = $slug;
        $this->articles = $articles;
    }

    public static function fromEntity(ArticleCategory $category, $baseImagePath = ''): ArticleCategoryDto
    {

        $categoryArticles = $category->getArticles()->toArray();
        $articlesCollection = new ArticlesCollection(
            ...array_map(
                function ($article) use ($baseImagePath) {
                    return ArticleDto::fromArticleEntity($article, $baseImagePath);
                },
                $categoryArticles
            )
        );

        return new self($category->getName(), $category->getSlug(), $articlesCollection);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return ArticlesCollection
     */
    public function getArticles(): ArticlesCollection
    {
        return $this->articles;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}