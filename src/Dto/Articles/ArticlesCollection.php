<?php

namespace App\Dto\Articles;

use App\Dto\Common\AbstractCollection;

class ArticlesCollection extends AbstractCollection
{
    public function __construct(ArticleDto ...$values)
    {
        parent::__construct(...$values);
    }
}