<?php

namespace App\Dto\Categories;

use App\Dto\Common\AbstractCollection;

class CategoriesCollection extends AbstractCollection
{
    public function __construct(CategoryDto ...$values)
    {
        parent::__construct(...$values);
    }
}