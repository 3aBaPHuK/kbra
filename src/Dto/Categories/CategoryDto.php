<?php

namespace App\Dto\Categories;

use App\Entity\Category;
use JsonSerializable;

final class CategoryDto implements JsonSerializable
{

    private $categoryId;

    private $name;

    private $parentId = null;

    private $level;

    private $image = null;

    private $description;

    private $short;

    private $canonical;

    private $metaDescription;

    private $metaTitle;

    private $metaKeywords;

    private $cheapestPrice;

    private function __construct()
    {
    }

    public static function fromCategoryEntity(Category $category, string $imageDirPath, ?float $cheapestPrice = null): CategoryDto
    {
        $object = new self();

        $object->setCategoryId($category->getId());
        $object->setName($category->getName());
        $object->setLevel($category->getLevel());
        $object->setDescription($category->getDescription() ?? '');
        $object->setShort($category->getShort());
        $object->setParentId($category->getParentId());
        $object->setImage(!empty($category->getImage()) ? $imageDirPath.DIRECTORY_SEPARATOR.$category->getImage() : '');
        $object->setCanonical($category->getSlug() ?? '');
        $object->setMetaTitle($category->getMetaTitle() ?? '');
        $object->setMetaDescription($category->getMetaDescription() ?? '');
        $object->setMetaKeywords($category->getMetaKeywords() ?? '');
        $object->setCheapestPrice($cheapestPrice);

        return $object;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getParentId(): int
    {
        return $this->parentId;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getCanonical(): string
    {
        return $this->canonical;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId(int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param int|null $parentId
     */
    public function setParentId(int $parentId = null): void
    {
        $this->parentId = $parentId;
    }

    /**
     * @param int $level
     */
    public function setLevel(int $level): void
    {
        $this->level = $level;
    }

    /**
     * @param string|null $image
     */
    public function setImage(string $image = null): void
    {
        $this->image = $image;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @param string $canonical
     */
    public function setCanonical(string $canonical): void
    {
        $this->canonical = $canonical;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return mixed
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param mixed $metaDescription
     */
    public function setMetaDescription($metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return mixed
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * @param mixed $metaTitle
     */
    public function setMetaTitle($metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @return mixed
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * @param mixed $metaKeywords
     */
    public function setMetaKeywords($metaKeywords): void
    {
        $this->metaKeywords = $metaKeywords;
    }

    public function getCheapestPrice(): ?float
    {
        return $this->cheapestPrice;
    }

    public function setCheapestPrice(?float $cheapestPrice): void
    {
        $this->cheapestPrice = $cheapestPrice;
    }

    public function getShort(): ?string
    {
        return $this->short;
    }

    public function setShort(?string $short): void
    {
        $this->short = $short;
    }
}
