<?php

namespace App\Dto\Shop;

use JsonSerializable;

class ShopDto implements JsonSerializable
{
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}