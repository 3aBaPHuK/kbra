<?php

namespace App\Dto\Common;

use Countable;
use Iterator;
use JsonSerializable;

abstract class AbstractCollection implements Countable, Iterator, JsonSerializable
{
    /**
     * @var array
     */
    private $values;

    public function __construct(...$values)
    {
        $this->values = $values;
    }

    public function current()
    {
        return current($this->values);
    }

    public function next()
    {
        return next($this->values);
    }

    public function key()
    {
        return key($this->values);
    }

    public function valid(): bool
    {
        return $this->key() !== null;
    }

    public function rewind()
    {
        reset($this->values);
    }

    public function count(): int
    {
        return count($this->values);
    }

    public function jsonSerialize()
    {
        return $this->values;
    }
}