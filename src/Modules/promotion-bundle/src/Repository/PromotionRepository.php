<?php

namespace App\Modules\PromotionBundle\Repository;

use App\Modules\PromotionBundle\Entity\Promotion;
use App\Modules\PromotionBundle\Interfaces\PromotionRepositoryInterface;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

class PromotionRepository extends ServiceEntityRepository implements PromotionRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Promotion::class);
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function save(Promotion $promotion): Promotion
    {
        $this->_em->persist($promotion);
        $this->_em->flush();

        return $promotion;
    }

    public function makeActive(Promotion $promotion): void
    {
        if (!$promotion->isActive()) {

            return;
        }

        $connection = $this->_em->getConnection();

        $connection->executeQuery('UPDATE '.$this->getClassMetadata()->getTableName().' 
        SET active=false WHERE id <> '.$promotion->getId());
    }

    public function getActivePromotion(): ?Promotion
    {
        $today = new DateTime();

        $activePromotion = $this->findOneBy(['active'=> true]);

        if ($activePromotion->getDateFrom() <= $today && $activePromotion->getDateTo() >= $today) {
            return $activePromotion;
        }

        return null;
    }
}
