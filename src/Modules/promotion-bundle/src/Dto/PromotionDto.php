<?php

namespace App\Modules\PromotionBundle\Dto;

use App\Modules\PromotionBundle\Entity\Promotion;
use DateTime;
use JsonSerializable;

class PromotionDto implements JsonSerializable
{
    private $id;

    private $dateFrom;

    private $dateTo;

    private $description;

    private $active;

    private $productsMessage;

    private function __construct(int $id, DateTime $dateFrom, DateTime $dateTo, string $description, bool $active, string $productsMessage)
    {
        $this->id = $id;
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
        $this->description = $description;
        $this->active = $active;
        $this->productsMessage = $productsMessage;
    }

    public static function fromEntity(Promotion $promotion): self
    {
        return new self(
            $promotion->getId(),
            $promotion->getDateFrom(),
            $promotion->getDateTo(),
            $promotion->getDescription(),
            $promotion->isActive(),
            $promotion->getProductsMessage()
        );
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getDateFrom(): DateTime
    {
        return $this->dateFrom;
    }

    /**
     * @return DateTime
     */
    public function getDateTo(): DateTime
    {
        return $this->dateTo;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return string
     */
    public function getProductsMessage(): string
    {
        return $this->productsMessage;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
