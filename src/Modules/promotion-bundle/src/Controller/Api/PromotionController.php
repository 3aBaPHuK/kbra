<?php

namespace App\Modules\PromotionBundle\Controller\Api;

use App\Modules\PromotionBundle\Dto\PromotionDto;
use App\Modules\PromotionBundle\Interfaces\PromotionRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class PromotionController extends AbstractController
{
    /**
     * @var PromotionRepositoryInterface
     */
    private $promotionRepository;

    public function __construct(PromotionRepositoryInterface $promotionRepository)
    {
        $this->promotionRepository = $promotionRepository;
    }

    /**
     * Get actual promotion
     *
     * @Route("/api/promotion/active", methods={"GET"})
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns promotion DTO",
     * )
     * @OA\Tag(name="promotion")
     */
    public function getActualPromotion(): JsonResponse
    {
        $promotion = $this->promotionRepository->getActivePromotion();

        if (!$promotion) {
            return new JsonResponse();
        }

        return new JsonResponse(PromotionDto::fromEntity($promotion));
    }
}
