<?php

namespace App\Modules\PromotionBundle\Controller\Admin;

use App\Modules\PromotionBundle\Entity\Promotion;
use App\Modules\PromotionBundle\Interfaces\PromotionRepositoryInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

class PromotionController extends EasyAdminController
{
    /**
     * @var PromotionRepositoryInterface
     */
    private $promotionRepository;

    public function __construct(PromotionRepositoryInterface $promotionRepository)
    {
        $this->promotionRepository = $promotionRepository;
    }

    /**
     * @param Promotion $entity
     */
    protected function persistEntity($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();

        if ($entity->isActive()) {
            $this->promotionRepository->makeActive($entity);
        }
    }

    /**
     * @param Promotion $entity
     */
    protected function updateEntity($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();

        if ($entity->isActive()) {
            $this->promotionRepository->makeActive($entity);
        }
    }

    protected function removeEntity($entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }
}
