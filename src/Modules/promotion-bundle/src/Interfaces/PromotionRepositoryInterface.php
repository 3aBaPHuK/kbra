<?php

namespace App\Modules\PromotionBundle\Interfaces;

use App\Modules\PromotionBundle\Entity\Promotion;

interface PromotionRepositoryInterface
{
    public function getActivePromotion():? Promotion;

    public function makeActive(Promotion $promotion): void;
}
