<?php

namespace App\Modules\PromotionBundle\Entity;
use DateTime;
use App\Modules\PromotionBundle\Repository\PromotionRepository;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PromotionRepository::class)
 */
class Promotion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     * @var DateTime
     */
    private $dateFrom;

    /**
     * @ORM\Column(type="date")
     * @var DateTime
     */
    private $dateTo;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var bool
     */
    private $active;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $productsMessage;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateFrom(): ?DateTime
    {
        return $this->dateFrom;
    }

    public function setDateFrom(DateTime $dateFrom): void
    {
        $this->dateFrom = $dateFrom;
    }

    public function getDateTo(): ?DateTime
    {
        return $this->dateTo;
    }


    public function setDateTo(DateTime $dateTo): void
    {
        $this->dateTo = $dateTo;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function getProductsMessage(): ?string
    {
        return $this->productsMessage;
    }

    public function setProductsMessage(string $productsMessage): void
    {
        $this->productsMessage = $productsMessage;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
}
