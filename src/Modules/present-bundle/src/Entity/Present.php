<?php

namespace App\Modules\PresentBundle\Entity;

use App\Entity\Product;
use Doctrine\ORM\Mapping as ORM;
use App\Modules\PresentBundle\Repository\PresentRepository;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass=PresentRepository::class)
 */
class Present implements JsonSerializable
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="present")
     */
    private $product;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="presentFor")
     */
    private $presentProduct;

    public function getId(): int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    public function getPresentProduct(): ?Product
    {
        return $this->presentProduct;
    }

    public function setPresentProduct(Product $presentProduct): void
    {
        $this->presentProduct = $presentProduct;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
