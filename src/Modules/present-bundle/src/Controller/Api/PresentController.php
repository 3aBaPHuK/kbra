<?php

namespace App\Modules\PresentBundle\Controller\Api;

use App\Modules\PresentBundle\Dto\PresentDto;
use App\Modules\PresentBundle\Repository\PresentRepositoryInterface;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Annotations as OA;
use Symfony\Component\Routing\Annotation\Route;

class PresentController extends AbstractController
{
    /**
     * @var PresentRepositoryInterface
     */
    private $presentRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(PresentRepositoryInterface $presentRepository, ProductRepository $productRepository)
    {
        $this->presentRepository = $presentRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * Get present for product
     *
     * @Route("/api/product/{productId}/present", methods={"GET"})
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns present for product if applicable. Else return null",
     * )
     * @OA\Tag(name="products")
     */
    public function presentByProductId(int $productId): Response
    {
        $product = $this->productRepository->find($productId);

        if (!$product) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        $present = $this->presentRepository->findActivePresentByProductId($product);

        return new JsonResponse($present ? PresentDto::fromPresentEntity($present, $this->getParameter('app.path.product_images')) : null);
    }
}
