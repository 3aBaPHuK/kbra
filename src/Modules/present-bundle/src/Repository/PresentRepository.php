<?php

namespace App\Modules\PresentBundle\Repository;

use App\Entity\Product;
use App\Modules\PresentBundle\Entity\Present;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class PresentRepository extends ServiceEntityRepository implements PresentRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Present::class);
    }

    public function findActivePresentByProductId(Product $product): ?Present
    {
        return $this->findOneBy(['product' => $product]);
    }
}
