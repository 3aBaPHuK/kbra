<?php

namespace App\Modules\PresentBundle\Repository;

use App\Entity\Product;
use App\Modules\PresentBundle\Entity\Present;

interface PresentRepositoryInterface
{
    public function findActivePresentByProductId(Product $product):? Present;
}
