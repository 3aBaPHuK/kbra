<?php

namespace App\Modules\PresentBundle\Dto;

use App\Dto\Products\ProductDto;
use App\Modules\PresentBundle\Entity\Present;
use JsonSerializable;

final class PresentDto implements JsonSerializable
{
    private $presentProduct;

    private $description;

    private function __construct(ProductDto $presentProduct, ?string $description = null)
    {
        $this->presentProduct = $presentProduct;
        $this->description = $description;
    }

    public static function fromPresentEntity(Present $presentEntity, string $imagesBasePath): self
    {
        return new self(ProductDto::fromProductEntity($presentEntity->getPresentProduct(), $imagesBasePath), $presentEntity->getDescription());
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return ProductDto
     */
    public function getPresentProduct(): ProductDto
    {
        return $this->presentProduct;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
}
