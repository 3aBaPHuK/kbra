<?php

namespace App\Modules\ShopBundle\Dto;

use App\Modules\ShopBundle\Entity\ShopEmail;
use JsonSerializable;

class EmailDto implements JsonSerializable
{
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $description;

    private function __construct(string $email, string $description)
    {
        $this->email = $email;
        $this->description = $description;
    }

    public static function fromEntity(ShopEmail $email): self
    {
        return new self(
            $email->getEmail(),
            $email->getDescription()
        );
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
