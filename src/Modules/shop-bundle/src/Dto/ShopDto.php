<?php

namespace App\Modules\ShopBundle\Dto;

use App\Modules\ShopBundle\Entity\Shop;
use App\Modules\ShopBundle\Entity\ShopAddress;
use App\Modules\ShopBundle\Entity\ShopCounter;
use App\Modules\ShopBundle\Entity\ShopEmail;
use App\Modules\ShopBundle\Entity\ShopPhone;
use App\Modules\ShopBundle\Entity\ShopSocial;
use JsonSerializable;

class ShopDto implements JsonSerializable
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $schedule;
    /**
     * @var string
     */
    private $inn;
    /**
     * @var string
     */
    private $ogrn;
    /**
     * @var array
     */
    private $phones;
    /**
     * @var array
     */
    private $addresses;
    /**
     * @var string
     */
    private $mapCode;
    /**
     * @var string
     */
    private $additional;
    /**
     * @var string|null
     */
    private $metaTitle;
    /**
     * @var string|null
     */
    private $metaDescription;
    /**
     * @var string|null
     */
    private $metaKeywords;
    /**
     * @var array
     */
    private $emails;
    /**
     * @var string|null
     */
    private $mainPageText;
    /**
     * @var array
     */
    private $counters;
    /**
     * @var array
     */
    private $socials;

    private function __construct(
        string $name,
        string $schedule,
        string $inn,
        string $ogrn,
        array $phones,
        array $addresses,
        array $emails,
        array $counters,
        array $socials,
        ?string $mapCode,
        ?string $mainPageText,
        ?string $additional,
        ?string $metaTitle,
        ?string $metaDescription,
        ?string $metaKeywords
    )
    {
        $this->name = $name;
        $this->schedule = $schedule;
        $this->inn = $inn;
        $this->ogrn = $ogrn;
        $this->phones = $phones;
        $this->addresses = $addresses;
        $this->mapCode = $mapCode;
        $this->socials = $socials;
        $this->additional = $additional;
        $this->metaTitle = $metaTitle;
        $this->metaDescription = $metaDescription;
        $this->metaKeywords = $metaKeywords;
        $this->emails = $emails;
        $this->mainPageText = $mainPageText;
        $this->counters = $counters;
    }

    public static function fromEntity(Shop $shop, string $socialImagesPath): self
    {
        $phones = self::sortPhones($shop);
        $addresses = self::sortAddresses($shop);
        $counters = self::sortCounters($shop);
        $emails = self::sortEmails($shop);
        $socials = self::sortSocials($shop, $socialImagesPath);

        return new self(
            $shop->getName(),
            $shop->getSchedule(),
            $shop->getInn(),
            $shop->getOgrn(),
            $phones,
            $addresses,
            $emails,
            $counters,
            $socials,
            $shop->getMapCode(),
            $shop->getMainPageText(),
            $shop->getAdditional(),
            $shop->getMetaTitle(),
            $shop->getMetaDescription(),
            $shop->getMetaKeywords()
        );
    }

    /**
     * @param Shop $shop
     * @return SocialDto[]|array
     */
    private static function sortSocials(Shop $shop, string $socialImagesPath): array
    {
        $socials = $shop->getSocials()->getValues();

        usort($socials, function (ShopSocial $socialA, ShopSocial $socialB) {
            return $socialA->getPosition() > $socialB->getPosition();
        });

        return array_map(function (ShopSocial $social) use ($socialImagesPath) {
            return SocialDto::fromEntity($social, $socialImagesPath);
        }, $socials);
    }

    /**
     * @param Shop $shop
     * @return PhoneDto[]|array
     */
    private static function sortPhones(Shop $shop): array
    {
        $phones = $shop->getPhones()->getValues();

        usort($phones, function (ShopPhone $phoneA, ShopPhone $phoneB) {
            return $phoneA->getPosition() > $phoneB->getPosition();
        });

        return array_map(function (ShopPhone $phone) {
            return PhoneDto::fromEntity($phone);
        }, $phones);
    }

    /**
     * @param Shop $shop
     * @return AddressDto[]|array
     */
    private static function sortAddresses(Shop $shop): array
    {
        $addresses = $shop->getAddresses()->getValues();

        usort($addresses, function (ShopAddress $addressA, ShopAddress $addressB) {
            return $addressA->getPosition() > $addressB->getPosition();
        });

        return array_map(function (ShopAddress $address) {
            return AddressDto::fromEntity($address);
        }, $addresses);
    }

    /**
     * @param Shop $shop
     * @return AddressDto[]|array
     */
    private static function sortEmails(Shop $shop): array
    {
        $emails = $shop->getEmails()->getValues();

        usort($emails, function (ShopEmail $emailA, ShopEmail $emailB) {
            return $emailA->getPosition() > $emailB->getPosition();
        });

        return array_map(function (ShopEmail $email) {
            return EmailDto::fromEntity($email);
        }, $emails);
    }

    /**
     * @param Shop $shop
     * @return AddressDto[]|array
     */
    private static function sortCounters(Shop $shop): array
    {
        $counters = $shop->getCounters()->getValues();

        usort($counters, function (ShopCounter $counterA, ShopCounter $counterB) {
            return $counterA->getPosition() > $counterB->getPosition();
        });

        return array_map(function (ShopCounter $counter) {
            return CounterDto::fromEntity($counter);
        }, $counters);
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
