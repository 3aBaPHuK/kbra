<?php

namespace App\Modules\ShopBundle\Dto;

use App\Modules\ShopBundle\Entity\ShopSocial;
use JsonSerializable;

class SocialDto implements JsonSerializable
{
    private $icon;
    private $link;

    private function __construct(string $icon, ?string $link)
    {
        $this->icon = $icon;
        $this->link = $link;
    }

    public static function fromEntity(ShopSocial $social, string $socialImagesPath): self
    {
        return new self(
            $socialImagesPath.DIRECTORY_SEPARATOR.$social->getIcon(),
            $social->getLink()
        );
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
