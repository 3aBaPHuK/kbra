<?php

namespace App\Modules\ShopBundle\Dto;

use App\Modules\ShopBundle\Entity\ShopPhone;
use JsonSerializable;

class PhoneDto implements JsonSerializable
{
    /**
     * @var string
     */
    private $number;
    /**
     * @var string
     */
    private $description;

    private function __construct(string $number, string $description)
    {
        $this->number = $number;
        $this->description = $description;
    }

    public static function fromEntity(ShopPhone $address): self
    {
        return new self(
            $address->getNumber(),
            $address->getDescription()
        );
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}