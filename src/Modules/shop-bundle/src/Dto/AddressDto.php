<?php

namespace App\Modules\ShopBundle\Dto;

use App\Modules\ShopBundle\Entity\Shop;
use App\Modules\ShopBundle\Entity\ShopAddress;
use Doctrine\ORM\PersistentCollection;
use JsonSerializable;

class AddressDto implements JsonSerializable
{
    /**
     * @var string
     */
    private $address;
    /**
     * @var string
     */
    private $description;

    private function __construct(string $address, string $description)
    {
        $this->address = $address;
        $this->description = $description;
    }

    public static function fromEntity(ShopAddress $address): self
    {
        return new self(
            $address->getAddress(),
            $address->getDescription()
        );
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}