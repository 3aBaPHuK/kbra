<?php

namespace App\Modules\ShopBundle\Dto;

use App\Modules\ShopBundle\Entity\ShopCounter;
use JsonSerializable;

final class CounterDto implements JsonSerializable
{
    private $description;
    private $code;

    private function __construct(string $description, string $code)
    {
        $this->description = $description;
        $this->code = $code;
    }

    public static function fromEntity(ShopCounter $shopCounter): CounterDto
    {
        return new self($shopCounter->getDescription(), $shopCounter->getCode());
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
