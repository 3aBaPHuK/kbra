<?php

namespace App\Modules\ShopBundle\Form\Admin;

use App\Modules\ShopBundle\Entity\ShopMainEmail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShopEmailForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class, ['required' => true, 'label' => 'ADMIN_SHOP_EMAIL_EMAIL'])
            ->add('description', TextType::class, ['required' => true, 'label' => 'ADMIN_SHOP_EMAIL_DESCRIPTION'])
            ->add('position', HiddenType::class, ['required' => true, 'label' => 'ADMIN_LABEL_POSITION']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ShopMainEmail::class,
        ));
    }
}
