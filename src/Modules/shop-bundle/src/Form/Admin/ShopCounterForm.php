<?php

namespace App\Modules\ShopBundle\Form\Admin;

use App\Modules\ShopBundle\Entity\ShopAddress;
use App\Modules\ShopBundle\Entity\ShopCounter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShopCounterForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextType::class, ['required' => true, 'label' => 'ADMIN_SHOP_COUNTER_DESCRIPTION'])
            ->add('code', TextareaType::class, ['required' => true, 'label' => 'ADMIN_SHOP_COUNTER_CODE'])
            ->add('position', HiddenType::class, ['required' => true, 'label' => 'ADMIN_LABEL_POSITION']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ShopCounter::class,
        ));
    }
}
