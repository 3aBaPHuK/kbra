<?php

namespace App\Modules\ShopBundle\Form\Admin;

use App\Modules\ShopBundle\Entity\ShopSocial;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ShopSocialsForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('iconFile', VichImageType::class, ['required' => true, 'label' => 'ADMIN_SHOP_SOCIALS_ICON'])
            ->add('link', TextType::class, ['required' => true, 'label' => 'ADMIN_SHOP_SOCIALS_LINK'])
            ->add('position', HiddenType::class, ['required' => true, 'label' => 'ADMIN_LABEL_POSITION']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ShopSocial::class,
            'sort' => 'position'
        ));
    }
}
