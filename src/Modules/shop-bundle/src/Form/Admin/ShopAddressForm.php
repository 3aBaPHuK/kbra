<?php

namespace App\Modules\ShopBundle\Form\Admin;

use App\Modules\ShopBundle\Entity\ShopAddress;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShopAddressForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('address', TextType::class, ['required' => true, 'label' => 'ADMIN_SHOP_ADDRESS_ADDRESS'])
            ->add('description', TextType::class, ['required' => true, 'label' => 'ADMIN_SHOP_ADDRESS_DESCRIPTION'])
            ->add('position', HiddenType::class, ['required' => true, 'label' => 'ADMIN_LABEL_POSITION']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ShopAddress::class,
            'sort' => 'position'
        ));
    }
}
