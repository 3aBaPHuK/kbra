<?php

namespace App\Modules\ShopBundle\Form\Admin;

use App\Modules\ShopBundle\Entity\ShopPhone;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShopPhoneForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', TextType::class, ['required' => true, 'label' => 'ADMIN_SHOP_PHONE_NUMBER'])
            ->add('description', TextType::class, ['required' => true, 'label' => 'ADMIN_SHOP_PHONE_DESCRIPTION'])
            ->add('position', HiddenType::class, ['required' => true, 'label' => 'ADMIN_LABEL_POSITION']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ShopPhone::class,
        ));
    }
}
