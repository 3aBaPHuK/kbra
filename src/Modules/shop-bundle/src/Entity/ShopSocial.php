<?php

namespace App\Modules\ShopBundle\Entity;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @Vich\Uploadable
 */
class ShopSocial
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $icon;

    /**
     * @Vich\UploadableField(mapping="shop_social_icons", fileNameProperty="icon")
     * @var File
     */
    private $iconFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $link;

    /**
     * @ORM\Column(type="integer")
     * @Gedmo\SortablePosition()
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="App\Modules\ShopBundle\Entity\Shop", inversedBy="socials")
     * @ORM\JoinColumn(nullable=true)
     */
    private $shop;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTime
     */
    private $updatedAt;

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(Shop $shop): void
    {
        $this->shop = $shop;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): void
    {
        $this->position = $position;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function getIconFile(): ?File
    {
        return $this->iconFile;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setIcon(?string $icon): void
    {
        $this->icon = $icon;
    }

    public function setLink(?string $link): void
    {
        $this->link = $link;
    }

    public function setImageFile(?File $icon = null)
    {
        $this->iconFile = $icon;

        if ($icon) {
            $this->updatedAt = new DateTime('now');
        }
    }

    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTime $updatedAt): void
    {
        if (!$updatedAt) {
            $updatedAt = new DateTime('now');
        }

        $this->updatedAt = $updatedAt;
    }

    public function setIconFile(?File $iconFile): void
    {
        $this->iconFile = $iconFile;
    }
}
