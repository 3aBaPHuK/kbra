<?php

namespace App\Modules\ShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Modules\ShopBundle\Interfaces\ShopRepositoryInterface")
 */
class Shop
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="string", length=12, nullable=true)
     */
    private $inn;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $ogrn;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $schedule;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $mapCode;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $additional;

    /**
     * @ORM\OneToMany(targetEntity="App\Modules\ShopBundle\Entity\ShopPhone", mappedBy="shop", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $phones;

    /**
     * @ORM\OneToMany(targetEntity="App\Modules\ShopBundle\Entity\ShopMainEmail", mappedBy="shop", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $emails;

    /**
     * @ORM\OneToMany(targetEntity="App\Modules\ShopBundle\Entity\ShopOperatorEmail", mappedBy="shop", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $operatorEmails;

    /**
     * @ORM\OneToMany(targetEntity="App\Modules\ShopBundle\Entity\ShopAddress", mappedBy="shop", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $addresses;

    /**
     * @ORM\OneToMany(targetEntity="App\Modules\ShopBundle\Entity\ShopSocial", mappedBy="shop", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $socials;

    /**
     * @ORM\OneToMany(targetEntity="App\Modules\ShopBundle\Entity\ShopCounter", mappedBy="shop", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $counters;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaTitle;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $mainPageText = '';

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaKeywords;

    public function __construct()
    {
        $this->addresses = new ArrayCollection();
        $this->phones = new ArrayCollection();
        $this->counters = new ArrayCollection();
        $this->emails = new ArrayCollection();
        $this->operatorEmails = new ArrayCollection();
        $this->socials = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMapCode():? string
    {
        return $this->mapCode;
    }

    public function setMapCode(string $mapCode): void
    {
        $this->mapCode = $mapCode;
    }

    public function getIsActive(): bool
    {
        return (bool) $this->isActive;
    }

    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    public function getInn(): ?string
    {
        return $this->inn;
    }

    public function setInn(string $inn): void
    {
        $this->inn = $inn;
    }

    public function getOgrn(): ?string
    {
        return $this->ogrn;
    }

    public function setOgrn(string $ogrn): void
    {
        $this->ogrn = $ogrn;
    }

    public function getPhones(): ?Collection
    {
        return $this->phones;
    }

    public function setPhones($phones): void
    {
        $this->phones = $phones;
    }

    public function getAddresses(): ?Collection
    {
        return $this->addresses;
    }

    public function setAddresses($addresses): void
    {
        $this->addresses = $addresses;
    }

    public function getSchedule(): ?string
    {
        return $this->schedule;
    }

    public function setSchedule(?string $schedule): void
    {
        $this->schedule = $schedule;
    }

    public function getAdditional(): ?string
    {
        return $this->additional;
    }

    public function setAdditional(?string $additional): void
    {
        $this->additional = $additional;
    }

    public function getCounters(): ?Collection
    {
        return $this->counters;
    }

    public function setCounters(?Collection $counters): void
    {
        $this->counters = $counters;
    }

    public function addCounter(ShopCounter $counter): Shop
    {
        $counter->setShop($this);

        $this->counters[] = $counter;

        return $this;
    }

    public function removeCounter(ShopCounter $counter)
    {
        $this->counters->removeElement($counter);
    }

    public function addAddress(ShopAddress $address): Shop
    {
        $address->setShop($this);

        $this->addresses[] = $address;

        return $this;
    }

    public function removeAddress(ShopAddress $address)
    {
        $this->addresses->removeElement($address);
    }

    public function addPhone(ShopPhone $phone): Shop
    {
        $phone->setShop($this);

        $this->phones[] = $phone;

        return $this;
    }

    public function removePhone(ShopPhone $phone)
    {
        $this->phones->removeElement($phone);
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    public function setMetaKeywords(?string $metaKeywords): void
    {
        $this->metaKeywords = $metaKeywords;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    public function getMainPageText(): ?string
    {
        return $this->mainPageText;
    }

    public function setMainPageText(?string $mainPageText): void
    {
        $this->mainPageText = $mainPageText;
    }

    public function addEmail(ShopMainEmail $email): Shop
    {
        $email->setShop($this);

        $this->emails[] = $email;

        return $this;
    }

    public function removeEmail(ShopMainEmail $email): void
    {
        $this->emails->removeElement($email);
    }

    public function getEmails(): ?Collection
    {
        return $this->emails;
    }

    public function addOperatorEmail(ShopOperatorEmail $email): Shop
    {
        $email->setShop($this);

        $this->operatorEmails[] = $email;

        return $this;
    }

    public function removeOperatorEmail(ShopOperatorEmail $email): void
    {
        $this->operatorEmails->removeElement($email);
    }

    public function getOperatorEmails(): ?Collection
    {
        return $this->operatorEmails;
    }

    public function addSocial(ShopSocial $social): Shop
    {
        $social->setShop($this);

        $this->socials[] = $social;

        return $this;
    }

    public function removeSocial(ShopSocial $social): void
    {
        $this->socials->removeElement($social);
    }

    public function getSocials(): ?Collection
    {
        return $this->socials;
    }
}
