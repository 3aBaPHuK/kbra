<?php

namespace App\Modules\ShopBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Modules\ShopBundle\Repository\ShopPhoneRepository")
 */
class ShopPhone
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="App\Modules\ShopBundle\Entity\Shop", inversedBy="phones")
     * @ORM\JoinColumn(nullable=true)
     */
    private $shop;

    /**
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(Shop $shop): void
    {
        $this->shop = $shop;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }


    public function setNumber(string $number): void
    {
        $this->number = $number;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): void
    {
        $this->position = $position;
    }
}
