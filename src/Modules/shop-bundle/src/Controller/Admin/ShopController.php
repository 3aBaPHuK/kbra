<?php

namespace App\Modules\ShopBundle\Controller\Admin;

use App\Modules\ShopBundle\Entity\Shop;
use App\Modules\ShopBundle\Interfaces\ShopRepositoryInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

class ShopController extends EasyAdminController
{
    /**
     * @var ShopRepositoryInterface
     */
    private $shopRepository;

    public function __construct(ShopRepositoryInterface $shopRepository)
    {
        $this->shopRepository = $shopRepository;
    }

    protected function persistEntity($entity)
    {
        $this->updateRelations($entity);
    }

    private function updateRelations(Shop $shop)
    {
        foreach ($shop->getCounters() as $counter) {
            $counter->setShop($shop);
        }

        foreach ($shop->getPhones() as $phone) {
            $phone->setShop($shop);
        }

        foreach ($shop->getAddresses() as $address) {
            $address->setShop($shop);
        }

        foreach ($shop->getEmails() as $email) {
            $email->setShop($shop);
        }

        foreach ($shop->getOperatorEmails() as $email) {
            $email->setShop($shop);
        }

        foreach ($shop->getSocials() as $social) {
            $social->setShop($shop);
        }

        $this->em->persist($shop);
        $this->em->flush();

        if ($shop->getIsActive()) {
            $this->shopRepository->makeActive($shop);
        }
    }

    protected function updateEntity($entity)
    {
        $this->updateRelations($entity);
    }

    protected function removeEntity($entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }
}
