<?php

namespace App\Modules\ShopBundle\Controller\Api;

use App\Modules\ShopBundle\Dto\ShopDto;
use App\Modules\ShopBundle\Interfaces\ShopRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ShopController extends AbstractController
{
    /**
     * @var ShopRepositoryInterface
     */
    private $shopRepository;
    /**
     * @var string
     */
    private $socialsImagesPath;

    public function __construct(ShopRepositoryInterface $shopRepository, string $socialsImagesPath)
    {
        $this->shopRepository = $shopRepository;
        $this->socialsImagesPath = $socialsImagesPath;
    }

    /**
     * Get active shop data
     *
     * @Route("/api/shop/active", methods={"GET"})
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns shop DTO",
     * )
     * @OA\Tag(name="shop")
     */
    public function getActiveShop(): JsonResponse
    {
        if (!$shop = $this->shopRepository->getActiveShop()) {

            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(ShopDto::fromEntity($shop, $this->socialsImagesPath));
    }
}
