<?php

namespace App\Modules\ShopBundle\Interfaces;

use App\Modules\ShopBundle\Entity\Shop;

interface ShopRepositoryInterface
{
    public function save(Shop $shop): Shop;

    public function makeActive(Shop $shop): void;

    public function getActiveShop(): Shop;
}