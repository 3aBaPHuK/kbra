<?php

namespace App\Modules\ShopBundle\Repository;

use App\Modules\ShopBundle\Entity\ShopEmail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class ShopEmailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShopEmail::class);
    }
}
