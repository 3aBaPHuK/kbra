<?php

namespace App\Modules\ShopBundle\Repository;

use App\Modules\ShopBundle\Entity\Shop;
use App\Modules\ShopBundle\Interfaces\ShopRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ShopRepository extends ServiceEntityRepository implements ShopRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Shop::class);
    }

    public function save(Shop $shop): Shop
    {
        $this->_em->persist($shop);
        $this->_em->flush();

        return $shop;
    }

    public function makeActive(Shop $shop): void
    {
        if (!$shop->getIsActive()) {

            return;
        }

        $connection = $this->_em->getConnection();

        $connection->executeQuery('UPDATE '.$this->getClassMetadata()->getTableName().' 
        SET is_active=false WHERE id <> '.$shop->getId());
    }

    public function getActiveShop(): Shop
    {
        return $this->findOneBy(['isActive' => true]);
    }
}
