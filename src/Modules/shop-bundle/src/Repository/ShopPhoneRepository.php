<?php

namespace App\Modules\ShopBundle\Repository;

use App\Modules\ShopBundle\Entity\ShopPhone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class ShopPhoneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShopPhone::class);
    }
}
