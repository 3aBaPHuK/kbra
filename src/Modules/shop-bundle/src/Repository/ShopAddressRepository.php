<?php

namespace App\Modules\ShopBundle\Repository;

use App\Modules\ShopBundle\Entity\ShopAddress;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class ShopAddressRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShopAddress::class);
    }
}
