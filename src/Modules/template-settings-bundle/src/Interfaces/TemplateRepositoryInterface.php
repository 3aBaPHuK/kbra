<?php

namespace App\Modules\TemplateSettingsBundle\Interfaces;

use App\Modules\TemplateSettingsBundle\Entity\Template;

interface TemplateRepositoryInterface
{
    public function save(Template $template): Template;
    public function makeActive(Template $template);
    public function getActiveTemplate(): Template;
}
