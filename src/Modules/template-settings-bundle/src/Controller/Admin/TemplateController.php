<?php

namespace App\Modules\TemplateSettingsBundle\Controller\Admin;

use App\Modules\TemplateSettingsBundle\Entity\Template;
use App\Modules\TemplateSettingsBundle\Interfaces\TemplateRepositoryInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

class TemplateController extends EasyAdminController
{
    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    public function __construct(TemplateRepositoryInterface $templateRepository)
    {
        $this->templateRepository = $templateRepository;
    }

    protected function persistEntity($entity)
    {
        $this->updateRelations($entity);
    }

    private function updateRelations(Template $template)
    {
        $this->em->persist($template);
        $this->em->flush();

        if ($template->getIsActive()) {
            $this->templateRepository->makeActive($template);
        }
    }

    protected function updateEntity($entity)
    {
        $this->updateRelations($entity);
    }

    protected function removeEntity($entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }
}
