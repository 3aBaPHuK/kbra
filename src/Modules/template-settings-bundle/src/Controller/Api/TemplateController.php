<?php

namespace App\Modules\TemplateSettingsBundle\Controller\Api;

use App\Modules\TemplateSettingsBundle\Interfaces\TemplateRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TemplateController extends AbstractController
{
    /**
     * @var TemplateRepositoryInterface
     */
    private $templateRepository;

    public function __construct(TemplateRepositoryInterface $templateRepository)
    {
        $this->templateRepository = $templateRepository;
    }

    /**
     * Get active template custom css
     *
     * @Route("/bundles/template/active/template.css", methods={"GET"})
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns css text",
     * )
     * @OA\Tag(name="template")
     */
    public function getCurrentTemplateCss(): Response
    {
        $template = $this->templateRepository->getActiveTemplate();

        if (!$template) {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }

        $response = new Response($template->getCustomCss());
        $response->headers->set('Content-Type', 'text/css');

        return $response;
    }
}
