<?php

namespace App\Modules\TemplateSettingsBundle\Repository;

use App\Modules\TemplateSettingsBundle\Entity\Template;
use App\Modules\TemplateSettingsBundle\Interfaces\TemplateRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TemplateRepository extends ServiceEntityRepository implements TemplateRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Template::class);
    }

    public function save(Template $shop): Template
    {
        $this->_em->persist($shop);
        $this->_em->flush();

        return $shop;
    }

    public function makeActive(Template $template): void
    {
        if (!$template->getIsActive()) {

            return;
        }

        $connection = $this->_em->getConnection();

        $connection->executeQuery('UPDATE '.$this->getClassMetadata()->getTableName().' 
        SET is_active=false WHERE id <> '.$template->getId());
    }

    public function getActiveTemplate(): Template
    {
        return $this->findOneBy(['isActive' => true]);
    }
}
