<?php

namespace App\Modules\MessageBundle\EventListener;

use App\Modules\MessageBundle\Message\SendEmailMessage;
use App\Modules\OrderBundle\Event\OrderStatusChangedEvent;
use Symfony\Component\Messenger\MessageBusInterface;

class OrderStatusChangedEventListener
{
    /**
     * @var MessageBusInterface
     */
    private $messageBus;
    /**
     * @var string
     */
    private $emailFrom;

    public function __construct(MessageBusInterface $messageBus, string $emailFrom)
    {
        $this->messageBus = $messageBus;
        $this->emailFrom = $emailFrom;
    }

    public function onOrderStatusChanged(OrderStatusChangedEvent $event)
    {
        $orderCustomer = $event->getOrder()->getCustomer();

//        $this->messageBus->dispatch(
//            new SendEmailMessage(
//                $this->emailFrom,
//                $orderCustomer->getEmail(),
//                'Order status changed',
//                '<strong>Your order '.$event->getOrder()->getId().' status was changed to "'.$event->getStatus().'"</strong>'
//            )
//        );
    }
}
