<?php

namespace App\Modules\MessageBundle\EventListener;

use App\Modules\MessageBundle\Message\SendEmailMessage;
use App\Modules\OrderBundle\Event\OrderCreatedEvent;
use App\Modules\ShopBundle\Entity\ShopOperatorEmail;
use App\Modules\ShopBundle\Interfaces\ShopRepositoryInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Twig\Environment;

class OrderCreatedEventListener
{
    private $messageBus;
    private $emailFrom;
    private $twig;
    private $emailAdmin;
    private $baseSiteUrl;
    private $shopRepository;

    public function __construct(MessageBusInterface $messageBus, string $emailFrom, string $emailAdmin, string $baseSiteUrl, Environment $twig, ShopRepositoryInterface $shopRepository)
    {
        $this->messageBus = $messageBus;
        $this->emailFrom = $emailFrom;
        $this->twig = $twig;
        $this->emailAdmin = $emailAdmin;
        $this->baseSiteUrl = $baseSiteUrl;
        $this->shopRepository = $shopRepository;
    }

    public function onOrderCreated(OrderCreatedEvent $event)
    {
        $orderCustomer = $event->getOrder()->getCustomer();

        $operatorEmails = $this->shopRepository->getActiveShop()->getOperatorEmails();
        $operatorEmailsArray = array_map(function (ShopOperatorEmail $email) {
            return $email->getEmail();
        }, iterator_to_array($operatorEmails));

        foreach ($operatorEmailsArray as $operatorEmail) {
            $this->messageBus->dispatch(
                new SendEmailMessage(
                    $this->emailFrom,
                    $operatorEmail,
                    'Новый заказ на '.$this->baseSiteUrl,
                    $this->twig->render('email/order/orderCreatedForAdmin.html.twig', [
                        'order' => $event->getOrder(),
                        'customer' => array_filter($event->getOrder()->getCustomer()->toArray()),
                    ])
                )
            );
        }

        $this->messageBus->dispatch(
            new SendEmailMessage(
                $this->emailFrom,
                $orderCustomer->getEmail(),
                'Спасибо за Ваш заказ',
                $this->twig->render('email/order/orderCreated.html.twig', [
                    'order' => $event->getOrder(),
                    'baseSiteUrl' => $this->baseSiteUrl
                ])
            )
        );
    }
}
