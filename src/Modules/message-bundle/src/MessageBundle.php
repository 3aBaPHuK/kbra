<?php

namespace App\Modules\MessageBundle;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MessageBundle extends Bundle
{
    public function getContainerExtension()
    {
        return null;
    }

    /**
     * @throws Exception
     */
    public function build(ContainerBuilder $container)
    {
        $container->setParameter('message_bundle_path', dirname(__DIR__));

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/Resources/config'));
        $loader->load('services.yaml');
        $loader->load('parameters.yaml');

        parent::build($container);
    }
}