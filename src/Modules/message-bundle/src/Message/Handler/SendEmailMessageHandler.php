<?php

namespace App\Modules\MessageBundle\Message\Handler;

use App\Modules\MessageBundle\Message\SendEmailMessage;
use App\Modules\MessageBundle\Service\MessageService;

class SendEmailMessageHandler
{
    /**
     * @var MessageService
     */
    private $messageService;

    public function __construct(MessageService $messageService)
    {
        $this->messageService = $messageService;
    }

    public function __invoke(SendEmailMessage $sendEmailMessage)
    {
        $this->messageService->sendEmail(
            $sendEmailMessage->getFrom(),
            $sendEmailMessage->getTo(),
            $sendEmailMessage->getSubject(),
            $sendEmailMessage->getMessage()
        );
    }
}