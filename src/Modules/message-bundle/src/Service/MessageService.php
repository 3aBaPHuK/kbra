<?php

namespace App\Modules\MessageBundle\Service;

use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\MailerInterface;

class MessageService
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendEmail(string $from, string $to, string $subject, string $message)
    {
        $message = (new Email())
            ->from($from)
            ->to($to)
            ->subject($subject)
            ->html($message);

        $this->mailer->send($message);
    }
}
