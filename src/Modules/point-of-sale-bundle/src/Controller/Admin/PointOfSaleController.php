<?php

namespace App\Modules\PointOfSaleBundle\Controller\Admin;

use App\Modules\PointOfSaleBundle\Entity\PointOfSale;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

class PointOfSaleController extends EasyAdminController
{
    protected function persistEntity($entity)
    {
        $this->updateRelations($entity);
    }

    protected function updateEntity($entity)
    {
        $this->updateRelations($entity);
    }

    private function updateRelations(PointOfSale $pointOfSale)
    {
        foreach ($pointOfSale->getScheduleItems() as $scheduleItem) {
            $scheduleItem->setPointOfSale($pointOfSale);
        }

        $this->em->persist($pointOfSale);
        $this->em->flush();
    }
}
