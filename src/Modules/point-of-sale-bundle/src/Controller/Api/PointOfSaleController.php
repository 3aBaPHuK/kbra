<?php

namespace App\Modules\PointOfSaleBundle\Controller\Api;

use App\Modules\PointOfSaleBundle\Dto\PointOfSaleDto;
use App\Modules\PointOfSaleBundle\Entity\PointOfSale;
use App\Modules\PointOfSaleBundle\Repository\PointOfSaleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

class PointOfSaleController extends AbstractController
{
    /**
     * @var PointOfSaleRepository
     */
    private $pointOfSaleRepository;

    public function __construct(PointOfSaleRepository $pointOfSaleRepository)
    {
        $this->pointOfSaleRepository = $pointOfSaleRepository;
    }

    /**
     * get points of sale
     *
     * @Route("/api/points-of-sale", methods={"GET"})
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns points of sale collection DTO",
     * )
     * @OA\Tag(name="points-of-sale")
     */
    public function getAllPointsOfSale(): JsonResponse
    {
        if (!$points = $this->pointOfSaleRepository->findAll()) {

            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(array_map(function (PointOfSale $pointOfSale) { return PointOfSaleDto::fromEntity($pointOfSale); }, $points));
    }
}
