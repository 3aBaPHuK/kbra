<?php

namespace App\Modules\PointOfSaleBundle\Repository;

use App\Modules\PointOfSaleBundle\Entity\PointOfSale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class PointOfSaleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PointOfSale::class);
    }
}
