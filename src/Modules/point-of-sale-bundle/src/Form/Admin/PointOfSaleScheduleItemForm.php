<?php

namespace App\Modules\PointOfSaleBundle\Form\Admin;

use App\Modules\PointOfSaleBundle\Entity\ScheduleItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PointOfSaleScheduleItemForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dayOfWeek', ChoiceType::class, [
                'required' => true,
                'label' => 'ADMIN_POINT_SCHEDULE_ITEM_DAY_OF_WEEK',
                'choices' => $this->getDayOfWeekChoices()
            ])
            ->add('fromTime', TextType::class, ['required' => true, 'label' => 'ADMIN_POINT_SCHEDULE_ITEM_FROM'])
            ->add('toTime', TextType::class, ['required' => true, 'label' => 'ADMIN_POINT_SCHEDULE_ITEM_TO'])
            ->add('lunchFrom', TextType::class, ['required' => false, 'label' => 'ADMIN_POINT_SCHEDULE_ITEM_LUNCH_FROM'])
            ->add('lunchTo', TextType::class, ['required' => false, 'label' => 'ADMIN_POINT_SCHEDULE_ITEM_LUNCH_TO'])
            ->add('position', HiddenType::class, ['required' => true, 'label' => 'ADMIN_LABEL_POSITION']);
    }

    private function getDayOfWeekChoices(): array
    {
        $res = [];

        foreach (ScheduleItem::AVAILABLE_DAYS as $day) {
            $res['ADMIN_POINT_SCHEDULE_ITEM_DAY_' . strtoupper($day)] = $day;
        }

        return $res;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ScheduleItem::class,
            'sort' => 'position'
        ));
    }
}
