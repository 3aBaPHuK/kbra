<?php

namespace App\Modules\PointOfSaleBundle\Dto;

use App\Modules\PointOfSaleBundle\Entity\ScheduleItem;
use JsonSerializable;

final class ScheduleItemDto implements JsonSerializable
{
    private $dayOfWeek;

    private $from;

    private $to;

    private $lunchFrom;

    private $lunchTo;

    private function __construct(string $dayOfWeek, string $from, string $to, ?string $lunchFrom, ?string $lunchTo)
    {
        $this->dayOfWeek = $dayOfWeek;
        $this->from = $from;
        $this->to = $to;
        $this->lunchFrom = $lunchFrom;
        $this->lunchTo = $lunchTo;
    }

    public static function fromEntity(ScheduleItem $scheduleItem)
    {
        return new self(
            $scheduleItem->getDayOfWeek(),
            $scheduleItem->getFromTime(),
            $scheduleItem->getToTime(),
            $scheduleItem->getLunchFrom(),
            $scheduleItem->getLunchTo()
        );
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
