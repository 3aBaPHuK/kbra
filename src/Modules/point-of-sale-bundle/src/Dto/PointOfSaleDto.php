<?php

namespace App\Modules\PointOfSaleBundle\Dto;

use App\Modules\PointOfSaleBundle\Entity\PointOfSale;
use App\Modules\PointOfSaleBundle\Entity\ScheduleItem;
use JsonSerializable;

final class PointOfSaleDto implements JsonSerializable
{
    private $id;

    private $title = '';

    private $address = '';

    private $phone = '';

    private $longitude = 0.00;

    private $latitude = 0.00;

    private $scheduleItems;

    private $linkTitle;

    private $link;

    private function __construct(
        int $id,
        string $title,
        string $address,
        string $phone,
        string $longitude,
        string $latitude,
        array $scheduleItems,
        ?string $linkTitle,
        ?string $link
    )
    {
        $this->id = $id;
        $this->title = $title;
        $this->address = $address;
        $this->phone = $phone;
        $this->longitude = $longitude;
        $this->latitude = $latitude;
        $this->scheduleItems = $scheduleItems;
        $this->linkTitle = $linkTitle;
        $this->link = $link;
    }

    public static function fromEntity(PointOfSale $pointOfSale): self
    {
        return new self(
            $pointOfSale->getId(),
            $pointOfSale->getTitle(),
            $pointOfSale->getAddress(),
            $pointOfSale->getPhone(),
            $pointOfSale->getLongitude(),
            $pointOfSale->getLatitude(),
            self::sortScheduleItems($pointOfSale),
            $pointOfSale->getLinkTitle(),
            $pointOfSale->getLink()
        );
    }

    private static function sortScheduleItems(PointOfSale $pointOfSale): array
    {
        $scheduleItems = $pointOfSale->getScheduleItems()->getValues();

        usort($scheduleItems, function (ScheduleItem $itemA, ScheduleItem $itemB) {
            return $itemA->getPosition() > $itemB->getPosition();
        });

        return array_map(function (ScheduleItem $phone) {
            return ScheduleItemDto::fromEntity($phone);
        }, $scheduleItems);
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
