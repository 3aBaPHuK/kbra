<?php

namespace App\Modules\PointOfSaleBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Modules\PointOfSaleBundle\Repository\PointOfSaleRepository")
 */
class PointOfSale
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $title = '';

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $address = '';

    /**
     * @var string
     * @ORM\Column(type="string", length=30)
     */
    private $phone = '';

    /**
     * @var float
     * @ORM\Column(type="float", nullable=false)
     */
    private $longitude = 0.00;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=false)
     */
    private $latitude = 0.00;

    /**
     * @ORM\OneToMany(targetEntity="App\Modules\PointOfSaleBundle\Entity\ScheduleItem", mappedBy="pointOfSale", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $scheduleItems;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkTitle;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link;

    public function __construct()
    {
        $this->scheduleItems = new ArrayCollection();
    }

    public function getScheduleItems(): ?Collection
    {
        return $this->scheduleItems;
    }

    public function setScheduleItems(?Collection $scheduleItems): void
    {
        $this->scheduleItems = $scheduleItems;
    }

    public function addScheduleItem(ScheduleItem $scheduleItem): PointOfSale
    {
        $scheduleItem->setPointOfSale($this);

        $this->scheduleItems[] = $scheduleItem;

        return $this;
    }

    public function removeScheduleItem(ScheduleItem $scheduleItem)
    {
        $this->scheduleItems->removeElement($scheduleItem);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): void
    {
        $this->longitude = $longitude;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): void
    {
        $this->latitude = $latitude;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    public function getLinkTitle(): ?string
    {
        return $this->linkTitle;
    }

    public function setLinkTitle(?string $linkTitle): void
    {
        $this->linkTitle = $linkTitle;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): void
    {
        $this->link = $link;
    }
}
