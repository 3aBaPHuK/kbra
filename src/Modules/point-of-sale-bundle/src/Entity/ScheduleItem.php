<?php

namespace App\Modules\PointOfSaleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * @ORM\Entity
 */
class ScheduleItem
{
    public const AVAILABLE_DAYS = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $dayOfWeek;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $fromTime;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $toTime;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $lunchFrom = null;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $lunchTo = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Modules\PointOfSaleBundle\Entity\PointOfSale", inversedBy="ScheduleItems")
     * @ORM\JoinColumn(nullable=true)
     */
    private $pointOfSale;

    public function getDayOfWeek(): ?string
    {
        return $this->dayOfWeek;
    }

    public function setDayOfWeek(?string $dayOfWeek): void
    {
        if (!in_array($dayOfWeek, self::AVAILABLE_DAYS)) {
            throw new InvalidArgumentException('Available to set only that days: ' . implode(', ', self::AVAILABLE_DAYS));
        }

        $this->dayOfWeek = $dayOfWeek;
    }

    public function getLunchFrom(): ?string
    {
        return $this->lunchFrom;
    }

    public function setLunchFrom(?string $lunchFrom): void
    {
        $this->lunchFrom = $lunchFrom;
    }

    public function getLunchTo(): ?string
    {
        return $this->lunchTo;
    }

    public function setLunchTo(?string $lunchTo): void
    {
        $this->lunchTo = $lunchTo;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    public function getPointOfSale()
    {
        return $this->pointOfSale;
    }

    public function setPointOfSale($pointOfSale): void
    {
        $this->pointOfSale = $pointOfSale;
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFromTime()
    {
        return $this->fromTime;
    }

    /**
     * @param mixed $from
     */
    public function setFromTime($from): void
    {
        $this->fromTime = $from;
    }

    public function getToTime()
    {
        return $this->toTime;
    }

    public function setToTime($toTime): void
    {
        $this->toTime = $toTime;
    }
}
