<?php

namespace App\Modules\SliderBundle\Controller\Api;

use App\Modules\SliderBundle\Dto\SliderDto;
use App\Modules\SliderBundle\Repository\SliderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use OpenApi\Annotations as OA;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SliderController extends AbstractController
{
    /**
     * @var SliderRepository
     */
    private $sliderRepository;
    /**
     * @var ParameterBag
     */
    private $parameterBag;

    public function __construct(SliderRepository $sliderRepository, ParameterBagInterface $parameterBag)
    {
        $this->sliderRepository = $sliderRepository;
        $this->parameterBag = $parameterBag;
    }

    /**
     * Get slider by alias
     *
     * @Route("/api/slides/{alias}", methods={"GET"})
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns shop DTO",
     * )
     * @OA\Tag(name="slider")
     */
    public function getByAlias(string $alias)
    {
        $sliderEntity = $this->sliderRepository->findOneBy(['alias' => $alias]);
        $imagesPath = $this->parameterBag->get('app.path.banner_images');

        if (!$sliderEntity) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(SliderDto::fromEntity($sliderEntity, $imagesPath));
    }
}