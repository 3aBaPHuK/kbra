<?php

namespace App\Modules\SliderBundle\Controller\Admin;

use App\Modules\SliderBundle\Entity\Slider;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

class SliderController extends EasyAdminController
{
    protected function persistEntity($entity)
    {
        $this->updateRelations($entity);
    }

    private function updateRelations(Slider $slider)
    {
        foreach ($slider->getSlides() as $slide) {
            $slide->setSlider($slider);
        }

        $this->em->persist($slider);
        $this->em->flush();
    }

    protected function updateEntity($entity)
    {
        $this->updateRelations($entity);
    }

    protected function removeEntity($entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }
}