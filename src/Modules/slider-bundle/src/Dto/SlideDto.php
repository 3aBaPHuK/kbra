<?php

namespace App\Modules\SliderBundle\Dto;

use App\Modules\SliderBundle\Entity\Slide;
use JsonSerializable;

class SlideDto implements JsonSerializable
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $description;
    /**
     * @var string
     */
    private $image;
    /**
     * @var string
     */
    private $link;
    /**
     * @var int
     */
    private $sortOrder;

    private function __construct(
        int $id,
        string $title,
        ?string $description,
        string $image,
        ?string $link,
        ?int $sortOrder
    )
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->image = $image;
        $this->link = $link;
        $this->sortOrder = $sortOrder;
    }

    public static function fromEntity(Slide $slide, $imagesPath): self
    {
        return new self(
            $slide->getId(),
            $slide->getTitle(),
            $slide->getDescription(),
            $imagesPath.DIRECTORY_SEPARATOR.$slide->getImage(),
            $slide->getLink(),
            $slide->getPosition() ?? 0
        );
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
