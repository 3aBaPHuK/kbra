<?php

namespace App\Modules\SliderBundle\Dto;

use App\Modules\SliderBundle\Entity\Slide;
use App\Modules\SliderBundle\Entity\Slider;
use JsonSerializable;

class SliderDto implements JsonSerializable
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $alias;
    /**
     * @var array
     */
    private $slides;

    private function __construct(
        string $title,
        string $alias,
        array $slides = []
    )
    {
        $this->title = $title;
        $this->alias = $alias;
        $this->slides = $slides;
    }

    public static function fromEntity(Slider $slider, $imagesPath): self
    {
        $slides = $slider->getActiveSlides()->toArray();

        usort($slides, function (Slide $a, Slide $b) {
            return $a->getPosition() < $b->getPosition() ? -1 : 1;
        });

        return new self(
            $slider->getTitle(),
            $slider->getAlias(),
            array_map(function (Slide $slide) use ($imagesPath) {
                return SlideDto::fromEntity($slide, $imagesPath);
            },  $slides)
        );
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
