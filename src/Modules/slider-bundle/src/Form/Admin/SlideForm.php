<?php

namespace App\Modules\SliderBundle\Form\Admin;

use App\Modules\SliderBundle\Entity\Slide;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class SlideForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['required' => true, 'label' => 'ADMIN_SLIDE_FORM_TITLE'])
            ->add('active', CheckboxType::class, ['required' => true, 'label' => 'ADMIN_SLIDE_FORM_IS_ACTIVE'])
            ->add('description', TextareaType::class, ['required' => true, 'label' => 'ADMIN_SLIDE_FORM_DESCRIPTION'])
            ->add('link', TextType::class, ['required' => false, 'label' => 'ADMIN_SLIDE_FORM_LINK'])
            ->add('imageFile', VichImageType::class, ['required' => true, 'label' => 'ADMIN_SLIDE_FORM_IMAGE'])
            ->add('position', HiddenType::class, ['required' => false, 'label' => 'ADMIN_POSITION_FIELD']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Slide::class,
        ));
    }
}
