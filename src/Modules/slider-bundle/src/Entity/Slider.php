<?php

namespace App\Modules\SliderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Modules\SliderBundle\Repository\SliderRepository")
 */
class Slider
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $alias;

    /**
     * @ORM\OneToMany(targetEntity="App\Modules\SliderBundle\Entity\Slide", mappedBy="slider", cascade={"persist", "remove"})
     */
    private $slides;

    public function __construct()
    {
        $this->slides = new ArrayCollection();
    }

    public function getSlides(): Collection
    {
        return $this->slides;
    }

    public function getActiveSlides(): Collection
    {
        return $this->slides->filter(function (Slide $slide) {
            return $slide->isActive();
        });
    }

    public function addSlide(Slide $slide): self
    {
        if (!$this->slides->contains($slide)) {
            $this->slides[] = $slide;
            $slide->setSlider($this);
        }

        return $this;
    }

    public function removeSlide(Slide $slide): self
    {
        if ($this->slides->contains($slide)) {
            $this->slides->removeElement($slide);

            if ($slide->getSlider() === $this) {
                $slide->setSlider(null);
            }
        }

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }


    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(?string $alias): void
    {
        $this->alias = $alias;
    }
}
