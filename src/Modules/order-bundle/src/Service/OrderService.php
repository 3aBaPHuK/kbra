<?php

namespace App\Modules\OrderBundle\Service;

use App\Entity\Offer;
use App\Entity\ProductOffer;
use App\Modules\CouponBundle\Interfaces\CouponServiceInterface;
use App\Modules\OrderBundle\Dto\OrderDto;
use App\Modules\OrderBundle\Entity\Customer;
use App\Modules\OrderBundle\Entity\Order;
use App\Modules\OrderBundle\Entity\OrderItem;
use App\Modules\OrderBundle\Repository\OrderItemRepository;
use App\Modules\OrderBundle\Repository\OrderRepository;
use App\Modules\OrderBundle\Value\OrderItemConfigurationCollection;
use App\Modules\OrderBundle\Value\OrderItemConfigurationPart;
use App\Repository\ProductRepository;
use DateTimeImmutable;
use InvalidArgumentException;

class OrderService
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepository;
    /**
     * @var CouponServiceInterface
     */
    private $couponService;

    public function __construct(
        OrderRepository $orderRepository,
        ProductRepository $productRepository,
        OrderItemRepository $orderItemRepository,
        CouponServiceInterface $couponService
    )
    {
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
        $this->orderItemRepository = $orderItemRepository;
        $this->couponService = $couponService;
    }

    public function createOrderFromDto(OrderDto $orderDto): Order
    {
        $order = new Order();

        $customerDto = $orderDto->getCustomer();
        $customer = new Customer();
        $customer->setType($orderDto->getCustomer()->getType());
        $customer->setPhone($customerDto->getPhone());
        $customer->setEmail($customerDto->getEmail());
        $customer->setContactAddress($customerDto->getContactAddress());

        switch ($orderDto->getCustomer()->getType()) {
            case Customer::TYPE_PHYSICAL:
                $customer->setFirstName($customerDto->getFirstName());
                $customer->setMiddleName($customerDto->getMiddleName());
                $customer->setLastName($customerDto->getLastName());
                break;
            case Customer::TYPE_LEGAL:
                $customer->setTypeOfOwnership($customerDto->getTypeOfOwnership());
                $customer->setCompanyName($customerDto->getCompanyName());
                $customer->setInn($customerDto->getInn());
                $customer->setAddress($customerDto->getAddress());
                $customer->setContactName($customerDto->getContactName());
                break;
        }

        $order->setCustomer($customer);

        $order->setStatus(Order::STATUS_CREATED);
        $order->setCreatedAt(new DateTimeImmutable('now'));
        $order->setUpdatedAt(new DateTimeImmutable('now'));
        $order->setCouponCode($orderDto->getCouponCode());
        $total = 0;

        foreach ($orderDto->getOrderItemsCollection() as $item) {

            $orderItem = new OrderItem();
            $orderItem->setAmount($item->getAmount());
            $orderItem->setOrderItemOrder($order);

            $product = $this->productRepository->find($item->getProductId());

            $orderItem->setName($product->getName());

            $selectedProductOffer = array_filter($product->getProductOffers()->toArray(), function (ProductOffer $productOffer) use ($item) {
                return $item->getOfferId() === $productOffer->getOffer()->getId();
            });

            if (!$selectedProductOffer = array_shift($selectedProductOffer)) {
                throw new InvalidArgumentException('Unknown offer with Id:' . $item->getOfferId(). ' of product: ' . $product->getId());
            }

            $orderItem->setIsPresent($item->getIsPresent());

            $orderItem->setPrice($this->calculateItemPrice($selectedProductOffer->getOffer(), $item));
            $orderItem->setOrderItemProduct($product->getId());
            $orderItem->setVendorCode($product->getVendorCode());
            $orderItem->setOrderItemOffer($selectedProductOffer->getOffer()->getId());

            if ($item->getConfigurationValues()) {
                $parts = [];

                foreach ($item->getConfigurationValues() as $part) {
                    $parts[] = new OrderItemConfigurationPart(
                        $part['level'],
                        $part['levelName'],
                        $part['name'],
                        $part['image']
                    );
                }

                $orderItem->setOderItemConfiguration(new OrderItemConfigurationCollection(...$parts));
            }

            $orderItem = $this->couponService->applyCouponToOrderItem($orderItem, $orderDto->getCouponCode());

            $this->orderItemRepository->save($orderItem);

            $total += ($orderItem->getPrice() * $orderItem->getAmount());
            $order->addOrderItem($orderItem);
        }

        $order->setTotal($total);
        $order->setHash($this->createOrderHash($order));

        $this->couponService->applyIsPresentToOrder($order, $orderDto->getCouponCode());

        return $this->orderRepository->saveOrder($order);
    }

    public function createOrderHash(Order $order): string
    {
        return md5($order->getId().'|'.$order->getCreatedAt()->format('Y-m-d H:i:s'));
    }

    public function findOrderByHash(string $hash)
    {
        return $this->orderRepository->findByHash($hash);
    }

    private function calculateItemPrice(Offer $offer, $item): float
    {
        $itemPrice = $offer->getPrice();

        if ($offer->getDiscountPrice()) {
            $itemPrice = $offer->getDiscountPrice();
        }

        if ($item->getIsPresent()) {
            $itemPrice = 0;
        }

        return $itemPrice;
    }
}
