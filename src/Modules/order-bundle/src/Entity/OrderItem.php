<?php

namespace App\Modules\OrderBundle\Entity;

use App\Entity\Offer;
use App\Entity\Product;
use App\Modules\OrderBundle\Value\OrderItemConfigurationCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Modules\OrderBundle\Repository\OrderItemRepository")
 */
class OrderItem
{
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $vendorCode;

    /**
     * @ORM\ManyToOne(targetEntity="App\Modules\OrderBundle\Entity\Order", inversedBy="orderItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $orderItemOrder;

    /**
     * @ORM\Column(type="integer")
     */
    private $orderItemProduct;

    /**
     * @ORM\Column(type="integer")
     */
    private $orderItemOffer;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPresent;

    /**
     * @var ?OrderItemConfigurationCollection
     * @ORM\Column(type="string", nullable=true)
     */
    private $oderItemConfiguration = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getOrderItemOrder(): ?Order
    {
        return $this->orderItemOrder;
    }

    public function setOrderItemOrder(?Order $orderItemOrder): self
    {
        $this->orderItemOrder = $orderItemOrder;

        return $this;
    }

    public function getOrderItemProduct(): int
    {
        return $this->orderItemProduct;
    }

    public function setOrderItemProduct(int $orderItemProduct): void
    {
        $this->orderItemProduct = $orderItemProduct;
    }

    public function getOrderItemOffer(): int
    {
        return $this->orderItemOffer;
    }

    public function setOrderItemOffer(int $orderItemOffer): void
    {
        $this->orderItemOffer = $orderItemOffer;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getVendorCode(): ?string
    {
        return $this->vendorCode;
    }

    public function setVendorCode(?string $vendorCode): void
    {
        $this->vendorCode = $vendorCode;
    }

    public function isPresent(): bool
    {
        return $this->isPresent;
    }

    public function setIsPresent(bool $isPresent): void
    {
        $this->isPresent = $isPresent;
    }

    public function getOderItemConfiguration(): ?OrderItemConfigurationCollection
    {
        if (!$this->oderItemConfiguration) {
            return null;
        }

        return OrderItemConfigurationCollection::fromJson($this->oderItemConfiguration);
    }

    public function setOderItemConfiguration(?OrderItemConfigurationCollection $oderItemConfiguration): void
    {
        $data = [];

        foreach ($oderItemConfiguration as $configurationItem) {
            $data[] = [
                'level' => $configurationItem->getLevel(),
                'levelName' => $configurationItem->getLevelName(),
                'name' => $configurationItem->getName(),
                'image' => $configurationItem->getImage()
            ];
        }

        $this->oderItemConfiguration = json_encode($data);
    }
}
