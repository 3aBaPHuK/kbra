<?php

namespace App\Modules\OrderBundle\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * @ORM\Entity(repositoryClass="App\Modules\OrderBundle\Repository\OrderRepository")
 * @ORM\Table(name="`order`")
 */
class Order
{
    const STATUS_CREATED = 'created';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_PROGRESS = 'progress';
    const STATUS_FINISHED = 'finished';

    const STATUSES_AVAILABLE = [
        self::STATUS_CREATED, self::STATUS_CANCELLED, self::STATUS_FINISHED, self::STATUS_PROGRESS
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Modules\OrderBundle\Entity\OrderItem", mappedBy="orderItemOrder", cascade={"persist", "remove"})
     */
    private $orderItems;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="float")
     */
    private $total = 0.00;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $status = self::STATUS_CREATED;

    /**
     * @OneToOne(targetEntity="App\Modules\OrderBundle\Entity\Customer", inversedBy="order", cascade={"persist", "remove"})
     */
    private $customer;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $hash;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $couponCode;

    /**
     * @ORM\Column(type="boolean")
     */
    private $providePresent = false;

    public function __construct()
    {
        $this->orderItems = new ArrayCollection();
    }

    public function __toString()
    {
        return ('Заказ ' . $this->getId()) . ' от ' . $this->getCreatedAt()->format('d.m.Y H:i');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|OrderItem[]
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItem $orderItem): self
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems[] = $orderItem;
            $orderItem->setOrderItemOrder($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItem $orderItem): self
    {
        if ($this->orderItems->contains($orderItem)) {
            $this->orderItems->removeElement($orderItem);
            // set the owning side to null (unless already changed)
            if ($orderItem->getOrderItemOrder() === $this) {
                $orderItem->setOrderItemOrder(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt ?? new DateTime('now');

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getTotal(): float
    {
        return $this->total;
    }

    public function setTotal(float $total): void
    {
        $this->total = $total;
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function setCustomer(Customer $customer): void
    {
        $this->customer = $customer;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function setHash(string $hash): void
    {
        $this->hash = $hash;
    }

    public function getCouponCode(): ?string
    {
        return $this->couponCode;
    }

    public function setCouponCode(?string $couponCode): void
    {
        $this->couponCode = $couponCode;
    }

    public function isProvidePresent(): bool
    {
        return $this->providePresent;
    }

    public function setProvidePresent(bool $providePresent): void
    {
        $this->providePresent = $providePresent;
    }
}
