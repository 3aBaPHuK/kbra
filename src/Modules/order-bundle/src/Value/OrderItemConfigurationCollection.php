<?php

declare(strict_types=1);

namespace App\Modules\OrderBundle\Value;

use Iterator;

class OrderItemConfigurationCollection implements Iterator
{
    private $parts;

    public function __construct(OrderItemConfigurationPart ...$parts)
    {
        $this->parts = $parts;
    }

    public static function fromJson(string $json):self
    {
        $data = json_decode($json, true);
        $parts = [];

        foreach ($data as $part) {
            $parts[] = new OrderItemConfigurationPart(
                $part['level'],
                $part['levelName'],
                $part['name'],
                $part['image']
            );
        }

        return new self(...$parts);
    }

    public function current(): OrderItemConfigurationPart
    {
        return current($this->parts);
    }

    public function next(): void
    {
        next($this->parts);
    }

    public function key(): ?int
    {
        return key($this->parts);
    }

    public function valid(): bool
    {
        return $this->key() !== null;
    }

    public function rewind()
    {
        reset($this->parts);
    }
}
