<?php

declare(strict_types=1);

namespace App\Modules\OrderBundle\Value;

class OrderItemConfigurationPart
{
    private $level;

    private $levelName;

    private $name;

    private $image;

    public function __construct(int $level, string $levelName, string $name, ?string $image)
    {
        $this->level = $level;
        $this->levelName = $levelName;
        $this->name = $name;
        $this->image = $image;
    }

    public function getLevelName(): string
    {
        return $this->levelName;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function __toArray(): array
    {
        return [
            'level' => $this->level,
            'levelName' => $this->levelName,
            'name' => $this->name,
            'image' => $this->image,
        ];
    }
}
