<?php

namespace App\Modules\OrderBundle\Dto;

use App\Dto\Common\AbstractCollection;

class OrderItemsCollection extends AbstractCollection
{
    public function __construct(OrderItemDto ...$values)
    {
        parent::__construct(...$values);
    }
}