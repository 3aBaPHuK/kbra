<?php

namespace App\Modules\OrderBundle\Dto;

use App\Modules\OrderBundle\Entity\OrderItem;
use App\Modules\OrderBundle\Value\OrderItemConfigurationPart;
use JsonSerializable;

class OrderItemDto implements JsonSerializable
{
    private $offerId;
    private $amount;
    private $productId;
    private $isPresent;
    private $configurationValues;
    private $price;

    private function __construct(
        int    $amount,
        int    $productId,
        int    $offerId,
        ?bool  $isPresent = false,
        ?array $configuration = [],
        ?float $price = null
    )
    {
        $this->amount = $amount;
        $this->productId = $productId;
        $this->offerId = $offerId;
        $this->isPresent = $isPresent;
        $this->configurationValues = $configuration;
        $this->price = $price;
    }

    public static function fromEntity(OrderItem $orderItemEntity): self
    {
        return new self(
            $orderItemEntity->getAmount(),
            $orderItemEntity->getOrderItemProduct(),
            $orderItemEntity->getOrderItemOffer(),
            $orderItemEntity->isPresent(),
            $orderItemEntity->getOderItemConfiguration() ?
                array_map(function (OrderItemConfigurationPart $part) {
                    return $part->__toArray();
                }, iterator_to_array($orderItemEntity->getOderItemConfiguration())) : null,
            $orderItemEntity->getPrice()
        );
    }

    public static function fromAssocArray(array $orderItemArray): self
    {
        return new self(
            $orderItemArray['amount'],
            $orderItemArray['productId'],
            $orderItemArray['offerId'],
            $orderItemArray['isPresent'] ?? false,
            $orderItemArray['configurationValues'] ?? null
        );
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function getProductId(): int
    {
        return $this->productId;
    }

    public function getOfferId(): int
    {
        return $this->offerId;
    }

    public function getIsPresent(): ?int
    {
        return $this->isPresent;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function getConfigurationValues(): ?array
    {
        return $this->configurationValues;
    }
}
