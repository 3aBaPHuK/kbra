<?php

namespace App\Modules\OrderBundle\Dto;

use App\Modules\OrderBundle\Entity\Order;
use App\Modules\OrderBundle\Entity\OrderItem;
use JsonSerializable;
use Symfony\Component\HttpFoundation\Request;

final class OrderDto implements JsonSerializable
{
    private $id;

    private $customer;

    private $orderItemsCollection;

    private $createdAt;

    private $status;

    private $hash;

    private $couponCode;

    private function __construct(
        ?int $id,
        CustomerDto $customer,
        OrderItemsCollection $orderItemsCollection,
        ?string $hash = null,
        string $createdAt = null,
        string $status = null,
        string $couponCode = null
    )
    {
        $this->id = $id;
        $this->orderItemsCollection = $orderItemsCollection;
        $this->customer = $customer;
        $this->createdAt = $createdAt;
        $this->status = $status;
        $this->hash = $hash;
        $this->couponCode = $couponCode;
    }

    public static function fromEntity(Order $order): self
    {
        return new self(
            $order->getId(),
            CustomerDto::fromEntity($order->getCustomer()),
            new OrderItemsCollection(...array_map(function (OrderItem $item) {
                    return OrderItemDto::fromEntity($item);
                }, $order->getOrderItems()->toArray())
            ),
            $order->getHash(),
            $order->getCreatedAt()->format('Y-m-d H:i'),
            $order->getStatus(),
            $order->getCouponCode()
        );
    }

    public static function fromRequest(Request $request): self
    {
        $data = json_decode($request->getContent(), true);
        $data['orderItemsCollection'] = array_filter($data['orderItemsCollection']);

        return new self(
            null,
            CustomerDto::fromAssocArray($data['customer']),
            new OrderItemsCollection(...array_map(function (array $item) {
                return OrderItemDto::fromAssocArray($item);
                }, $data['orderItemsCollection'])
            ),
            null,
            null,
            null,
            $data['couponCode'] ?? null
        );
    }

    public function getOrderItemsCollection(): OrderItemsCollection
    {
        return $this->orderItemsCollection;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function getCustomer(): CustomerDto
    {
        return $this->customer;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function getCouponCode(): ?string
    {
        return $this->couponCode;
    }
}
