<?php

namespace App\Modules\OrderBundle\Dto;

use App\Modules\OrderBundle\Entity\Customer;
use JsonSerializable;

class CustomerDto implements JsonSerializable
{
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $phone;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string|null
     */
    private $typeOfOwnership;
    /**
     * @var string|null
     */
    private $companyName;
    /**
     * @var string|null
     */
    private $inn;
    /**
     * @var string|null
     */
    private $address;
    /**
     * @var string|null
     */
    private $firstName;
    /**
     * @var string|null
     */
    private $lastName;
    /**
     * @var string|null
     */
    private $middleName;
    /**
     * @var string|null
     */
    private $contactAddress;
    /**
     * @var string|null
     */
    private $contactName;

    public static function fromEntity(Customer $customer): self
    {
        return new self(
            $customer->getType(),
            $customer->getPhone(),
            $customer->getEmail(),
            $customer->getTypeOfOwnership(),
            $customer->getCompanyName(),
            $customer->getInn(),
            $customer->getAddress(),
            $customer->getFirstName(),
            $customer->getLastName(),
            $customer->getMiddleName(),
            $customer->getContactAddress(),
            $customer->getContactName()
        );
    }

    public static function fromAssocArray(array $customerArray): self
    {
        return new self(
            $customerArray['type'],
            $customerArray['phone'],
            $customerArray['email'],
            $customerArray['typeOfOwnership'] ?? null,
            $customerArray['companyName'] ?? null,
            $customerArray['inn'] ?? null,
            $customerArray['contactName'] ?? null,
            $customerArray['address'] ?? null,
            $customerArray['firstName'] ?? null,
            $customerArray['lastName'] ?? null,
            $customerArray['middleName'] ?? null,
            $customerArray['contactAddress'] ?? null
        );
    }

    private function __construct(
        string $type,
        string $phone,
        string $email,
        ?string $typeOfOwnership,
        ?string $companyName,
        ?string $inn,
        ?string $contactName,
        ?string $address,
        ?string $firstName,
        ?string $lastName,
        ?string $middleName,
        ?string $contactAddress
    )
    {
        $this->type = $type;
        $this->phone = $phone;
        $this->email = $email;
        $this->typeOfOwnership = $typeOfOwnership;
        $this->companyName = $companyName;
        $this->inn = $inn;
        $this->address = $address;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->middleName = $middleName;
        $this->contactAddress = $contactAddress;
        $this->contactName = $contactName;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getTypeOfOwnership(): ?string
    {
        return $this->typeOfOwnership;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function getInn(): ?string
    {
        return $this->inn;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function getContactAddress(): ?string
    {
        return $this->contactAddress;
    }

    /**
     * @return string|null
     */
    public function getContactName(): ?string
    {
        return $this->contactName;
    }
}
