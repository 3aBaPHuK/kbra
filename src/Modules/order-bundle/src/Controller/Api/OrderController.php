<?php

namespace App\Modules\OrderBundle\Controller\Api;

use App\Modules\OrderBundle\Event\OrderCreatedEvent;
use App\Modules\OrderBundle\Repository\OrderRepository;
use App\Modules\OrderBundle\Service\OrderService;
use App\Modules\OrderBundle\Value\OrderTotal;
use InvalidArgumentException;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Psr\EventDispatcher\EventDispatcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use OpenApi\Annotations as OA;
use App\Modules\OrderBundle\Dto\OrderDto;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

class OrderController extends AbstractController
{
    private $eventDispatcher;

    private $assetsPath;

    private $orderService;

    private $orderRepository;

    public function __construct(OrderRepository $orderRepository, OrderService $orderService, EventDispatcherInterface $eventDispatcher, string $assetsPath)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->assetsPath = $assetsPath;
        $this->orderService = $orderService;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Create order
     *
     * @Route("/api/order/create", methods={"POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @OA\Response(
     *     response=200,
     *     description="If order successfully created",
     * )
     * @OA\RequestBody(
     *   required=true,
     *   content={
     *       @OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=OrderDto::class))
     *       )
     *     }
     * )
     *
     * @OA\Tag(name="order")
     */
    public function makeOrder(Request $request): JsonResponse
    {
        $orderDto = OrderDto::fromRequest($request);

        try {
            $order = $this->orderService->createOrderFromDto($orderDto);

            $this->eventDispatcher->dispatch(new OrderCreatedEvent($order));

            return new JsonResponse(OrderDto::fromEntity($order), Response::HTTP_OK);
        } catch (InvalidArgumentException $ex) {
            return new JsonResponse(['error' => $ex->getMessage()], Response::HTTP_BAD_REQUEST);
        } catch (Throwable $ex) {
            return new JsonResponse(['error' => $ex->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get order by hash
     *
     * @Route("/api/order/{hash}", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @OA\Tag(name="order")
     */
    public function getOrderByHash(string $hash): JsonResponse
    {
        if (!$order = $this->orderService->findOrderByHash($hash)) {

            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(OrderDto::fromEntity($order));
    }

    /**
     * Get order by id
     *
     * @Route("/api/order/{id}/pdf", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @OA\Tag(name="order")
     */
    public function getOrderPdfById(int $id, Pdf $knpSnappyPdf): Response
    {
        if (!$order = $this->orderRepository->find($id)) {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }

        $html = $this->renderView(
            '@Order/pdf/order.twig',
            array(
                'order' => $order,
                'assetsPath' => $this->assetsPath,
                'orderTotalString' => (new OrderTotal($order->getTotal()))->getStringValue()
            )
        );

        return new PdfResponse(
            $knpSnappyPdf->getOutputFromHtml($html),
            'order'.$order->getId().'.pdf'
        );
    }
}
