<?php

namespace App\Modules\OrderBundle\Controller\Admin;

use App\Modules\OrderBundle\Entity\Order;
use App\Modules\OrderBundle\Event\OrderStatusChangedEvent;
use App\Modules\OrderBundle\Repository\OrderRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use OpenApi\Annotations as OA;
use App\Modules\OrderBundle\Dto\OrderDto;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

class OrderController extends EasyAdminController
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(OrderRepository $orderRepository, EventDispatcherInterface $eventDispatcher)
    {
        $this->orderRepository = $orderRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Change order status
     *
     * @Route("/order/{orderId}/status/change", name="order_bundle.update_status", methods={"POST"})
     *
     */
    public function changeOrderStatus(int $orderId, Request $request): JsonResponse
    {
        $status = $request->get('status');
        if (!in_array($status, Order::STATUSES_AVAILABLE)) {
            throw new InvalidArgumentException();
        }

        $this->orderRepository->changeOrderStatus($orderId, $status);

        $this->eventDispatcher->dispatch(new OrderStatusChangedEvent($this->orderRepository->find($orderId), $status));

        return new JsonResponse(['order_status' => $status]);
    }
}