<?php

namespace App\Modules\OrderBundle\Event;

use App\Modules\OrderBundle\Entity\Order;

class OrderStatusChangedEvent
{
    private $order;
    /**
     * @var string
     */
    private $status;

    public function __construct(Order $order, string $status)
    {
        $this->order = $order;
        $this->status = $status;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }
}