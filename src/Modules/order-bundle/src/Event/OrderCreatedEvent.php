<?php

namespace App\Modules\OrderBundle\Event;

use App\Modules\OrderBundle\Entity\Order;

class OrderCreatedEvent
{
    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }
}