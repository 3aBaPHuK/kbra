<?php

namespace App\Modules\OrderBundle\Repository;

use App\Entity\Offer;
use App\Entity\ProductOffer;
use App\Modules\OrderBundle\Dto\OrderDto;
use App\Modules\OrderBundle\Entity\Customer;
use App\Modules\OrderBundle\Entity\Order;
use App\Modules\OrderBundle\Entity\OrderItem;
use App\Repository\ProductRepository;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use InvalidArgumentException;

final class OrderRepository extends ServiceEntityRepository
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(ManagerRegistry $registry, ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;

        parent::__construct($registry, Order::class);
    }

    public function saveOrder(Order $order): Order
    {
        $this->_em->persist($order);
        $this->_em->flush();

        return $order;
    }

    public function changeOrderStatus(int $orderId, string $status): void
    {
        if (!in_array($status, Order::STATUSES_AVAILABLE)) {
            throw new InvalidArgumentException('Invalid status');
        }

        $order = $this->find($orderId);
        $order->setStatus($status);

        $this->_em->persist($order);
        $this->_em->flush();
    }

    public function findByHash(string $hash): ?Order
    {
        return $this->findOneBy(['hash' => $hash]);
    }
}
