<?php

namespace App\Modules\OrderBundle\Repository;

use App\Modules\OrderBundle\Entity\OrderItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class OrderItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderItem::class);
    }

    public function save(OrderItem $orderItem): OrderItem
    {
        $this->_em->persist($orderItem);

        return $orderItem;
    }
}
