<?php

declare(strict_types=1);

namespace App\Modules\ProductConfigurationBundle\Dto;

use Symfony\Component\HttpFoundation\Request;

class SelectedValuesDto
{
    private $values;

    public function __construct(array $values)
    {
        $this->values = $values;
    }

    public static function fromRequest(Request $request): self
    {
        $values = json_decode($request->getContent(), true);

        return new self($values);
    }

    public function values(): array
    {
        return $this->{__FUNCTION__};
    }
}
