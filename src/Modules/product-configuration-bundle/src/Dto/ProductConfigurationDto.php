<?php

declare(strict_types=1);

namespace App\Modules\ProductConfigurationBundle\Dto;

use App\Modules\ProductConfigurationBundle\Entity\Configuration;
use App\Modules\ProductConfigurationBundle\Entity\ConfigurationPart;

final class ProductConfigurationDto
{
    public $name;
    public $vector;
    public $parts;

    public function __construct(
        string $name,
        array $vector,
        array $parts
    )
    {
        $this->name = $name;
        $this->vector = $vector;
        $this->parts = $parts;
    }
}
