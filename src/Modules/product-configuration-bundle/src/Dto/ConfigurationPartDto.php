<?php

declare(strict_types=1);

namespace App\Modules\ProductConfigurationBundle\Dto;

use App\Modules\ProductConfigurationBundle\Entity\ConfigurationPart;

class ConfigurationPartDto
{
    public $name;
    public $image;
    public $children;
    public $level;
    public $levelName;
    public $id;

    private function __construct(
        int $id,
        string $name,
        ?string $image,
        int $level,
        string $levelName,
        ?array $children
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->image = $image;
        $this->level = $level;
        $this->levelName = $levelName;
        $this->children = $children;
    }

    public static function fromEntity(ConfigurationPart $configurationPart, string $imagesPath): self
    {
        return new self(
            $configurationPart->getId(),
            $configurationPart->getName(),
            $configurationPart->getImage() ? $imagesPath .'/'. $configurationPart->getImage() : null,
            $configurationPart->getLevel()->getLevel(),
            $configurationPart->getLevel()->getName(),
            array_map(function (ConfigurationPart $entity) use ($imagesPath) {
                return self::fromEntity($entity, $imagesPath);
            }, iterator_to_array($configurationPart->getChildren()))
        );
    }
}
