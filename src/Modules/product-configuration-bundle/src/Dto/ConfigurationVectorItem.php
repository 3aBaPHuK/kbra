<?php

namespace App\Modules\ProductConfigurationBundle\Dto;

final class ConfigurationVectorItem
{
    public $name;

    public $level;

    public function __construct(string $name, int $level)
    {
        $this->name = $name;
        $this->level = $level;
    }

    public function __toString(): string
    {
        return $this->level."|".$this->name;
    }
}
