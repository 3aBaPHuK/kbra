<?php

namespace App\Modules\ProductConfigurationBundle\Controller;

use App\Modules\ProductConfigurationBundle\Dto\SelectedValuesDto;
use App\Modules\ProductConfigurationBundle\Factory\ProductConfigurationDtoFactory;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ProductConfigurationController extends AbstractController
{

    private $productRepository;
    private $productConfigurationDtoFactory;

    public function __construct(
        ProductRepository $productRepository,
        ProductConfigurationDtoFactory $productConfigurationDtoFactory
    )
    {
        $this->productRepository = $productRepository;
        $this->productConfigurationDtoFactory = $productConfigurationDtoFactory;
    }

    /**
     * Get configuration for product
     *
     * @Route("/api/product/{categorySlug}/{productSlug}/configuration", methods={"GET"})
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns configuration for product if applicable. Else return null",
     * )
     * @OA\Tag(name="products")
     */
    public function getProductConfiguration(string $productSlug, string $categorySlug): JsonResponse
    {
        $product = $this->productRepository->findBySlugAndCategory($productSlug, $categorySlug);

        if (!$product) {
            throw new NotFoundHttpException('Product not found');
        }

        if (!$configuration = $product->getConfiguration()) {
            return new JsonResponse(null);
        }

        return $this->json($this->productConfigurationDtoFactory->fromEntity($configuration));
    }
}
