<?php

declare(strict_types=1);

namespace App\Modules\ProductConfigurationBundle\Entity;

use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Modules\ProductConfigurationBundle\Entity\Configuration;
use App\Modules\ProductConfigurationBundle\Entity\ConfigurationPartLevel;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity
 * @Vich\Uploadable
 */
class ConfigurationPart
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="image")
     */
    private $imageFile;

    /**
     * @ORM\ManyToOne(targetEntity=ConfigurationPartLevel::class, inversedBy="configurationParts")
     */
    private $level;

    /**
     * @ORM\ManyToOne(targetEntity=ConfigurationPart::class, inversedBy="children")
     */
    private $parentPart = null;

    /**
     * @ORM\OneToMany(targetEntity=ConfigurationPart::class, mappedBy="parentPart")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity=Configuration::class, inversedBy="configurationParts")
     */
    private $configuration;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @var DateTimeImmutable
     */
    private $updatedAt;

    public function getId()
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setImageFile(?File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updatedAt = new DateTimeImmutable('now');
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImage(?string $image)
    {
        $this->image = $image;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function getParentPart(): ?ConfigurationPart
    {
        return $this->parentPart;
    }

    public function setParentPart(ConfigurationPart $parentPart): void
    {
        $this->parentPart = $parentPart;
    }

    public function getLevel(): ?ConfigurationPartLevel
    {
        return $this->level;
    }

    public function setLevel(ConfigurationPartLevel $level): void
    {
        $this->level = $level;
    }

    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function setChildren(Collection $children): void
    {
        $this->children = $children;
    }

    public function getConfiguration(): ?Configuration
    {
        return $this->configuration;
    }

    public function setConfiguration(?Configuration $configuration): void
    {
        $this->configuration = $configuration;
    }

    public function __toString(): string
    {
        return $this->id.": ".$this->name;
    }
}
