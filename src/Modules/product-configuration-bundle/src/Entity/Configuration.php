<?php

declare(strict_types=1);

namespace App\Modules\ProductConfigurationBundle\Entity;

use App\Entity\Product;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Modules\ProductConfigurationBundle\Repository\ConfigurationRepository;
use App\Modules\ProductConfigurationBundle\Entity\ConfigurationPart;

/**
 * @ORM\Entity(repositoryClass=ConfigurationRepository::class)
 * @ORM\Table(name="product_configuration")
 */
class Configuration
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name = '';

    /**
     * @var Product
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="configuration")
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity=ConfigurationPart::class, mappedBy="configuration")
     */
    private $configurationParts;

    public function __construct()
    {
        $this->configurationParts = new ArrayCollection();
        $this->products = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getConfigurationParts(): Collection
    {
        return $this->configurationParts;
    }

    public function setConfigurationParts(Collection $configurationParts): void
    {
        $this->configurationParts = $configurationParts;
    }

    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function setProducts(ArrayCollection $products): void
    {
        $this->products = $products;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
