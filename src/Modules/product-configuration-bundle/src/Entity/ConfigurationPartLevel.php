<?php

namespace App\Modules\ProductConfigurationBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ConfigurationPartLevel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name = '';

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\OneToMany(targetEntity=ConfigurationPart::class, mappedBy="level")
     */
    private $configurationParts;

    public function __construct()
    {
        $this->configurationParts = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(?int $level): void
    {
        $this->level = $level;
    }

    public function getConfigurationParts(): Collection
    {
        return $this->configurationParts;
    }

    public function setConfigurationParts(ArrayCollection $configurationParts): void
    {
        $this->configurationParts = $configurationParts;
    }

    public function __toString(): string
    {
        return $this->id.":".$this->name;
    }
}
