<?php

namespace App\Modules\ProductConfigurationBundle\Factory;

use App\Modules\ProductConfigurationBundle\Dto\ConfigurationPartDto;
use App\Modules\ProductConfigurationBundle\Dto\ConfigurationVectorItem;
use App\Modules\ProductConfigurationBundle\Dto\ProductConfigurationDto;
use App\Modules\ProductConfigurationBundle\Entity\Configuration;
use App\Modules\ProductConfigurationBundle\Entity\ConfigurationPart;

final class ProductConfigurationDtoFactory
{
    private $imagesPath;

    public function __construct(string $imagesPath)
    {
        $this->imagesPath = $imagesPath;
    }

    public function fromEntity(Configuration $configuration): ProductConfigurationDto
    {
        $firstLevel = $configuration->getConfigurationParts()->filter(function (ConfigurationPart $configurationPart) {
            return $configurationPart->getParentPart() === null;
        });

        $vector = [];

        foreach ($configuration->getConfigurationParts() as $configurationPart) {
            $vector[] = new ConfigurationVectorItem(
                $configurationPart->getLevel()->getName(),
                $configurationPart->getLevel()->getLevel()
            );
        }

        $vector = array_unique($vector);

        usort($vector, function (ConfigurationVectorItem $a, ConfigurationVectorItem $b) {
            return $a->level > $b->level ? 1 : -1;
        });

        $parts = array_map(
            function (ConfigurationPart $entity) {
                return ConfigurationPartDto::fromEntity($entity, $this->imagesPath);
            },
            iterator_to_array($firstLevel)
        );

        return new ProductConfigurationDto(
            $configuration->getName(),
            $vector,
            array_values($parts)
        );
    }
}
