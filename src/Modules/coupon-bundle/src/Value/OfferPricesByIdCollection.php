<?php

declare(strict_types=1);

namespace App\Modules\CouponBundle\Value;

use Generator;
use IteratorAggregate;
use JsonSerializable;
use RuntimeException;

final class OfferPricesByIdCollection implements IteratorAggregate, JsonSerializable
{
    private $offerPrices;

    public function __construct(OfferPrice ...$offerPrices)
    {
        foreach ($offerPrices as $offerPrice) {
            $this->offerPrices[$offerPrice->offerId()] = $offerPrice;
        }
    }

    public function add(OfferPrice $offerPrice): void
    {
        if (!empty($this->offerPrices[$offerPrice->offerId()])) {
            throw new RuntimeException("Offer with id {$offerPrice->offerId()} already exist");
        }

        $this->offerPrices[$offerPrice->offerId()] = $offerPrice;
    }

    public function getByOfferId(int $offerId): OfferPrice
    {
        return $this->offerPrices[$offerId];
    }

    public function getIterator(): Generator
    {
        foreach ($this->offerPrices as $offerPrice) {
            yield $offerPrice;
        }
    }

    public function jsonSerialize(): array
    {
        return array_values($this->offerPrices ?? []);
    }
}
