<?php

declare(strict_types=1);

namespace App\Modules\CouponBundle\Value;

use JsonSerializable;

final class OfferPrice implements JsonSerializable
{
    /**
     * @var int
     */
    private $offerId;
    /**
     * @var float
     */
    private $price;

    public function __construct(int $offerId, float $price)
    {
        $this->offerId = $offerId;
        $this->price = $price;
    }

    public function offerId(): int
    {
        return $this->{__FUNCTION__};
    }

    public function price(): float
    {
        return $this->{__FUNCTION__};
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
