<?php

namespace App\Modules\CouponBundle\Interfaces;

use App\Modules\CouponBundle\Value\OfferPricesByIdCollection;
use App\Modules\OrderBundle\Entity\Order;
use App\Modules\OrderBundle\Entity\OrderItem;

interface CouponServiceInterface
{
    public function generateCouponCode(int $length = 10): string;

    public function recalculateOfferPricesWithCoupon(string $couponCode, array $offerIds): OfferPricesByIdCollection;

    public function applyCouponToOrderItem(OrderItem $orderItem, ?string $couponCode): OrderItem;

    public function applyIsPresentToOrder(Order $order, ?string $couponCode): Order;
}
