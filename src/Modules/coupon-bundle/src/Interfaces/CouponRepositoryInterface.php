<?php

namespace App\Modules\CouponBundle\Interfaces;

use App\Modules\CouponBundle\Entity\Coupon;

interface CouponRepositoryInterface
{
    public function findActiveByCode(string $code): ?Coupon;
}
