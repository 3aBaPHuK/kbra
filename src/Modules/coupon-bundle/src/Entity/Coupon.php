<?php

declare(strict_types=1);

namespace App\Modules\CouponBundle\Entity;

use DateTimeImmutable;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Modules\CouponBundle\Repository\CouponRepository;
use App\Entity\Product;

/**
 * @ORM\Entity(repositoryClass=CouponRepository::class)
 */
class Coupon
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $code;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity=Product::class)
     * @var Collection
     */
    private $products;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * @var DateTimeImmutable
     */
    private $validFrom;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * @var DateTimeImmutable
     */
    private $validUntil;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var float
     */
    private $discountPercent;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var float
     */
    private $discountAmount;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var bool
     */
    private $hasPresent;


    public function getId(): int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description = ''): void
    {
        $this->description = $description;
    }

    public function getProducts(): ?Collection
    {
        return $this->products;
    }

    public function getOffers(callable $filter = null): array
    {
        $offers = [];

        foreach ($this->getProducts() as $product) {
            foreach ($product->getProductOffers() as $productOffer) {
                $offers[] = $productOffer->getOffer();
            }
        }

        if ($filter) {
            return array_filter($offers, $filter);
        }

        return $offers;
    }

    public function setProducts(?Collection $categories): void
    {
        $this->products = $categories;
    }

    public function getValidFrom(): ?DateTimeImmutable
    {
        return $this->validFrom;
    }

    public function setValidFrom(?DateTimeImmutable $validFrom): void
    {
        $this->validFrom = $validFrom;
    }

    public function getValidUntil(): ?DateTimeImmutable
    {
        return $this->validUntil;
    }

    public function setValidUntil(?DateTimeImmutable $validUntil): void
    {
        $this->validUntil = $validUntil;
    }

    public function getDiscountPercent(): ?float
    {
        return $this->discountPercent;
    }

    public function setDiscountPercent(?float $discountPercent): void
    {
        $this->discountPercent = $discountPercent;
    }

    public function getDiscountAmount(): ?float
    {
        return $this->discountAmount;
    }

    public function setDiscountAmount(?float $discountAmount): void
    {
        $this->discountAmount = $discountAmount;
    }

    public function isHasPresent(): ?bool
    {
        return $this->hasPresent;
    }

    public function setHasPresent(?bool $hasPresent): void
    {
        $this->hasPresent = $hasPresent;
    }
}
