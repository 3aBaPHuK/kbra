<?php

namespace App\Modules\CouponBundle\Dto\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

final class ApplyCouponRequest
{
    /**
     * @var string
     */
    private $couponCode;
    /**
     * @var int[]
     */
    private $offerIds;

    private function __construct(
        string $couponCode,
        array $offerIds
    )
    {
        $this->couponCode = $couponCode;
        $this->offerIds = $offerIds;
    }

    public final static function fromRequest(Request $request): self
    {
        $content = json_decode($request->getContent(), true);

        if (empty($content['couponCode']) || empty($content['offerIds'])) {
            throw new BadRequestHttpException("Wrong request body");
        }

        return new self(
            $content['couponCode'],
            $content['offerIds']
        );
    }

    public function getCouponCode(): string
    {
        return $this->couponCode;
    }

    public function getOfferIds(): array
    {
        return $this->offerIds;
    }
}
