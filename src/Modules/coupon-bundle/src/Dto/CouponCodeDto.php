<?php

namespace App\Modules\CouponBundle\Dto;

final class CouponCodeDto
{

    public $code;

    public function __construct(string $code)
    {
        $this->code = $code;
    }
}
