<?php

namespace App\Modules\CouponBundle\Exception;

use OutOfBoundsException;

class UnknownCouponException extends OutOfBoundsException
{
    public static function throwFromCouponCode(string $couponCode): void
    {
        throw new self("Unknown Coupon Code $couponCode");
    }
}
