<?php

declare(strict_types=1);

namespace App\Modules\CouponBundle\Service;

use App\Entity\Offer;
use App\Modules\CouponBundle\Entity\Coupon;
use App\Modules\CouponBundle\Exception\UnknownCouponException;
use App\Modules\CouponBundle\Interfaces\CouponRepositoryInterface;
use App\Modules\CouponBundle\Interfaces\CouponServiceInterface;
use App\Modules\CouponBundle\Value\OfferPrice;
use App\Modules\CouponBundle\Value\OfferPricesByIdCollection;
use App\Modules\OrderBundle\Entity\Order;
use App\Modules\OrderBundle\Entity\OrderItem;

class CouponService implements CouponServiceInterface
{

    private $couponRepository;

    public function __construct(CouponRepositoryInterface $couponRepository)
    {
        $this->couponRepository = $couponRepository;
    }

    public function generateCouponCode(int $length = 10): string
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $code = '';

        for ($i = 0; $i < $length; $i++) {
            $code .= $chars[mt_rand(0, strlen($chars) - 1)];
        }

        return $code;
    }

    public function recalculateOfferPricesWithCoupon(string $couponCode, array $offerIds): OfferPricesByIdCollection
    {
        $offerPrices = new OfferPricesByIdCollection();

        $coupon = $this->couponRepository->findActiveByCode($couponCode);

        if (!$coupon) {
            UnknownCouponException::throwFromCouponCode($couponCode);
        }

        $couponOffers = $coupon->getOffers(function (Offer $offer) use ($offerIds) {
            return in_array($offer->getId(), $offerIds);
        });

        foreach ($couponOffers as $offer) {
            $discountedPrice = $this->getDiscountedPrice($offer->getDiscountPrice() ?? $offer->getPrice(), $coupon);

            $offerPrices->add(new OfferPrice(
                $offer->getId(),
                $discountedPrice
            ));
        }

        return $offerPrices;
    }

    private function getDiscountedPrice(float $price, Coupon $coupon): float
    {
        $discountedPrice = $price;

        if ($coupon->getDiscountAmount()) {
            $discountedPrice = $price - $coupon->getDiscountAmount();
        } elseif ($coupon->getDiscountPercent()) {
            $discountedPrice = $price - ($price * ($coupon->getDiscountPercent() / 100));
        }

        return max(0, $discountedPrice);
    }

    public function applyCouponToOrderItem(OrderItem $orderItem, ?string $couponCode): OrderItem
    {
        if (!$couponCode) {
            return $orderItem;
        }

        $coupon = $this->couponRepository->findActiveByCode($couponCode);
        $couponOfferIds = array_map(function (Offer $offer) {
            return $offer->getId();
        }, $coupon->getOffers());

        if (!$coupon || !in_array($orderItem->getOrderItemOffer(), $couponOfferIds)) {
            return $orderItem;
        }

        return $orderItem->setPrice((int) $this->getDiscountedPrice($orderItem->getPrice(), $coupon));
    }

    public function applyIsPresentToOrder(Order $order, ?string $couponCode): Order
    {
        if (!$couponCode) {
            return $order;
        }

        $coupon = $this->couponRepository->findActiveByCode($couponCode);

        if ($coupon->isHasPresent()) {
            $order->setProvidePresent(true);
        }

        return $order;
    }
}
