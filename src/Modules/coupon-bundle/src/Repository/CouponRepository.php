<?php

declare(strict_types=1);

namespace App\Modules\CouponBundle\Repository;

use App\Modules\CouponBundle\Entity\Coupon;
use App\Modules\CouponBundle\Interfaces\CouponRepositoryInterface;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CouponRepository extends ServiceEntityRepository implements CouponRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Coupon::class);
    }

    public function findActiveByCode(string $code): ?Coupon
    {
        $coupons =  $this->createQueryBuilder('c')
            ->where('c.code = :code')
            ->andWhere('c.validFrom <= :now')
            ->andWhere('(c.validUntil > :now OR c.validUntil IS NULL)')
            ->orWhere('c.validFrom IS NULL AND c.validUntil IS NULL')
            ->setParameter('now', new DateTimeImmutable('now'))
            ->setParameter('code', $code)
            ->getQuery()
            ->getResult();

        return is_array($coupons) ? array_shift($coupons) : null;
    }
}
