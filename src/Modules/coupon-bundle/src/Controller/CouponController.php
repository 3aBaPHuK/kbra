<?php

namespace App\Modules\CouponBundle\Controller;

use App\Modules\CouponBundle\Dto\CouponCodeDto;
use App\Modules\CouponBundle\Exception\UnknownCouponException;
use App\Modules\CouponBundle\Interfaces\CouponServiceInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Modules\CouponBundle\Dto\Request\ApplyCouponRequest;

class CouponController
{

    private $couponService;

    public function __construct(CouponServiceInterface $couponService)
    {
        $this->couponService = $couponService;
    }

    /**
     * Generate coupon code
     *
     * @Route("/coupon/generate-code", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @OA\Response(
     *     response=200,
     *     description="Coupon code",
     *     content={
     *       @OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=CouponCodeDto::class))
     *       )
     *     }
     * )
     *
     * @OA\Tag(name="coupon")
     */
    public function generateCouponCode(): JsonResponse
    {
        return new JsonResponse(new CouponCodeDto($this->couponService->generateCouponCode()));
    }

    /**
     * Calculate prices with discount by coupon
     *
     * @Route("/api/coupon/calculate-discount", methods={"POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @OA\Response(
     *     response=200,
     *     description="Successfully calculated",
     * )
     * @OA\RequestBody(
     *   required=true,
     *   content={
     *       @OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=ApplyCouponRequest::class))
     *       )
     *     }
     * )
     *
     * @OA\Tag(name="coupon")
     */
    public function getDiscountOfferPricesByCouponCode(Request $request): JsonResponse
    {

        try {
            $couponRequest = ApplyCouponRequest::fromRequest($request);
            $offerPrices = $this->couponService->recalculateOfferPricesWithCoupon($couponRequest->getCouponCode(), $couponRequest->getOfferIds());

        } catch (UnknownCouponException $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        }

        return new JsonResponse($offerPrices);
    }
}
