<?php

namespace App\Modules\CouponBundle;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CouponBundle extends Bundle
{
    public function getContainerExtension()
    {
        return null;
    }

    /**
     * @throws Exception
     */
    public function build(ContainerBuilder $container)
    {
        $container->setParameter('discount_bundle_path', dirname(__DIR__));

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/Resources/config'));
        $loader->load('services.yaml');
        $loader->load('parameters.yaml');
        $loader->load('doctrine.yaml');
        $loader->load('easy_admin.yaml');
        $loader->load('twig.yaml');

        parent::build($container);
    }
}
