<?php

namespace App\Modules\DiscountBundle\Entity;

use App\Entity\Product;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;


/**
 * @ORM\Entity(repositoryClass="App\Modules\DiscountBundle\Repository\DiscountRepository")
 */
class Discount implements JsonSerializable
{
    /**
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="discount",cascade={"persist"})
     */
    private $products;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var float
     */
    private $discountAmount;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @var float
     */
    private $discountAmountPercent;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getProducts(): ?Collection
    {
        return $this->products;
    }

    public function getDiscountAmount(): ?float
    {
        return $this->discountAmount;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function setDiscountAmount(?float $discountAmount): void
    {
        $this->discountAmount = $discountAmount;
    }

    public function setProducts(?ArrayCollection $products): void
    {
        $this->products = $products;
    }

    public function addProduct(?Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setDiscount($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            if ($product->getDiscount() === $this) {
                $product->setDiscount(null);
            }
        }

        return $this;
    }

    public function getDiscountAmountPercent(): ?float
    {
        return $this->discountAmountPercent;
    }

    public function setDiscountAmountPercent(?float $discountAmountPercent): void
    {
        $this->discountAmountPercent = $discountAmountPercent;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
