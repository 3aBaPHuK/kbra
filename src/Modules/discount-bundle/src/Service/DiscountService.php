<?php

namespace App\Modules\DiscountBundle\Service;

use App\Entity\Offer;
use App\Entity\Product;
use App\Entity\ProductOffer;
use App\Modules\DiscountBundle\Entity\Discount;
use App\Modules\DiscountBundle\Interfaces\DiscountServiceInterface;
use App\Repository\OfferRepository;
use App\Repository\ProductRepository;

class DiscountService implements DiscountServiceInterface
{
    private $offerRepository;
    private $productRepository;

    public function __construct(OfferRepository $offerRepository, ProductRepository $productRepository)
    {
        $this->offerRepository = $offerRepository;
        $this->productRepository = $productRepository;
    }

    public function updateProductOffersByDiscount(Discount $discount): void
    {
        $discountOffers = $this->getDiscountOffers($discount);

        foreach ($discountOffers as $offer) {
            $newPrice = $this->calculateDiscount(
                $offer->getPrice(),
                $discount->getDiscountAmount(),
                $discount->getDiscountAmountPercent()
            );

            $offer->setDiscountPrice(max(0, $newPrice));

            $this->offerRepository->save($offer);
        }
    }

    public function clearOffersByDiscount(Discount $discount): void
    {
        $discountOffers = $this->getDiscountOffers($discount);

        foreach ($discountOffers as $offer) {
            $offer->setDiscountPrice(null);

            $this->offerRepository->save($offer);
        }
    }

    private function calculateDiscount(float $basePrice, ?float $discountSum, ?float $discountPercent): float
    {
        if ($discountPercent) {
            return $basePrice - round($basePrice * ($discountPercent / 100), PHP_ROUND_HALF_DOWN);
        }

        if ($discountSum) {
            return round($basePrice - $discountSum, PHP_ROUND_HALF_DOWN);
        }

        return $basePrice;
    }

    /**
     * @return Offer[]
     */
    private function getDiscountOffers(Discount $discount): array
    {
        $offers = [];

        $products = $this->productRepository->findBy(['discount' => $discount]);

        foreach ($products as $product) {
            $offers = array_merge($offers, array_map(function (ProductOffer $productOffer) {
                return $productOffer->getOffer();
            }, $product->getProductOffers()->toArray()));
        }

        return $offers;
    }
}
