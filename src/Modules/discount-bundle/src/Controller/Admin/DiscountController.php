<?php

namespace App\Modules\DiscountBundle\Controller\Admin;

use App\Modules\DiscountBundle\Entity\Discount;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

class DiscountController extends EasyAdminController
{
    protected function persistEntity($entity)
    {
        $this->updateRelations($entity);
    }

    protected function removeEntity($entity)
    {
        foreach ($entity->getProducts() as $product) {
            $product->setDiscount(null);

            $this->em->persist($product);
            $this->em->flush();
        }

        $this->em->remove($entity);
        $this->em->flush();
    }

    protected function updateEntity($entity)
    {
        $this->updateRelations($entity);
    }

    private function updateRelations(Discount $discount)
    {
        foreach ($discount->getProducts() as $product) {
            $product->setDiscount($discount);
        }

        $this->em->persist($discount);
        $this->em->flush();
    }
}
