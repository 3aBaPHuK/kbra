<?php

namespace App\Modules\DiscountBundle\Interfaces;

use App\Modules\DiscountBundle\Entity\Discount;

interface DiscountServiceInterface
{
    public function updateProductOffersByDiscount(Discount $discount): void;

    public function clearOffersByDiscount(Discount $discount): void;
}
