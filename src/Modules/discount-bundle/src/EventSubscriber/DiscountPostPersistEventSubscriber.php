<?php

declare(strict_types=1);

namespace App\Modules\DiscountBundle\EventSubscriber;

use App\Entity\Product;
use App\Modules\DiscountBundle\Entity\Discount;
use App\Modules\DiscountBundle\Interfaces\DiscountServiceInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class DiscountPostPersistEventSubscriber implements EventSubscriberInterface
{
    private $discountService;

    public function __construct(DiscountServiceInterface $discountService)
    {
        $this->discountService = $discountService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            EasyAdminEvents::POST_PERSIST => 'updateOffers',
            EasyAdminEvents::PRE_UPDATE => 'clearOffersDiscount',
            EasyAdminEvents::POST_UPDATE => 'updateOffers',
            EasyAdminEvents::PRE_REMOVE => 'clearOffersDiscount'
        ];
    }

    public function updateOffers(GenericEvent $event): void
    {
        $subject = $event->getSubject();

        if ($subject instanceof Discount) {
            $this->discountService->clearOffersByDiscount($subject);
            $this->discountService->updateProductOffersByDiscount($subject);

            return;
        }

        if ($subject instanceof Product) {
            $discount = $subject->getDiscount();

            if (!$discount) {
                return;
            }

            $this->discountService->updateProductOffersByDiscount($discount);
        }
    }

    public function clearOffersDiscount(GenericEvent $event)
    {
        $subject = $event->getSubject();

        if ($subject instanceof Discount) {
            $this->discountService->clearOffersByDiscount($subject);
        }
    }
}
