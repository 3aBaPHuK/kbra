<?php

namespace App\Service;

use App\Entity\Category;
use App\Entity\Offer;
use App\Entity\Product;
use App\Entity\ProductImage;
use App\Entity\ProductOffer;
use App\Entity\ProductProperty;
use App\Modules\OrderBundle\Entity\Order;
use App\Modules\OrderBundle\Repository\OrderRepository;
use App\Repository\CategoryRepository;
use App\Repository\OfferRepository;
use App\Repository\ProductImageRepository;
use App\Repository\ProductOfferRepository;
use App\Repository\ProductPropertyRepository;
use App\Repository\ProductRepository;
use DateTime;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

class ErpService
{
    private const APPLICABLE_PRODUCT_REQUISITES_UNITS = [
        [
            'label' => 'Ширина',
            'unit' => 'см.'
        ],
        [
            'label' => 'Объем',
            'unit' => 'м.куб.'
        ],
        [
            'label' => 'Длина',
            'unit' => 'см.'
        ],
        [
            'label' => 'Высота',
            'unit' => 'см.'
        ],
        [
            'label' => 'Вес',
            'unit' => 'кг.'
        ]
    ];

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var OfferRepository
     */
    private $offerRepository;
    /**
     * @var ProductImageRepository
     */
    private $productImageRepository;
    /**
     * @var ProductOfferRepository
     */
    private $productOfferRepository;
    /**
     * @var ProductPropertyRepository
     */
    private $productPropertyRepository;
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var Environment
     */
    private $twig;
    /**
     * @var ProductImageRepository
     */
    private $imageRepository;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        ParameterBagInterface $parameterBag,
        CategoryRepository $categoryRepository,
        ProductRepository $productRepository,
        OfferRepository $offerRepository,
        ProductImageRepository $productImageRepository,
        ProductOfferRepository $productOfferRepository,
        ProductPropertyRepository $productPropertyRepository,
        ProductImageRepository $imageRepository,
        OrderRepository $orderRepository,
        Environment $twig,
        LoggerInterface $logger
    )
    {
        $this->parameterBag = $parameterBag;
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
        $this->offerRepository = $offerRepository;
        $this->productImageRepository = $productImageRepository;
        $this->productOfferRepository = $productOfferRepository;
        $this->productPropertyRepository = $productPropertyRepository;
        $this->orderRepository = $orderRepository;
        $this->twig = $twig;
        $this->imageRepository = $imageRepository;
        $this->logger = $logger;
    }

    public function getOrdersXml(): string
    {
        $orders = $this->orderRepository->findBy(['status' => Order::STATUS_CREATED]);

        return $this->twig->render('1c/1cOrder.xml.twig', ['orders' => $orders]);
    }

    public function writeFileFromFileRequest(Request $request): void
    {
        $filename = $this->parameterBag->get('app.path.1c_files_path') . DIRECTORY_SEPARATOR . $request->get('filename');

        $dirName = dirname($filename);

        if ($dirName && !is_dir($dirName)) {
            mkdir($dirName, 0777, true);
        }

        $content = $request->getContent();
        $handle = fopen($filename, 'w+');
        fwrite($handle, $content);
        fclose($handle);
    }

    public function startImport(string $filename)
    {
        $fileSystemName = $this->parameterBag->get('app.path.1c_files_path') . DIRECTORY_SEPARATOR . $filename;

        switch (true) {
            case $filename === 'import.xml':
                $this->importProductsAndCategories($fileSystemName);
                break;
            case $filename === 'offers.xml':
                $this->importOffers($fileSystemName);
                break;
        }
    }

    private function createProducts(array $products)
    {
        if (isset($products['Ид'])) {
            $products = [$products];
        }

        foreach ($products as $product)  {
            $productEntity = $this->productRepository->findByExternalId($product['Ид']);

            if (!$productEntity) {
                $productEntity = new Product();
            } else {
                foreach ($productEntity->getImageCollection() as $image) {
                    $this->imageRepository->removeImage($image);
                    $productEntity->removeImageCollection($image);
                }
            }

            if (!empty($product['Картинка'])) {
                $images = $this->createOrUpdateMainImage($product);

                foreach ($images as $image) {
                    $productEntity->addImageCollection($image);
                }
            }

            $productEntity->setExternalId($product['Ид']);

            if (!empty($product['Артикул'])) {
                $productEntity->setVendorCode($product['Артикул']);
            }

            $productEntity->setName($product['Наименование']);

            $description = !empty($product['Описание']) ? (is_array($product['Описание']) ?  array_shift($product['Описание']) : $product['Описание']) : '';

            $productEntity->setAnnounce($description ?? '');

            $categoryExternalId = !empty($product['Группы']) ? array_shift($product['Группы']) : '';

            $category = $this->categoryRepository->findByExternalId($categoryExternalId);

            if ($category) {
                $productEntity->setCategory($category);
            }

            $this->productRepository->save($productEntity);

            $applicableRequisites = array_filter($product['ЗначенияРеквизитов']['ЗначениеРеквизита'], function ($requisite) {
                return in_array($requisite['Наименование'], array_column(self::APPLICABLE_PRODUCT_REQUISITES_UNITS, 'label'));
            });
        }
    }

    private function createCategories(array $categories, $parentId = null, $level = 0)
    {
        if (isset($categories['Ид'])) {
            $categories = [$categories];
        }

        foreach ($categories as $category) {
            $categoryEntity = $this->categoryRepository->findByExternalId($category['Ид']);

            if (!$categoryEntity) {
                $categoryEntity = new Category();
            }

            $categoryEntity->setExternalId($category['Ид']);
            $categoryEntity->setParentId($parentId);
            $categoryEntity->setStatus(true);
            $categoryEntity->setName($category['Наименование']);
            $categoryEntity->setLevel($level);

            $savedCategory = $this->categoryRepository->save($categoryEntity);

            if (!empty($category['Группы'])) {
                $this->createCategories($category['Группы']['Группа'], $savedCategory->getId(), $level + 1);
            }
        }
    }

    private function importOffers(string $fileName):void
    {
        $dataArray = json_decode(json_encode(simplexml_load_file($fileName)), true);

        if (empty($dataArray['ПакетПредложений']['Предложения'])) {
            return;
        }

        $offers = $dataArray['ПакетПредложений']['Предложения']['Предложение'];

        if (!empty($offers['Ид'])) {
            $offers = [$offers];
        }

        foreach ($offers as $erpOffer) {
            if (empty($erpOffer['Ид'])) {
                continue;
            }

            $offer = $this->offerRepository->findByExternalCode($erpOffer['Ид']);

            if (!$offer) {
                $offer = new Offer();
            }

            $offer->setExternalCode($erpOffer['Ид']);
            $offer->setName($erpOffer['Наименование']);
            $offer->setPrice($erpOffer['Цены']['Цена']['ЦенаЗаЕдиницу']);
            $offer->setColor('#FFFFFF');
            $offer->setQuantity((int) $erpOffer['Количество']);

            $this->offerRepository->save($offer);

            $productForOffer = $this->productRepository->findByExternalId($erpOffer['Ид']);

            if (!$productForOffer) {

                continue;
            }

            $productOffer = $this->productOfferRepository->findByProductAndOffer($productForOffer, $offer);

            if (!$productOffer) {
                $productOffer = new ProductOffer();
            }

            $productOffer->setProduct($productForOffer);
            $productOffer->setOffer($offer);
            $productOffer->setDisplayName($erpOffer['Ид']);

            $this->productOfferRepository->save($productOffer);
        }
    }

    private function importProductsAndCategories(string $fileName): void
    {
        $dataArray = json_decode(json_encode(simplexml_load_file($fileName)), true);

        $productsArray = $dataArray['Каталог']['Товары']['Товар'];
        $categoriesArray = $dataArray['Классификатор']['Группы']['Группа'];

        $this->createCategories($categoriesArray);
        $this->createProducts($productsArray);
    }

    public function createOrUpdateMainImage($product): array
    {
        if (!is_array($product['Картинка'])) {
            $product['Картинка'] = [$product['Картинка']];
        }

        $savedImages = [];

        $sortedImages = $product['Картинка'];

        usort($sortedImages, function($imageNameA, $imageNameB) use ($product)  {
            $imageARequisites = array_filter($product['ЗначенияРеквизитов']['ЗначениеРеквизита'], function ($item) use ($imageNameA) {
                return $item['Наименование'] === 'ОписаниеФайла' && strpos($item['Значение'], $imageNameA) !== false;
            });

            $imageBRequisites = array_filter($product['ЗначенияРеквизитов']['ЗначениеРеквизита'], function ($item) use ($imageNameB) {
                return $item['Наименование'] === 'ОписаниеФайла' && strpos($item['Значение'], $imageNameB) !== false;
            });

            $imageARequisite = array_shift($imageARequisites);
            $imageBRequisite = array_shift($imageBRequisites);

            $imageADescriptionParts = explode('#', $imageARequisite['Значение']);
            $imageBDescriptionParts = explode('#', $imageBRequisite['Значение']);

            $imageADescription = array_pop($imageADescriptionParts);
            $imageBDescription = array_pop($imageBDescriptionParts);

            $partsA = explode('_', $imageADescription);
            $indexA = array_pop($partsA);
            $indexA = $indexA ? intval($indexA) : 0;

            $partsB = explode('_', $imageBDescription);
            $indexB = array_pop($partsB);
            $indexB = $indexB ? intval($indexB) : 0;

            return $indexA < $indexB ? -1 : 1;
        });

        foreach ($sortedImages as $key => $imagePath) {
            $productImagePath = realpath('./') . $this->parameterBag->get('app.path.product_images') . DIRECTORY_SEPARATOR . $imagePath;

            if (!is_dir(dirname($productImagePath))) {
                mkdir(dirname($productImagePath), 0777, true);
            }

            $imageContent = file_get_contents($this->parameterBag->get('app.path.1c_files_path') . DIRECTORY_SEPARATOR . $imagePath);

            $this->logger->debug('Try to copy image: ' . $imagePath);
            $this->logger->debug('To ' . $productImagePath);

            $result = file_put_contents($productImagePath, $imageContent);
            $this->logger->debug('Result: ' . $result);

            $image = new ProductImage();
            $image->setImage($imagePath);
            $image->setMain(!$key);
            $image->setUpdatedAt(new DateTime('now'));

            $savedImages[] = $this->productImageRepository->save($image);
        }

        return $savedImages;
    }

    private function fillProductProperties(array $applicableRequisites, ?Product $productEntity): void
    {
        foreach ($applicableRequisites as $requisite) {
            $productProperty = $this->productPropertyRepository->findByProductAndLabel($productEntity, $requisite['Наименование']);

            if (!$productProperty) {
                $productProperty = new ProductProperty();
            }

            $units = array_filter(self::APPLICABLE_PRODUCT_REQUISITES_UNITS, function ($item) use ($requisite) {
                return $item['label'] === $requisite['Наименование'];
            });

            $unit = array_shift($units);

            $productProperty->setProduct($productEntity);
            $productProperty->setLabel($requisite['Наименование']);
            $productProperty->setValue($requisite['Значение'] . ' ' . $unit['unit']);

            $this->productPropertyRepository->save($productProperty);
        }
    }
}
