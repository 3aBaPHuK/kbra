import CollectionFieldController from "./controllers/CollectionFieldController";
import DiscountPercentController from "./controllers/DiscountPercentController";
import CouponController from "./controllers/CouponController";

new CollectionFieldController();
new DiscountPercentController();
new CouponController();
