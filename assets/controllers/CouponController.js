export default class CouponController {
    constructor() {
        this.button = document.getElementById('coupon_code_generate');
        this.field = document.getElementById('coupon_code');
        this.discountPercentField = document.getElementById('coupon_discountPercent');
        this.discountAmountField = document.getElementById('coupon_discountAmount');

        this.listenGenerateButton();
        this.listenPercentOrAmountChanged();
    }

    listenPercentOrAmountChanged() {
        [this.discountPercentField, this.discountAmountField].map(field => {
            field.addEventListener('input', ev => {
                const oppositeField = ev.target.id === this.discountAmountField.id ? this.discountPercentField : this.discountAmountField;

                oppositeField.value = '';

                this.validatePercentField();
                this.validateAmountField();
            })
        })
    }

    validatePercentField() {
        if (parseInt(this.discountPercentField.value) > 100) {
            this.discountPercentField.value = '100';

            return;
        }

        if (parseInt(this.discountPercentField.value) < 0) {
            this.discountPercentField.value = '0';
        }
    }

    validateAmountField() {}

    listenGenerateButton() {
        if (!this.button || !this.field) {
            return;
        }

        this.button.addEventListener('click', () => {
            fetch('/coupon/generate-code').then(blob => {
                return blob.json()
            }).then(data => {
                if (typeof data.code === 'undefined') {
                    return;
                }

                this.field.value = data.code;
            })
        })
    }
}
