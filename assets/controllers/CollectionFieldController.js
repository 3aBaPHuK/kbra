import Sortable from "sortablejs";

export default class CollectionFieldController {
    constructor() {
        this.fieldRows = Array.prototype.slice.call(document.querySelectorAll('.form-widget-compound [data-prototype]'))

        this.initFields();
        this.fieldRows.forEach(fieldRow => {
            const addButton = fieldRow.closest('.col-12').querySelector('.field-collection-action a');

            addButton.addEventListener('click', () => {
                setTimeout(() => { // await easy-admin finished with creating new row
                    const addedField = fieldRow.lastChild;
                    const positionInput = addedField.querySelector('[name*=position]');

                    if (positionInput) {

                        positionInput.value = fieldRow.children.length
                    }

                }, 300)
            })

            Sortable.create(fieldRow, {
                handle: '.field-collection-item-row',
                animation: 400,
                onSort: () => {
                    this.fillPositions(fieldRow)
                },
            })
        })
    }

    initFields() {
        this.fieldRows.forEach(fieldRow => {
            const items = fieldRow.children;

            const sorted = [...items].sort((a, b) => {
                let positionA = 0;
                let positionB = 0;
                const positionInputA = a.querySelector('[name*=position]')
                const positionInputB = b.querySelector('[name*=position]')

                if (positionInputA) {
                    positionA = parseInt(positionInputA.value)
                }

                if (positionInputB) {
                    positionB = parseInt(positionInputB.value)
                }

                return positionA > positionB ? 1 : -1
            })

            sorted.forEach(node => fieldRow.appendChild(node));
        })

    }

    fillPositions(fieldRow) {
        Array.prototype.slice.call(fieldRow.children).forEach((child, key) => {
            const positionInput = child.querySelector('[name*=position]');

            if (!positionInput) {
                return;
            }

            positionInput.value = key + 1;
        })
    }
}
