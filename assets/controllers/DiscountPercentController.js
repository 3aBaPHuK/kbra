export default class DiscountPercentController {
    constructor() {
        const fields = document.querySelectorAll('.discount-group input[type="number"]');

        fields.forEach(field => {
            field.addEventListener('change', () => {
                fields.forEach(anotherField => {
                    if (anotherField.name === field.name) {
                        return;
                    }

                    anotherField.value = '';
                });
            });
        });
    }
}
