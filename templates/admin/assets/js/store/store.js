export default {
    mutations: {
        setData  : function (data, name, $store) {
            $store[name] = data;
        },
        pushData : function (data, name, $store, prefix, index) {
            if (!index) {
                index = 0;
            }
            $store[name][prefix] = [];
            $store[name][prefix][index] = data;
        },
        sliceData: function (name, $store, prefix, index) {
            if (!index) {
                index = 0;
            }
            $store[name][prefix][index];
        }
    },
    getters  : {}
};