import {UrlExtension} from './UrlExtension';

export class HttpRequestExtension {
    constructor() {
        this.urlExtension = new UrlExtension();
    }

    getServerData(url, method, body, dataType, hidePreloader) {
        if (!hidePreloader) {
            this.showPreloader();
        }

        return fetch(url, {
            method: method ? method : 'GET',
            body  : body ? body : undefined
        }).then((response) => {

            if (response.status === 403) {
                window.location.href = response.headers.get('X-Login');
                return;
            }

            if (!hidePreloader) {
                this.hidePreloader();
            }

            let messages = response.headers.get('X-Messages');
            messages = messages ? JSON.parse(messages) : null;

            if (messages && messages.length) {
                document.dispatchEvent(new CustomEvent('requestComplete', {detail: messages}));
            }

            return response[dataType ? dataType : 'text']();
        });
    }

    setJsonData(data) {
        let formData = new FormData();
        formData.append('json', JSON.stringify(data));
        return formData;
    }

    showPreloader() {
        document.querySelector('body').classList.add('preloader');
    }

    hidePreloader() {
        document.querySelector('body').classList.remove('preloader');
    }
}